/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.xtext.perspective.ui.handler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Platform;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osbp.xtext.oxtype.oxtype.OXImportDeclaration;
import org.eclipse.osbp.xtext.perspective.Perspective;
import org.eclipse.osbp.xtext.perspective.PerspectiveModel;
import org.eclipse.osbp.xtext.perspective.PerspectivePackage;
import org.eclipse.osbp.xtext.perspective.ui.converter.E4PerspectiveToDSLConverter;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.ui.resource.IResourceSetProvider;
import org.eclipse.xtext.xtype.XImportDeclaration;
import org.eclipse.xtext.xtype.XtypeFactory;

import com.google.inject.Inject;

public class ConvertE4PerspectiveToOSBPHandler extends AbstractHandler {

	public static final String PERSPECTIVE_MODEL_PATH = "org.fhsolution.eclipse.plugins.csvedit.customeditor.preferences.perspectiveModelPath";

	@Inject
	E4PerspectiveToDSLConverter converter;

	@Inject
	IResourceSetProvider provider;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection) selection;
			Object element = ssel.getFirstElement();
			if (element instanceof IFile
					&& ((IFile) element).getFileExtension().equals(
							"mperspective")) {
				BusyIndicator.showWhile(Display.getCurrent(), new Runnable() {
					public void run() {
						try {

							IFile file = (IFile) element;
							ResourceSet rs = provider.get(file.getProject());

							// read the resource
							//
							URI inURI = org.eclipse.emf.common.util.URI
									.createPlatformResourceURI(file
											.getFullPath().toString(), false);
							Resource inResource = rs.getResource(inURI, true);

							String modelPath = Platform
									.getPreferencesService()
									.getString(
											"org.fhsolution.eclipse.plugins.csvedit",
											"perspectiveModelPath", "", null);

							URI outURI = toPlatform(modelPath);

							Resource outResource = null;
							try {
								outResource = rs.getResource(outURI, true);
							} catch (Exception e) {
								outResource = rs.createResource(outURI);
							}

							if (outResource == null) {
								return;
							}

							// convert the model
							//
							PerspectiveModel newPerspectiveModel = converter
									.toModel((MPerspective) inResource
											.getContents().get(0));

							if (outResource.isLoaded()) {
								PerspectiveModel current = (PerspectiveModel) outResource
										.getContents().get(0);
								PerspectivePackage currentPkg = current
										.getPackages().get(0);

								PerspectivePackage newPkg = newPerspectiveModel
										.getPackages().get(0);

								// copy the missing packages
								for (OXImportDeclaration convertedImport : getImportDeclarations(newPerspectiveModel)) {
									boolean contains = false;
									for (OXImportDeclaration currentImport : new ArrayList<>(
											getImportDeclarations(current))) {

										if (convertedImport.isFqnImport()
												&& currentImport.isFqnImport()) {
											if (convertedImport
													.getImportedFullyQualifiedName()
													.equals(currentImport
															.getImportedFullyQualifiedName())) {
												contains = true;
												break;
											}
										} else if (convertedImport.isWildcard()
												&& currentImport.isWildcard()) {
											if (convertedImport
													.getImportedNamespace()
													.equals(currentImport
															.getImportedNamespace())) {
												contains = true;
												break;
											}
										} else if (convertedImport
												.getImportedType() != null
												&& currentImport
														.getImportedType() != null) {
											if (convertedImport
													.getImportedType()
													.getQualifiedName()
													.equals(currentImport
															.getImportedType()
															.getQualifiedName())) {
												contains = true;
												break;
											}
										}
									}

									if (!contains) {
										if (current.getImportSection() == null) {
											current.setImportSection(XtypeFactory.eINSTANCE
													.createXImportSection());
										}
										current.getImportSection()
												.getImportDeclarations()
												.add(convertedImport);
									}
								}

								for (Perspective newPersp : new ArrayList<>(
										newPkg.getPerspectives())) {
									boolean replaced = false;
									for (Perspective currentPerspective : new ArrayList<>(
											currentPkg.getPerspectives())) {
										if (newPersp.getName().equals(
												currentPerspective.getName())) {
											currentPkg.getPerspectives()
													.remove(currentPerspective);
											currentPkg.getPerspectives().add(
													newPersp);
											replaced = true;
											break;
										}
									}

									if (!replaced) {
										currentPkg.getPerspectives().add(
												newPersp);
									}
								}

								outResource.save(null);
							} else {
								outResource.getContents().add(
										newPerspectiveModel);
								outResource.save(null);
							}

						} catch (Exception e) {
							MessageDialog.openError(Display.getCurrent()
									.getActiveShell(),
									"Conversion to perspective", e.toString());
							throw new RuntimeException(e);
						}
					}
				});

			}
		}
		return null;
	}

	protected URI toPlatform(String fileURIString) {
		URI path = URI.createFileURI(fileURIString);

		IProject project = null;
		String modelFile = null;
		// find the bundle segment
		for (IProject proj : ResourcesPlugin.getWorkspace().getRoot()
				.getProjects()) {
			for (int i = 0; i < path.segmentCount(); i++) {
				if (modelFile != null) {
					modelFile += "/" + path.segment(i);
				}
				if (proj.getName().equals(path.segment(i))) {
					project = proj;
					modelFile = "";
				}
			}
			if (modelFile != null) {
				break;
			}
		}
		return project != null ? URI.createPlatformResourceURI(project
				.getFullPath().toString() + modelFile, false) : null;
	}

	protected List<OXImportDeclaration> getImportDeclarations(
			PerspectiveModel model) {
		if (model.getImportSection() == null) {
			return Collections.emptyList();
		}

		List<OXImportDeclaration> result = new ArrayList<>();
		for (XImportDeclaration decl : model.getImportSection()
				.getImportDeclarations()) {
			result.add((OXImportDeclaration) decl);
		}
		return result;
	}
}
