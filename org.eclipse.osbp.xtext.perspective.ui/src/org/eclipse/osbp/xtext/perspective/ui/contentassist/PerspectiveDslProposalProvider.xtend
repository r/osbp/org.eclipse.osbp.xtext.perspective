/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.perspective.ui.contentassist

import com.google.inject.Inject
import org.eclipse.bpmn2.impl.ProcessImpl
import org.eclipse.bpmn2.impl.UserTaskImpl
import org.eclipse.emf.ecore.EObject
import org.eclipse.jface.viewers.StyledString
import org.eclipse.osbp.xtext.basic.ui.contentassist.BasicDSLProposalProviderHelper
import org.eclipse.osbp.xtext.perspective.PerspectivePart
import org.eclipse.osbp.xtext.perspective.PerspectivePartStack
import org.eclipse.osbp.xtext.perspective.ui.PerspectiveDSLDocumentationTranslator
import org.eclipse.swt.layout.FillLayout
import org.eclipse.swt.widgets.Shell
import org.eclipse.xtext.Assignment
import org.eclipse.xtext.Keyword
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor
import org.eclipse.xtext.ui.editor.contentassist.ReplacementTextApplier
import org.mihalis.opal.imageSelector.ImageSelectorDialog
import org.eclipse.osbp.xtext.basic.ui.contentassist.TerminalsProposalProviderWithDescription

class IconNameTextApplier extends ReplacementTextApplier {
	var ContentAssistContext context
	var String[] extensions

	def setContext(ContentAssistContext context) {
		this.context = context
	}

	def setExtensions(String[] fileExtensions) {
		extensions = fileExtensions
	}

	// this will inject a file dialog when selecting the file picker proposal 
	override getActualReplacementString(ConfigurableCompletionProposal proposal) {
		var display = context.getViewer().getTextWidget().getDisplay();
		var shell = new Shell(display);
		shell.setLayout(new FillLayout());
		var imageSelectorDialog = new ImageSelectorDialog(shell, 16);
		imageSelectorDialog.setFilterExtensions(extensions)
		var imageFileName = imageSelectorDialog.open(true);
		return if (imageFileName !== null) "\"".concat(imageFileName).concat("\"") else "";
	}

}

class PerspectiveDslProposalProvider extends AbstractPerspectiveDslProposalProvider {

	@Inject TerminalsProposalProviderWithDescription provider
	@Inject BasicDSLProposalProviderHelper providerHelper

	def StyledString getProposalString(ProcessImpl process, UserTaskImpl userTask) {
		return new StyledString('''«userTask.name» in process «process.id»''')
	}

	/**
	 * This override will enable 1 length non letter characters as keyword.
	 */
	override protected boolean isKeywordWorthyToPropose(Keyword keyword) {
		return true
	}

	def iconPickerProposal(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor, String fileExtensions) {
		var fileName = createCompletionProposal("Select icon...", context) as ConfigurableCompletionProposal
		if (fileName != null) {
			var applier = new IconNameTextApplier()
			applier.setExtensions(fileExtensions.split(","))
			applier.setContext(context)
			fileName.setTextApplier = applier
		}
		acceptor.accept(fileName)
	}

	override completePerspective_IconURI(
		EObject model,
		Assignment assignment,
		ContentAssistContext context,
		ICompletionProposalAcceptor acceptor
	) {
		iconPickerProposal(model, assignment, context, acceptor, ".png")
	}

	override completePerspectivePart_IconURI(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		iconPickerProposal(model, assignment, context, acceptor, ".png")
	}

	override complete_ValidID(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		complete_ID(model, ruleCall, context, acceptor)
	}

	override complete_TRANSLATABLEID(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		complete_ID(model, ruleCall, context, acceptor)
	}

	override protected boolean isValidProposal(String proposal, String prefix, ContentAssistContext context) {
		var result = super.isValidProposal(proposal, prefix, context)
		if (context.getCurrentModel() instanceof PerspectivePartStack) {
			if ("partStack".equals(proposal) || "sashContainer".equals(proposal)) {
				return false
			}
			return true
		}
		if (context.getCurrentModel() instanceof PerspectivePart) {
			if ("part".equals(proposal) || "partStack".equals(proposal) || "sashContainer".equals(proposal)) {
				return false
			}
			return true
		}
		return result
	}

	override protected StyledString getKeywordDisplayString(Keyword keyword) {
		return BasicDSLProposalProviderHelper.getKeywordDisplayString(keyword,
			PerspectiveDSLDocumentationTranslator.instance());
	}

	override public void complete_QualifiedName(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		providerHelper.complete_PackageName(model, ruleCall, context, acceptor, this)
	}

	// ------------------------ delegates to TerminalsProposalProvider -----------------
	override complete_ID(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		provider.setDocumentationTranslator(PerspectiveDSLDocumentationTranslator.instance());
		provider.complete_ID(model, ruleCall, context, acceptor)
	}

	override complete_TRANSLATABLESTRING(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		provider.complete_STRING(model, ruleCall, context, acceptor)
	}

	override public void completePerspectiveSashContainer_ContainerData(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		provider.complete_STRING(model, (assignment.getTerminal() as RuleCall), context, acceptor)
	}

	override public void completePerspectivePart_ContainerData(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		provider.complete_STRING(model, (assignment.getTerminal() as RuleCall), context, acceptor)
	}

	override public void completePerspectivePartStack_ContainerData(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		provider.complete_STRING(model, (assignment.getTerminal() as RuleCall), context, acceptor)
	}

	override public void completePerspective_AccessibilityPhrase(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		provider.complete_STRING(model, (assignment.getTerminal() as RuleCall), context, acceptor)
	}

	override public void completePerspectiveSashContainer_AccessibilityPhrase(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		provider.complete_STRING(model, (assignment.getTerminal() as RuleCall), context, acceptor)
	}

	override public void completePerspectivePart_AccessibilityPhrase(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		provider.complete_STRING(model, (assignment.getTerminal() as RuleCall), context, acceptor)
	}

	override public void completePerspectivePartStack_AccessibilityPhrase(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		provider.complete_STRING(model, (assignment.getTerminal() as RuleCall), context, acceptor)
	}
}
