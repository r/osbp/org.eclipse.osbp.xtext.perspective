/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
package org.eclipse.osbp.xtext.perspective.ui.converter

import com.google.inject.Inject
import java.util.Map
import java.util.Set
import org.eclipse.e4.ui.model.application.ui.MUIElement
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective
import org.eclipse.e4.ui.model.application.ui.basic.MPart
import org.eclipse.e4.ui.model.application.ui.basic.MPartSashContainer
import org.eclipse.e4.ui.model.application.ui.basic.MPartSashContainerElement
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack
import org.eclipse.e4.ui.model.application.ui.basic.MStackElement
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EPackage
import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesFactory
import org.eclipse.osbp.xtext.chart.Chart
import org.eclipse.osbp.xtext.chart.ChartDSLPackage
import org.eclipse.osbp.xtext.dialogdsl.Dialog
import org.eclipse.osbp.xtext.dialogdsl.DialogDSLPackage
import org.eclipse.osbp.xtext.perspective.Perspective
import org.eclipse.osbp.xtext.perspective.PerspectiveChart
import org.eclipse.osbp.xtext.perspective.PerspectiveDialog
import org.eclipse.osbp.xtext.perspective.PerspectiveDslFactory
import org.eclipse.osbp.xtext.perspective.PerspectiveElement
import org.eclipse.osbp.xtext.perspective.PerspectiveModel
import org.eclipse.osbp.xtext.perspective.PerspectivePart
import org.eclipse.osbp.xtext.perspective.PerspectiveReport
import org.eclipse.osbp.xtext.perspective.PerspectiveTable
import org.eclipse.osbp.xtext.perspective.SashOrientation
import org.eclipse.osbp.xtext.reportdsl.Report
import org.eclipse.osbp.xtext.reportdsl.ReportDSLPackage
import org.eclipse.osbp.xtext.table.Table
import org.eclipse.osbp.xtext.table.TableDSLPackage
import org.eclipse.xtext.naming.QualifiedName
import org.eclipse.xtext.resource.IResourceDescriptions
import org.eclipse.osbp.xtext.perspective.PerspectiveGrid
import org.eclipse.xtext.xtype.XtypeFactory
import org.eclipse.osbp.xtext.oxtype.oxtype.OXtypeFactory

/**
 * This class converts a given E4-Perspective to an OSBP perspective model or dsl text.
 */
class E4PerspectiveToDSLConverter {

	private static final String OSBP_NS_URI = "osbp-NS-Uri";
	private static final String OSBP_FQN = "osbp-model-fqn";
	private static final String OSBP_FLAVOR = "osbp-NS-Uri-flavor";

	@Inject
	private IResourceDescriptions descriptions

	private PerspectiveDslFactory pFactory = PerspectiveDslFactory.eINSTANCE

	private Set<String> imports = newHashSet()

	private Map<MUIElement, EObject> elements = newHashMap()

	private int counter;

	def PerspectiveModel toModel(MPerspective mPerspective) {
		counter = 0;
		imports.clear

		val oModel = pFactory.createPerspectiveModel
		oModel.importSection = XtypeFactory.eINSTANCE.createXImportSection
		val importSection = oModel.importSection
		
		val oPkg = pFactory.createPerspectivePackage
		oPkg.name = "to.be.defined"
		oModel.packages += oPkg
		
		val perspective = mPerspective.convertPerspective
		oPkg.perspectives += perspective

		imports.forEach [
			val imp = OXtypeFactory.eINSTANCE.createOXImportDeclaration
			imp.importedNamespace = it + ".*"
			imp.wildcard = true
			importSection.importDeclarations += imp
		]

		return oModel
	}

	def Perspective convertPerspective(MPerspective mPerspective) {

		val Perspective oPerspective = pFactory.createPerspective
		mPerspective.register(oPerspective)
		oPerspective.accessibilityPhrase = mPerspective.accessibilityPhrase
		oPerspective.iconURI = mPerspective.iconURI
		oPerspective.name = mPerspective.label

		mPerspective.children.forEach [
			val result = it.convert
			if (result != null) {
				oPerspective.elements += result
			}
		]

		return oPerspective
	}

	def dispatch PerspectiveElement convert(MPartSashContainerElement element) {
		return null
	}

	def dispatch PerspectiveElement convert(MStackElement element) {
		return null
	}

	def dispatch PerspectiveElement convert(MPartSashContainer mElement) {
		val oElement = pFactory.createPerspectiveSashContainer
		mElement.register(oElement)
		oElement.accessibilityPhrase = mElement.accessibilityPhrase
		oElement.containerData = mElement.containerData
		oElement.elementId = toId
		oElement.orientation = mElement.horizontal.toOrientation

		mElement.children.forEach [
			val result = it.convert
			if (result != null) {
				oElement.elements += result
			}
		]

		return oElement
	}

	def String toId() '''e«counter++»'''

	def dispatch PerspectiveElement convert(MPartStack mElement) {
		val oElement = pFactory.createPerspectivePartStack
		mElement.register(oElement)
		oElement.accessibilityPhrase = mElement.accessibilityPhrase
		oElement.containerData = mElement.containerData
		oElement.elementId = toId

		mElement.children.forEach [
			val result = it.convert
			if (result != null) {
				oElement.elements += result
			}
		]

		return oElement
	}

	def dispatch PerspectiveElement convert(MPart mElement) {
		val String nsURI = mElement.getPersistedState().get(OSBP_NS_URI);
		val EPackage ePkg = EPackage.Registry.INSTANCE.getEPackage(nsURI)
		if (ePkg == null) {
			return null
		}

		val oElement = pFactory.createPerspectivePart
		mElement.register(oElement)
		oElement.accessibilityPhrase = mElement.accessibilityPhrase
		oElement.containerData = mElement.containerData
		if (mElement.label == null) {
			oElement.elementId = toId
		} else {
			oElement.elementId = mElement.label.replaceAll(" ", "").replaceAll("(\\W)", "")
		}
		oElement.iconURI = mElement.iconURI
		oElement.isClosable = mElement.closeable

		ePkg.convertView(mElement, oElement)

		return oElement
	}

	def dispatch void convertView(EPackage ePackage, MPart mPart, PerspectivePart oPart) {
	}

	def dispatch void convertView(TableDSLPackage ePackage, MPart mPart, PerspectivePart oPart) {

		val String flavor = mPart.persistedState.get(OSBP_FLAVOR);
		if (flavor != null && flavor.equals("grid")) {
			val PerspectiveGrid oTable = pFactory.createPerspectiveGrid
			oPart.view = oTable
			val String fqn = mPart.persistedState.get(OSBP_FQN);
			if (fqn.nullOrEmpty) {
				return
			}
			imports += fqn.substring(0, fqn.lastIndexOf("."))

			val result = descriptions.getExportedObjects(TableDSLPackage.Literals.TABLE,
				QualifiedName.create(fqn.split("\\.")), false)
			if (result.iterator.hasNext) {
				val eObjectDesc = result.iterator.next
				oTable.ref = eObjectDesc.EObjectOrProxy as Table
			}
		} else {
			val PerspectiveTable oTable = pFactory.createPerspectiveTable
			oPart.view = oTable
			val String fqn = mPart.persistedState.get(OSBP_FQN);
			if (fqn.nullOrEmpty) {
				return
			}
			imports += fqn.substring(0, fqn.lastIndexOf("."))

			val result = descriptions.getExportedObjects(TableDSLPackage.Literals.TABLE,
				QualifiedName.create(fqn.split("\\.")), false)
			if (result.iterator.hasNext) {
				val eObjectDesc = result.iterator.next
				oTable.ref = eObjectDesc.EObjectOrProxy as Table
			}
		}
	}

	def dispatch void convertView(ChartDSLPackage ePackage, MPart mPart, PerspectivePart oPart) {
		val PerspectiveChart oChart = pFactory.createPerspectiveChart
		oPart.view = oChart
		val String fqn = mPart.persistedState.get(E4PerspectiveToDSLConverter.OSBP_FQN);
		if (fqn.nullOrEmpty) {
			return
		}
		imports += fqn.substring(0, fqn.lastIndexOf("."))

		val tokens = fqn.split("\\.")
		val result = descriptions.getExportedObjects(ChartDSLPackage.Literals.CHART,
			QualifiedName.create(tokens.get(tokens.length - 1)), false)
		if (result.iterator.hasNext) {
			val eObjectDesc = result.iterator.next
			oChart.ref = eObjectDesc.EObjectOrProxy as Chart
		}
	}

	def dispatch void convertView(ReportDSLPackage ePackage, MPart mPart, PerspectivePart oPart) {

		val PerspectiveReport oReport = pFactory.createPerspectiveReport
		oPart.view = oReport
		val String fqn = mPart.persistedState.get(E4PerspectiveToDSLConverter.OSBP_FQN);
		if (fqn.nullOrEmpty) {
			return
		}
		imports += fqn.substring(0, fqn.lastIndexOf("."))

		val result = descriptions.getExportedObjects(ReportDSLPackage.Literals.REPORT,
			QualifiedName.create(fqn.split("\\.")), false)
		if (result.iterator.hasNext) {
			val eObjectDesc = result.iterator.next
			oReport.ref = eObjectDesc.EObjectOrProxy as Report
		}

	}

	def dispatch void convertView(DialogDSLPackage ePackage, MPart mPart, PerspectivePart oPart) {
		val PerspectiveDialog oDialog = pFactory.createPerspectiveDialog
		oPart.view = oDialog
		val String fqn = mPart.persistedState.get(E4PerspectiveToDSLConverter.OSBP_FQN);
		if (fqn.nullOrEmpty) {
			return
		}
		imports += fqn.substring(0, fqn.lastIndexOf("."))

		val result = descriptions.getExportedObjects(DialogDSLPackage.Literals.DIALOG,
			QualifiedName.create(fqn.split("\\.")), false)
		if (result.iterator.hasNext) {
			val eObjectDesc = result.iterator.next
			oDialog.ref = eObjectDesc.EObjectOrProxy as Dialog
		}
	}

	def SashOrientation toOrientation(boolean isHorizontal) {
		if(isHorizontal) return SashOrientation.HORIZONTAL else SashOrientation.VERTICAL
	}

	def void register(MUIElement mElement, EObject oElement) {
		elements.put(mElement, oElement)
	}
}
