/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.perspective.ui.labeling

import com.google.inject.Inject
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider
import org.eclipse.osbp.xtext.basic.ui.labeling.BasicDSLLabelProvider
import org.eclipse.osbp.xtext.perspective.Perspective
import org.eclipse.osbp.xtext.perspective.PerspectiveChart
import org.eclipse.osbp.xtext.perspective.PerspectiveDialog
import org.eclipse.osbp.xtext.perspective.PerspectiveGrid
import org.eclipse.osbp.xtext.perspective.PerspectiveModel
import org.eclipse.osbp.xtext.perspective.PerspectiveOrganization
import org.eclipse.osbp.xtext.perspective.PerspectivePackage
import org.eclipse.osbp.xtext.perspective.PerspectivePart
import org.eclipse.osbp.xtext.perspective.PerspectivePartStack
import org.eclipse.osbp.xtext.perspective.PerspectiveReport
import org.eclipse.osbp.xtext.perspective.PerspectiveSashContainer
import org.eclipse.osbp.xtext.perspective.PerspectiveSelection
import org.eclipse.osbp.xtext.perspective.PerspectiveTable
import org.eclipse.osbp.xtext.perspective.PerspectiveTopology

/**
 * Provides labels for a EObjects.
 *
 * see http://www.eclipse.org/Xtext/documentation.html#labelProvider
 */
class PerspectiveDslLabelProvider extends BasicDSLLabelProvider {

	@Inject
	new(AdapterFactoryLabelProvider delegate) {
		super(delegate);
	}

	// Labels and icons can be computed like this:

	 override text ( Object o ) {
		switch (o) {
			PerspectivePackage			: generateText( o, 'package'      , o.name ) 
			Perspective					: generateText( o, 'perspective'  , o.name )
			PerspectiveSashContainer	: generateText( o, 'sashContainer', o.elementId )
			PerspectivePartStack		: generateText( o, 'partStack'    , o.elementId )
			PerspectivePart				: generateText( o, 'part'         , o.elementId )
			PerspectiveSelection		: generateText( o, 'select'       , o.ref.name )
			PerspectiveTable			: generateText( o, 'table'        , o.ref.name )
			PerspectiveGrid				: generateText( o, 'grid'         , o.ref.name )
			PerspectiveChart			: generateText( o, 'chart'        , o.ref.name )
			PerspectiveReport			: generateText( o, 'report'       , o.ref.name )
			PerspectiveOrganization		: generateText( o, 'organigram'   , o.ref.name )
			PerspectiveTopology			: generateText( o, 'topology'     , o.ref.name )
			PerspectiveDialog			: generateText( o, 'dialog'       , o.ref.name )
			default						: super.text( o )
		}
	}

	override image ( Object o ) {
		switch (o) {
			PerspectiveModel			: getInternalImage( 'model.png'            , class )
			PerspectivePackage			: getInternalImage( 'package.gif'          , class )
			Perspective					: getInternalImage( 'dsl_perspective.png'  , class )
//			PerspectiveSashContainer	: getInternalImage( '.png'                 , class )
//			PerspectivePartStack		: getInternalImage( '.png'                 , class )
			PerspectiveSelection		: getInternalImage( 'dsl_select.png'       , class )
			PerspectiveTable			: getInternalImage( 'dsl_table.png'        , class )
			PerspectiveGrid				: getInternalImage( 'CxGridSourceInput.png', class )
			PerspectiveChart			: getInternalImage( 'dsl_chart.png'        , class )
			PerspectiveReport			: getInternalImage( 'dsl_report.gif'       , class )
			PerspectiveOrganization		: getInternalImage( 'dsl_organigram.png'   , class )
			PerspectiveTopology			: getInternalImage( 'dsl_topology.png'     , class )
			PerspectiveDialog			: getInternalImage( 'dsl_dialog.gif'       , class )
			default						: super.image( o )
		}
	}
}
