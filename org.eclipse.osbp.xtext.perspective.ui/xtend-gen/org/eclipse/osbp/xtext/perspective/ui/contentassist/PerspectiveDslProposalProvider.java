/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.perspective.ui.contentassist;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import org.eclipse.bpmn2.impl.ProcessImpl;
import org.eclipse.bpmn2.impl.UserTaskImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.osbp.xtext.basic.ui.contentassist.BasicDSLProposalProviderHelper;
import org.eclipse.osbp.xtext.basic.ui.contentassist.TerminalsProposalProviderWithDescription;
import org.eclipse.osbp.xtext.perspective.PerspectivePart;
import org.eclipse.osbp.xtext.perspective.PerspectivePartStack;
import org.eclipse.osbp.xtext.perspective.ui.PerspectiveDSLDocumentationTranslator;
import org.eclipse.osbp.xtext.perspective.ui.contentassist.AbstractPerspectiveDslProposalProvider;
import org.eclipse.osbp.xtext.perspective.ui.contentassist.IconNameTextApplier;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor;

@SuppressWarnings("all")
public class PerspectiveDslProposalProvider extends AbstractPerspectiveDslProposalProvider {
  @Inject
  private TerminalsProposalProviderWithDescription provider;
  
  @Inject
  private BasicDSLProposalProviderHelper providerHelper;
  
  public StyledString getProposalString(final ProcessImpl process, final UserTaskImpl userTask) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = userTask.getName();
    _builder.append(_name);
    _builder.append(" in process ");
    String _id = process.getId();
    _builder.append(_id);
    return new StyledString(_builder.toString());
  }
  
  /**
   * This override will enable 1 length non letter characters as keyword.
   */
  @Override
  protected boolean isKeywordWorthyToPropose(final Keyword keyword) {
    return true;
  }
  
  public void iconPickerProposal(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor, final String fileExtensions) {
    ICompletionProposal _createCompletionProposal = this.createCompletionProposal("Select icon...", context);
    ConfigurableCompletionProposal fileName = ((ConfigurableCompletionProposal) _createCompletionProposal);
    boolean _notEquals = (!Objects.equal(fileName, null));
    if (_notEquals) {
      IconNameTextApplier applier = new IconNameTextApplier();
      applier.setExtensions(fileExtensions.split(","));
      applier.setContext(context);
      fileName.setTextApplier(applier);
    }
    acceptor.accept(fileName);
  }
  
  @Override
  public void completePerspective_IconURI(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.iconPickerProposal(model, assignment, context, acceptor, ".png");
  }
  
  @Override
  public void completePerspectivePart_IconURI(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.iconPickerProposal(model, assignment, context, acceptor, ".png");
  }
  
  @Override
  public void complete_ValidID(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.complete_ID(model, ruleCall, context, acceptor);
  }
  
  @Override
  public void complete_TRANSLATABLEID(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.complete_ID(model, ruleCall, context, acceptor);
  }
  
  @Override
  protected boolean isValidProposal(final String proposal, final String prefix, final ContentAssistContext context) {
    boolean result = super.isValidProposal(proposal, prefix, context);
    EObject _currentModel = context.getCurrentModel();
    if ((_currentModel instanceof PerspectivePartStack)) {
      if (("partStack".equals(proposal) || "sashContainer".equals(proposal))) {
        return false;
      }
      return true;
    }
    EObject _currentModel_1 = context.getCurrentModel();
    if ((_currentModel_1 instanceof PerspectivePart)) {
      if ((("part".equals(proposal) || "partStack".equals(proposal)) || "sashContainer".equals(proposal))) {
        return false;
      }
      return true;
    }
    return result;
  }
  
  @Override
  protected StyledString getKeywordDisplayString(final Keyword keyword) {
    return BasicDSLProposalProviderHelper.getKeywordDisplayString(keyword, 
      PerspectiveDSLDocumentationTranslator.instance());
  }
  
  @Override
  public void complete_QualifiedName(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.providerHelper.complete_PackageName(model, ruleCall, context, acceptor, this);
  }
  
  @Override
  public void complete_ID(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.provider.setDocumentationTranslator(PerspectiveDSLDocumentationTranslator.instance());
    this.provider.complete_ID(model, ruleCall, context, acceptor);
  }
  
  @Override
  public void complete_TRANSLATABLESTRING(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.provider.complete_STRING(model, ruleCall, context, acceptor);
  }
  
  @Override
  public void completePerspectiveSashContainer_ContainerData(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    AbstractElement _terminal = assignment.getTerminal();
    this.provider.complete_STRING(model, ((RuleCall) _terminal), context, acceptor);
  }
  
  @Override
  public void completePerspectivePart_ContainerData(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    AbstractElement _terminal = assignment.getTerminal();
    this.provider.complete_STRING(model, ((RuleCall) _terminal), context, acceptor);
  }
  
  @Override
  public void completePerspectivePartStack_ContainerData(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    AbstractElement _terminal = assignment.getTerminal();
    this.provider.complete_STRING(model, ((RuleCall) _terminal), context, acceptor);
  }
  
  @Override
  public void completePerspective_AccessibilityPhrase(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    AbstractElement _terminal = assignment.getTerminal();
    this.provider.complete_STRING(model, ((RuleCall) _terminal), context, acceptor);
  }
  
  @Override
  public void completePerspectiveSashContainer_AccessibilityPhrase(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    AbstractElement _terminal = assignment.getTerminal();
    this.provider.complete_STRING(model, ((RuleCall) _terminal), context, acceptor);
  }
  
  @Override
  public void completePerspectivePart_AccessibilityPhrase(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    AbstractElement _terminal = assignment.getTerminal();
    this.provider.complete_STRING(model, ((RuleCall) _terminal), context, acceptor);
  }
  
  @Override
  public void completePerspectivePartStack_AccessibilityPhrase(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    AbstractElement _terminal = assignment.getTerminal();
    this.provider.complete_STRING(model, ((RuleCall) _terminal), context, acceptor);
  }
}
