/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.perspective.ui.converter;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartSashContainer;
import org.eclipse.e4.ui.model.application.ui.basic.MPartSashContainerElement;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.model.application.ui.basic.MStackElement;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.osbp.xtext.chart.Chart;
import org.eclipse.osbp.xtext.chart.ChartDSLPackage;
import org.eclipse.osbp.xtext.dialogdsl.Dialog;
import org.eclipse.osbp.xtext.dialogdsl.DialogDSLPackage;
import org.eclipse.osbp.xtext.oxtype.oxtype.OXImportDeclaration;
import org.eclipse.osbp.xtext.oxtype.oxtype.OXtypeFactory;
import org.eclipse.osbp.xtext.perspective.Perspective;
import org.eclipse.osbp.xtext.perspective.PerspectiveChart;
import org.eclipse.osbp.xtext.perspective.PerspectiveDialog;
import org.eclipse.osbp.xtext.perspective.PerspectiveDslFactory;
import org.eclipse.osbp.xtext.perspective.PerspectiveElement;
import org.eclipse.osbp.xtext.perspective.PerspectiveGrid;
import org.eclipse.osbp.xtext.perspective.PerspectiveModel;
import org.eclipse.osbp.xtext.perspective.PerspectivePackage;
import org.eclipse.osbp.xtext.perspective.PerspectivePart;
import org.eclipse.osbp.xtext.perspective.PerspectivePartStack;
import org.eclipse.osbp.xtext.perspective.PerspectiveReport;
import org.eclipse.osbp.xtext.perspective.PerspectiveSashContainer;
import org.eclipse.osbp.xtext.perspective.PerspectiveTable;
import org.eclipse.osbp.xtext.perspective.SashOrientation;
import org.eclipse.osbp.xtext.reportdsl.Report;
import org.eclipse.osbp.xtext.reportdsl.ReportDSLPackage;
import org.eclipse.osbp.xtext.table.Table;
import org.eclipse.osbp.xtext.table.TableDSLPackage;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.StringExtensions;
import org.eclipse.xtext.xtype.XImportDeclaration;
import org.eclipse.xtext.xtype.XImportSection;
import org.eclipse.xtext.xtype.XtypeFactory;

/**
 * This class converts a given E4-Perspective to an OSBP perspective model or dsl text.
 */
@SuppressWarnings("all")
public class E4PerspectiveToDSLConverter {
  private final static String OSBP_NS_URI = "osbp-NS-Uri";
  
  private final static String OSBP_FQN = "osbp-model-fqn";
  
  private final static String OSBP_FLAVOR = "osbp-NS-Uri-flavor";
  
  @Inject
  private IResourceDescriptions descriptions;
  
  private PerspectiveDslFactory pFactory = PerspectiveDslFactory.eINSTANCE;
  
  private Set<String> imports = CollectionLiterals.<String>newHashSet();
  
  private Map<MUIElement, EObject> elements = CollectionLiterals.<MUIElement, EObject>newHashMap();
  
  private int counter;
  
  public PerspectiveModel toModel(final MPerspective mPerspective) {
    this.counter = 0;
    this.imports.clear();
    final PerspectiveModel oModel = this.pFactory.createPerspectiveModel();
    oModel.setImportSection(XtypeFactory.eINSTANCE.createXImportSection());
    final XImportSection importSection = oModel.getImportSection();
    final PerspectivePackage oPkg = this.pFactory.createPerspectivePackage();
    oPkg.setName("to.be.defined");
    EList<PerspectivePackage> _packages = oModel.getPackages();
    _packages.add(oPkg);
    final Perspective perspective = this.convertPerspective(mPerspective);
    EList<Perspective> _perspectives = oPkg.getPerspectives();
    _perspectives.add(perspective);
    final Consumer<String> _function = (String it) -> {
      final OXImportDeclaration imp = OXtypeFactory.eINSTANCE.createOXImportDeclaration();
      imp.setImportedNamespace((it + ".*"));
      imp.setWildcard(true);
      EList<XImportDeclaration> _importDeclarations = importSection.getImportDeclarations();
      _importDeclarations.add(imp);
    };
    this.imports.forEach(_function);
    return oModel;
  }
  
  public Perspective convertPerspective(final MPerspective mPerspective) {
    final Perspective oPerspective = this.pFactory.createPerspective();
    this.register(mPerspective, oPerspective);
    oPerspective.setAccessibilityPhrase(mPerspective.getAccessibilityPhrase());
    oPerspective.setIconURI(mPerspective.getIconURI());
    oPerspective.setName(mPerspective.getLabel());
    final Consumer<MPartSashContainerElement> _function = (MPartSashContainerElement it) -> {
      final PerspectiveElement result = this.convert(it);
      boolean _notEquals = (!Objects.equal(result, null));
      if (_notEquals) {
        EList<PerspectiveElement> _elements = oPerspective.getElements();
        _elements.add(result);
      }
    };
    mPerspective.getChildren().forEach(_function);
    return oPerspective;
  }
  
  protected PerspectiveElement _convert(final MPartSashContainerElement element) {
    return null;
  }
  
  protected PerspectiveElement _convert(final MStackElement element) {
    return null;
  }
  
  protected PerspectiveElement _convert(final MPartSashContainer mElement) {
    final PerspectiveSashContainer oElement = this.pFactory.createPerspectiveSashContainer();
    this.register(mElement, oElement);
    oElement.setAccessibilityPhrase(mElement.getAccessibilityPhrase());
    oElement.setContainerData(mElement.getContainerData());
    oElement.setElementId(this.toId());
    oElement.setOrientation(this.toOrientation(mElement.isHorizontal()));
    final Consumer<MPartSashContainerElement> _function = (MPartSashContainerElement it) -> {
      final PerspectiveElement result = this.convert(it);
      boolean _notEquals = (!Objects.equal(result, null));
      if (_notEquals) {
        EList<PerspectiveElement> _elements = oElement.getElements();
        _elements.add(result);
      }
    };
    mElement.getChildren().forEach(_function);
    return oElement;
  }
  
  public String toId() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("e");
    int _plusPlus = this.counter++;
    _builder.append(_plusPlus);
    return _builder.toString();
  }
  
  protected PerspectiveElement _convert(final MPartStack mElement) {
    final PerspectivePartStack oElement = this.pFactory.createPerspectivePartStack();
    this.register(mElement, oElement);
    oElement.setAccessibilityPhrase(mElement.getAccessibilityPhrase());
    oElement.setContainerData(mElement.getContainerData());
    oElement.setElementId(this.toId());
    final Consumer<MStackElement> _function = (MStackElement it) -> {
      final PerspectiveElement result = this.convert(it);
      boolean _notEquals = (!Objects.equal(result, null));
      if (_notEquals) {
        EList<PerspectiveElement> _elements = oElement.getElements();
        _elements.add(result);
      }
    };
    mElement.getChildren().forEach(_function);
    return oElement;
  }
  
  protected PerspectiveElement _convert(final MPart mElement) {
    final String nsURI = mElement.getPersistedState().get(E4PerspectiveToDSLConverter.OSBP_NS_URI);
    final EPackage ePkg = EPackage.Registry.INSTANCE.getEPackage(nsURI);
    boolean _equals = Objects.equal(ePkg, null);
    if (_equals) {
      return null;
    }
    final PerspectivePart oElement = this.pFactory.createPerspectivePart();
    this.register(mElement, oElement);
    oElement.setAccessibilityPhrase(mElement.getAccessibilityPhrase());
    oElement.setContainerData(mElement.getContainerData());
    String _label = mElement.getLabel();
    boolean _equals_1 = Objects.equal(_label, null);
    if (_equals_1) {
      oElement.setElementId(this.toId());
    } else {
      oElement.setElementId(mElement.getLabel().replaceAll(" ", "").replaceAll("(\\W)", ""));
    }
    oElement.setIconURI(mElement.getIconURI());
    oElement.setIsClosable(mElement.isCloseable());
    this.convertView(ePkg, mElement, oElement);
    return oElement;
  }
  
  protected void _convertView(final EPackage ePackage, final MPart mPart, final PerspectivePart oPart) {
  }
  
  protected void _convertView(final TableDSLPackage ePackage, final MPart mPart, final PerspectivePart oPart) {
    final String flavor = mPart.getPersistedState().get(E4PerspectiveToDSLConverter.OSBP_FLAVOR);
    if (((!Objects.equal(flavor, null)) && flavor.equals("grid"))) {
      final PerspectiveGrid oTable = this.pFactory.createPerspectiveGrid();
      oPart.setView(oTable);
      final String fqn = mPart.getPersistedState().get(E4PerspectiveToDSLConverter.OSBP_FQN);
      boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(fqn);
      if (_isNullOrEmpty) {
        return;
      }
      String _substring = fqn.substring(0, fqn.lastIndexOf("."));
      this.imports.add(_substring);
      final Iterable<IEObjectDescription> result = this.descriptions.getExportedObjects(TableDSLPackage.Literals.TABLE, 
        QualifiedName.create(fqn.split("\\.")), false);
      boolean _hasNext = result.iterator().hasNext();
      if (_hasNext) {
        final IEObjectDescription eObjectDesc = result.iterator().next();
        EObject _eObjectOrProxy = eObjectDesc.getEObjectOrProxy();
        oTable.setRef(((Table) _eObjectOrProxy));
      }
    } else {
      final PerspectiveTable oTable_1 = this.pFactory.createPerspectiveTable();
      oPart.setView(oTable_1);
      final String fqn_1 = mPart.getPersistedState().get(E4PerspectiveToDSLConverter.OSBP_FQN);
      boolean _isNullOrEmpty_1 = StringExtensions.isNullOrEmpty(fqn_1);
      if (_isNullOrEmpty_1) {
        return;
      }
      String _substring_1 = fqn_1.substring(0, fqn_1.lastIndexOf("."));
      this.imports.add(_substring_1);
      final Iterable<IEObjectDescription> result_1 = this.descriptions.getExportedObjects(TableDSLPackage.Literals.TABLE, 
        QualifiedName.create(fqn_1.split("\\.")), false);
      boolean _hasNext_1 = result_1.iterator().hasNext();
      if (_hasNext_1) {
        final IEObjectDescription eObjectDesc_1 = result_1.iterator().next();
        EObject _eObjectOrProxy_1 = eObjectDesc_1.getEObjectOrProxy();
        oTable_1.setRef(((Table) _eObjectOrProxy_1));
      }
    }
  }
  
  protected void _convertView(final ChartDSLPackage ePackage, final MPart mPart, final PerspectivePart oPart) {
    final PerspectiveChart oChart = this.pFactory.createPerspectiveChart();
    oPart.setView(oChart);
    final String fqn = mPart.getPersistedState().get(E4PerspectiveToDSLConverter.OSBP_FQN);
    boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(fqn);
    if (_isNullOrEmpty) {
      return;
    }
    String _substring = fqn.substring(0, fqn.lastIndexOf("."));
    this.imports.add(_substring);
    final String[] tokens = fqn.split("\\.");
    int _length = tokens.length;
    int _minus = (_length - 1);
    final Iterable<IEObjectDescription> result = this.descriptions.getExportedObjects(ChartDSLPackage.Literals.CHART, 
      QualifiedName.create(tokens[_minus]), false);
    boolean _hasNext = result.iterator().hasNext();
    if (_hasNext) {
      final IEObjectDescription eObjectDesc = result.iterator().next();
      EObject _eObjectOrProxy = eObjectDesc.getEObjectOrProxy();
      oChart.setRef(((Chart) _eObjectOrProxy));
    }
  }
  
  protected void _convertView(final ReportDSLPackage ePackage, final MPart mPart, final PerspectivePart oPart) {
    final PerspectiveReport oReport = this.pFactory.createPerspectiveReport();
    oPart.setView(oReport);
    final String fqn = mPart.getPersistedState().get(E4PerspectiveToDSLConverter.OSBP_FQN);
    boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(fqn);
    if (_isNullOrEmpty) {
      return;
    }
    String _substring = fqn.substring(0, fqn.lastIndexOf("."));
    this.imports.add(_substring);
    final Iterable<IEObjectDescription> result = this.descriptions.getExportedObjects(ReportDSLPackage.Literals.REPORT, 
      QualifiedName.create(fqn.split("\\.")), false);
    boolean _hasNext = result.iterator().hasNext();
    if (_hasNext) {
      final IEObjectDescription eObjectDesc = result.iterator().next();
      EObject _eObjectOrProxy = eObjectDesc.getEObjectOrProxy();
      oReport.setRef(((Report) _eObjectOrProxy));
    }
  }
  
  protected void _convertView(final DialogDSLPackage ePackage, final MPart mPart, final PerspectivePart oPart) {
    final PerspectiveDialog oDialog = this.pFactory.createPerspectiveDialog();
    oPart.setView(oDialog);
    final String fqn = mPart.getPersistedState().get(E4PerspectiveToDSLConverter.OSBP_FQN);
    boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(fqn);
    if (_isNullOrEmpty) {
      return;
    }
    String _substring = fqn.substring(0, fqn.lastIndexOf("."));
    this.imports.add(_substring);
    final Iterable<IEObjectDescription> result = this.descriptions.getExportedObjects(DialogDSLPackage.Literals.DIALOG, 
      QualifiedName.create(fqn.split("\\.")), false);
    boolean _hasNext = result.iterator().hasNext();
    if (_hasNext) {
      final IEObjectDescription eObjectDesc = result.iterator().next();
      EObject _eObjectOrProxy = eObjectDesc.getEObjectOrProxy();
      oDialog.setRef(((Dialog) _eObjectOrProxy));
    }
  }
  
  public SashOrientation toOrientation(final boolean isHorizontal) {
    SashOrientation _xifexpression = null;
    if (isHorizontal) {
      return SashOrientation.HORIZONTAL;
    } else {
      _xifexpression = SashOrientation.VERTICAL;
    }
    return _xifexpression;
  }
  
  public void register(final MUIElement mElement, final EObject oElement) {
    this.elements.put(mElement, oElement);
  }
  
  public PerspectiveElement convert(final MUIElement mElement) {
    if (mElement instanceof MPartSashContainer) {
      return _convert((MPartSashContainer)mElement);
    } else if (mElement instanceof MPartStack) {
      return _convert((MPartStack)mElement);
    } else if (mElement instanceof MPart) {
      return _convert((MPart)mElement);
    } else if (mElement instanceof MPartSashContainerElement) {
      return _convert((MPartSashContainerElement)mElement);
    } else if (mElement instanceof MStackElement) {
      return _convert((MStackElement)mElement);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(mElement).toString());
    }
  }
  
  public void convertView(final EPackage ePackage, final MPart mPart, final PerspectivePart oPart) {
    if (ePackage instanceof ChartDSLPackage) {
      _convertView((ChartDSLPackage)ePackage, mPart, oPart);
      return;
    } else if (ePackage instanceof DialogDSLPackage) {
      _convertView((DialogDSLPackage)ePackage, mPart, oPart);
      return;
    } else if (ePackage instanceof ReportDSLPackage) {
      _convertView((ReportDSLPackage)ePackage, mPart, oPart);
      return;
    } else if (ePackage instanceof TableDSLPackage) {
      _convertView((TableDSLPackage)ePackage, mPart, oPart);
      return;
    } else if (ePackage != null) {
      _convertView(ePackage, mPart, oPart);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(ePackage, mPart, oPart).toString());
    }
  }
}
