/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.perspective.ui.labeling;

import com.google.inject.Inject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.osbp.xtext.basic.ui.labeling.BasicDSLLabelProvider;
import org.eclipse.osbp.xtext.perspective.Perspective;
import org.eclipse.osbp.xtext.perspective.PerspectiveChart;
import org.eclipse.osbp.xtext.perspective.PerspectiveDialog;
import org.eclipse.osbp.xtext.perspective.PerspectiveGrid;
import org.eclipse.osbp.xtext.perspective.PerspectiveModel;
import org.eclipse.osbp.xtext.perspective.PerspectiveOrganization;
import org.eclipse.osbp.xtext.perspective.PerspectivePackage;
import org.eclipse.osbp.xtext.perspective.PerspectivePart;
import org.eclipse.osbp.xtext.perspective.PerspectivePartStack;
import org.eclipse.osbp.xtext.perspective.PerspectiveReport;
import org.eclipse.osbp.xtext.perspective.PerspectiveSashContainer;
import org.eclipse.osbp.xtext.perspective.PerspectiveSelection;
import org.eclipse.osbp.xtext.perspective.PerspectiveTable;
import org.eclipse.osbp.xtext.perspective.PerspectiveTopology;

/**
 * Provides labels for a EObjects.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#labelProvider
 */
@SuppressWarnings("all")
public class PerspectiveDslLabelProvider extends BasicDSLLabelProvider {
  @Inject
  public PerspectiveDslLabelProvider(final AdapterFactoryLabelProvider delegate) {
    super(delegate);
  }
  
  @Override
  public Object text(final Object o) {
    Object _switchResult = null;
    boolean _matched = false;
    if (o instanceof PerspectivePackage) {
      _matched=true;
      _switchResult = this.generateText(o, "package", ((PerspectivePackage)o).getName());
    }
    if (!_matched) {
      if (o instanceof Perspective) {
        _matched=true;
        _switchResult = this.generateText(o, "perspective", ((Perspective)o).getName());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectiveSashContainer) {
        _matched=true;
        _switchResult = this.generateText(o, "sashContainer", ((PerspectiveSashContainer)o).getElementId());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectivePartStack) {
        _matched=true;
        _switchResult = this.generateText(o, "partStack", ((PerspectivePartStack)o).getElementId());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectivePart) {
        _matched=true;
        _switchResult = this.generateText(o, "part", ((PerspectivePart)o).getElementId());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectiveSelection) {
        _matched=true;
        _switchResult = this.generateText(o, "select", ((PerspectiveSelection)o).getRef().getName());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectiveTable) {
        _matched=true;
        _switchResult = this.generateText(o, "table", ((PerspectiveTable)o).getRef().getName());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectiveGrid) {
        _matched=true;
        _switchResult = this.generateText(o, "grid", ((PerspectiveGrid)o).getRef().getName());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectiveChart) {
        _matched=true;
        _switchResult = this.generateText(o, "chart", ((PerspectiveChart)o).getRef().getName());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectiveReport) {
        _matched=true;
        _switchResult = this.generateText(o, "report", ((PerspectiveReport)o).getRef().getName());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectiveOrganization) {
        _matched=true;
        _switchResult = this.generateText(o, "organigram", ((PerspectiveOrganization)o).getRef().getName());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectiveTopology) {
        _matched=true;
        _switchResult = this.generateText(o, "topology", ((PerspectiveTopology)o).getRef().getName());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectiveDialog) {
        _matched=true;
        _switchResult = this.generateText(o, "dialog", ((PerspectiveDialog)o).getRef().getName());
      }
    }
    if (!_matched) {
      _switchResult = super.text(o);
    }
    return _switchResult;
  }
  
  @Override
  public Object image(final Object o) {
    Object _switchResult = null;
    boolean _matched = false;
    if (o instanceof PerspectiveModel) {
      _matched=true;
      _switchResult = this.getInternalImage("model.png", this.getClass());
    }
    if (!_matched) {
      if (o instanceof PerspectivePackage) {
        _matched=true;
        _switchResult = this.getInternalImage("package.gif", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof Perspective) {
        _matched=true;
        _switchResult = this.getInternalImage("dsl_perspective.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectiveSelection) {
        _matched=true;
        _switchResult = this.getInternalImage("dsl_select.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectiveTable) {
        _matched=true;
        _switchResult = this.getInternalImage("dsl_table.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectiveGrid) {
        _matched=true;
        _switchResult = this.getInternalImage("CxGridSourceInput.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectiveChart) {
        _matched=true;
        _switchResult = this.getInternalImage("dsl_chart.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectiveReport) {
        _matched=true;
        _switchResult = this.getInternalImage("dsl_report.gif", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectiveOrganization) {
        _matched=true;
        _switchResult = this.getInternalImage("dsl_organigram.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectiveTopology) {
        _matched=true;
        _switchResult = this.getInternalImage("dsl_topology.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof PerspectiveDialog) {
        _matched=true;
        _switchResult = this.getInternalImage("dsl_dialog.gif", this.getClass());
      }
    }
    if (!_matched) {
      _switchResult = super.image(o);
    }
    return _switchResult;
  }
}
