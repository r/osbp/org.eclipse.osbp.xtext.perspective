/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.perspective.jvmmodel

import com.vaadin.ui.UI
import org.eclipse.osbp.eventbroker.EventBrokerMsg
import org.eclipse.osbp.xtext.basic.generator.BasicDslGeneratorUtils
import org.eclipse.osbp.xtext.i18n.I18NModelGenerator
import java.util.ArrayList
import java.util.Collection
import java.util.HashMap
import java.util.Locale
import java.util.Map
import java.util.ResourceBundle
import javax.inject.Inject
import org.eclipse.core.databinding.DataBindingContext
import org.eclipse.core.databinding.UpdateValueStrategy
import org.eclipse.core.databinding.beans.BeansObservables
import org.eclipse.e4.core.di.extensions.EventUtils
import org.eclipse.e4.core.services.events.IEventBroker
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.xbase.compiler.GeneratorConfig
import org.eclipse.xtext.xbase.compiler.ImportManager
import org.eclipse.osbp.runtime.common.i18n.ITranslator
import org.eclipse.osbp.runtime.web.vaadin.databinding.VaadinObservables
import org.osgi.service.event.EventHandler
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class PerspectiveModelGenerator extends I18NModelGenerator {
//	@Inject extension JvmTypesBuilder
//	@Inject extension IQualifiedNameProvider
	@Inject extension BasicDslGeneratorUtils
	
	override createAppendable(EObject context, ImportManager importManager, GeneratorConfig config) {
		// required to initialize the needed builder to avoid deprecated methods
		builder = context.eResource
		// ---------
		importManager.addImportFor(_typeReferenceBuilder.typeRef(HashMap, _typeReferenceBuilder.typeRef(String), _typeReferenceBuilder.typeRef(String)).type)
		importManager.addImportFor(_typeReferenceBuilder.typeRef(Map, _typeReferenceBuilder.typeRef(String), _typeReferenceBuilder.typeRef(String)).type)
		importManager.addImportFor(_typeReferenceBuilder.typeRef(Collection, _typeReferenceBuilder.typeRef(String)).type)
		addImportFor(importManager, _typeReferenceBuilder
			,ArrayList
			,IEventBroker
			,EventUtils
			,EventBrokerMsg
			,EventHandler
			,Logger
			,LoggerFactory
			// this stuff for translations
			,UI
			,VaadinObservables
			,DataBindingContext
			,BeansObservables
	//		,PropertyChangeSupport
			,ResourceBundle
			,Locale
			,UpdateValueStrategy
			,ITranslator
		)
		super.createAppendable(context, importManager, config)
	}

	override doGenerate(Resource resource, IFileSystemAccess fsa) {
		addTranslatables("PerspectiveNA")
		super.doGenerate(resource, fsa)
	}
	
}
