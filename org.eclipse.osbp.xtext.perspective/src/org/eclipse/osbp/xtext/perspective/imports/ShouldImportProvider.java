package org.eclipse.osbp.xtext.perspective.imports;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.dsl.semantic.dto.LDto;
import org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage;
import org.eclipse.osbp.xtext.action.ActionDSLPackage;
import org.eclipse.osbp.xtext.action.ActionToolbar;
import org.eclipse.osbp.xtext.blip.Blip;
import org.eclipse.osbp.xtext.blip.BlipDSLPackage;
import org.eclipse.osbp.xtext.chart.Chart;
import org.eclipse.osbp.xtext.chart.ChartDSLPackage;
import org.eclipse.osbp.xtext.datainterchange.DataDSLPackage;
import org.eclipse.osbp.xtext.datainterchange.DataInterchangeGroup;
import org.eclipse.osbp.xtext.dialogdsl.Dialog;
import org.eclipse.osbp.xtext.dialogdsl.DialogDSLPackage;
import org.eclipse.osbp.xtext.organizationdsl.Organization;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage;
import org.eclipse.osbp.xtext.oxtype.imports.DefaultShouldImportProvider;
import org.eclipse.osbp.xtext.reportdsl.Report;
import org.eclipse.osbp.xtext.reportdsl.ReportDSLPackage;
import org.eclipse.osbp.xtext.table.Table;
import org.eclipse.osbp.xtext.table.TableDSLPackage;
import org.eclipse.osbp.xtext.topologydsl.Topology;
import org.eclipse.osbp.xtext.topologydsl.TopologyDSLPackage;

public class ShouldImportProvider extends DefaultShouldImportProvider {

	protected boolean doShouldImport(EObject toImport, EReference eRef, EObject context) {
		return toImport instanceof Table || toImport instanceof Report || toImport instanceof Blip
				|| toImport instanceof Chart || toImport instanceof Organization || toImport instanceof Topology
				|| toImport instanceof Dialog || toImport instanceof ActionToolbar || toImport instanceof LDto ||
				toImport instanceof DataInterchangeGroup;
	}

	protected boolean doShouldProposeAllElements(EObject object, EReference reference) {
		EClass type = reference.getEReferenceType();
		return TableDSLPackage.Literals.TABLE.isSuperTypeOf(type)
				|| ReportDSLPackage.Literals.REPORT.isSuperTypeOf(type)
				|| BlipDSLPackage.Literals.BLIP.isSuperTypeOf(type)
				|| ChartDSLPackage.Literals.CHART.isSuperTypeOf(type)
				|| OrganizationDSLPackage.Literals.ORGANIZATION.isSuperTypeOf(type)
				|| TopologyDSLPackage.Literals.TOPOLOGY.isSuperTypeOf(type)
				|| DialogDSLPackage.Literals.DIALOG.isSuperTypeOf(type)
				|| ActionDSLPackage.Literals.ACTION_TOOLBAR.isSuperTypeOf(type)
				|| OSBPDtoPackage.Literals.LDTO.isSuperTypeOf(type)
				|| DataDSLPackage.Literals.DATA_INTERCHANGE_GROUP.isSuperTypeOf(type);
		
	}
}
