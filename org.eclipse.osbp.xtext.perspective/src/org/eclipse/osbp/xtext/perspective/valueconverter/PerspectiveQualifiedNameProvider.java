/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.xtext.perspective.valueconverter;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.xtext.naming.IQualifiedNameConverter;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.xbase.scoping.XbaseQualifiedNameProvider;

import com.google.inject.Inject;

import org.eclipse.osbp.xtext.perspective.Perspective;
import org.eclipse.osbp.xtext.perspective.PerspectivePackage;
import org.eclipse.osbp.xtext.perspective.PerspectivePart;
import org.eclipse.osbp.xtext.perspective.PerspectivePartStack;

@SuppressWarnings("restriction")
public class PerspectiveQualifiedNameProvider extends XbaseQualifiedNameProvider {

	@Inject
	private IQualifiedNameConverter qualifiedNameConverter;

	@Override
	public QualifiedName getFullyQualifiedName(EObject obj) {
		if (obj == null) {
			return QualifiedName.create("");
		}
		if (obj instanceof PerspectivePartStack) {
			PerspectivePartStack stack = (PerspectivePartStack) obj;
            if (stack.getElementId() == null) {
            	return null;
            }
            return qualifiedNameConverter.toQualifiedName(stack.getElementId());
		}
		if (obj instanceof PerspectivePart) {
			PerspectivePart part = (PerspectivePart) obj;
            if (part.getElementId() == null) {
            	return null;
            }
            return qualifiedNameConverter.toQualifiedName(part.getElementId());
		}
		return super.getFullyQualifiedName(obj);
	}

	// this method evaluates the cross referenced name of an object without causing cyclic lazy linking errors
	private String getCrossReferenceName(EObject obj, EStructuralFeature feature) {
		List<INode> nodes = NodeModelUtils.findNodesForFeature(obj, feature);
		if (nodes.size() == 0) {
			return null;
		}
		return NodeModelUtils.getTokenText(nodes.get(0));
	}

	private String getPackageName(EObject obj) {
		EObject o = obj.eContainer();
		while(!(o instanceof PerspectivePackage)) {
			o = o.eContainer();
		}
		return ((PerspectivePackage)o).getName();
	}
}
