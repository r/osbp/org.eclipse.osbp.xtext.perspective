/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * 		Christophe Loetz - Initial implementation
 *      Jörg Riegel - functional enhancements
 */
package org.eclipse.osbp.xtext.perspective.valueconverter;

import org.eclipse.osbp.xtext.basic.valueconverter.TRANSLATABLESTRINGValueConverter;
import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;
import org.eclipse.xtext.xbase.conversion.XbaseValueConverterService;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Adds a value conversion for the QualifiedNameWithWildCard rule.
 * 
 * @author Joerg Riegel
 */
@SuppressWarnings("restriction")
@Singleton
public class PerspectiveValueConverterService extends
		XbaseValueConverterService {

	@Inject
	private TRANSLATABLESTRINGValueConverter converter;

	@ValueConverter(rule = "QualifiedNameWithWildCard")
	public IValueConverter<String> getQualifiedNameWithWildCard() {
		return getQualifiedNameValueConverter();
	}

	@ValueConverter(rule = "TRANSLATABLESTRING")
	public IValueConverter<String> getTranslatableStringValueConverter() {
		return converter;
	}
	
	// this code is only a ¡¡¡¡DIRTY HACK!!!! to create a quotation around the proposal
	// text and to avoid the creation of multiplication of quotations
//	public String toString(Object value, String lexerRule) {
//		IValueConverter<Object> converter = getConverter(lexerRule);
//		String convertedValue = converter.toString(value);
//		if (I18nUtil.TRANSLATABLESTRING.equals(lexerRule)) {
//			if (convertedValue.startsWith("\"")) {
//				return convertedValue;
//			} else {
//				return '"' + convertedValue + '"';
//			}
//		}
//		return convertedValue;
//	}

}
