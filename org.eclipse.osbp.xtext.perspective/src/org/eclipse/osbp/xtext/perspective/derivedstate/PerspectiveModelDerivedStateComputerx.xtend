/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.perspective.derivedstate

import org.eclipse.osbp.xtext.perspective.Perspective
import org.eclipse.osbp.xtext.perspective.PerspectiveModel
import org.eclipse.osbp.xtext.perspective.PerspectivePackage
import org.eclipse.osbp.xtext.perspective.PerspectivePart
import org.eclipse.osbp.xtext.perspective.PerspectivePartStack
import org.eclipse.osbp.xtext.perspective.PerspectiveSashContainer
import org.eclipse.xtext.resource.DerivedStateAwareResource
import org.eclipse.xtext.xbase.jvmmodel.JvmModelAssociator
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@SuppressWarnings("restriction")
class PerspectiveModelDerivedStateComputerx extends JvmModelAssociator {
	private static final Logger log = LoggerFactory.getLogger(typeof(PerspectiveModelDerivedStateComputerx))

	DerivedStateAwareResource resource

	// not in use - just a sample
	override installDerivedState(DerivedStateAwareResource resource, boolean preLinkingPhase) {
		log.debug("PerspectiveModelDerivedStateComputerx called")
		super.installDerivedState(resource, preLinkingPhase)
		this.resource = resource

		if (resource.getContents().isEmpty()) {
			return;
		}

		if (!preLinkingPhase) {

			val PerspectiveModel eObject = resource.getContents().get(0) as PerspectiveModel;

			try {
				// complete all elements
				eObject.eContents.forEach [
					it.map
				]
			} catch (Exception ex) {
				log.error("{}", ex)
			}

//			if (views.size > 0) {
//				resource.contents += views.get(0)
//			}

		}
	}
	
	def dispatch void map(PerspectivePackage object) {
		log.debug("CCPerspectivePackage")
		object.perspectives.forEach[it.map]
	}

	def dispatch void map(Perspective object) {
		log.debug("CCPerspective")
		object.elements.forEach[it.map]
	}

	def dispatch void map(PerspectiveSashContainer object) {
		log.debug("CCPerspectiveSashContainer")
		object.elements.forEach[it.map]
	}

	def dispatch void map(PerspectivePartStack object) {
		log.debug("CCPerspectivePartStack")
		object.elements.forEach[it.map]
	}

	def dispatch void map(PerspectivePart object) {
		log.debug("CCPerspectivePart")
	}
	
	def dispatch void map(Void object) {
		log.debug("void")
	}
}
