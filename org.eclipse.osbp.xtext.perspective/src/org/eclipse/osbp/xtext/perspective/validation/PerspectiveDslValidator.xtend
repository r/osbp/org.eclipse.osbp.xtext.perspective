/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 * 
 */

package org.eclipse.osbp.xtext.perspective.validation

import java.util.HashSet
import org.eclipse.jdt.internal.core.JavaProject
import org.eclipse.osbp.xtext.perspective.Perspective
import org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage
import org.eclipse.osbp.xtext.perspective.PerspectiveElement
import org.eclipse.osbp.xtext.perspective.PerspectiveEvent
import org.eclipse.osbp.xtext.perspective.PerspectivePackage
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.validation.Check
import org.eclipse.xtext.validation.CheckType
import org.eclipse.osbp.xtext.perspective.PerspectiveEventManager

/**
 * Custom validation rules. 
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
class PerspectiveDslValidator extends AbstractPerspectiveDslValidator {
	@Check
	def checkBPMlicensed(Perspective pers) {
		if ((pers.process !== null)) {
			val rs = pers.eResource.resourceSet as XtextResourceSet
			if(rs.classpathURIContext instanceof JavaProject){
				val JavaProject javaProject = rs.classpathURIContext as JavaProject
				var found = javaProject.findType("net.osbee.bpm.BPMEngine")
				if(found === null) {
					info(
						'''BPM is needed and not yet licensed. License BPM at www.osbee.net''', pers,
						PerspectiveDslPackage.Literals.PERSPECTIVE__PROCESS)
				}
			}
		}
	}
	
	@Check(CheckType.NORMAL)
	def void checkForDuplicatedPerspectiveNames(PerspectivePackage perspectivePackage) {
		var names = new HashSet<String>()
		var counter = 0
		for (pers : perspectivePackage.perspectives){
			var name = pers.name
			if (!names.add(name)) {
				error(String.format("Perspective %s must be unique in package %s!", name, perspectivePackage.name),
						PerspectiveDslPackage.Literals.PERSPECTIVE_PACKAGE__PERSPECTIVES, counter)
				return;
			}
			counter++;
		}
	}
	
	@Check(CheckType.NORMAL)
	def void checkForDuplicatedPerspectiveElementNames(Perspective perspective) {
		var elementIds = new HashSet<String>()
		var counter = 0
		for (element : perspective.elements){
			var elementId = element.elementId
			if (!elementIds.add(elementId)) {
				error(String.format("Perspective element %s must be unique in perspective %s!", elementId, perspective.name),
						PerspectiveDslPackage.Literals.PERSPECTIVE__ELEMENTS, counter)
				return;
			}
			counter++;
		}
	}
	
	@Check(CheckType.NORMAL)
	def void checkForDuplicatedPerspectiveElementNames(PerspectiveElement perspectiveElement) {
		var elementIds = new HashSet<String>()
		var counter = 0
		for (element : perspectiveElement.elements){
			var elementId = element.elementId
			if (!elementIds.add(elementId)) {
				error(String.format("Perspective element %s must be unique in perspective element %s!", elementId, perspectiveElement.elementId),
						PerspectiveDslPackage.Literals.PERSPECTIVE_ELEMENT__ELEMENTS, counter)
				return;
			}
			counter++;
		}
	}
	
	@Check(CheckType.NORMAL)
	def void checkForDuplicatePerspectiveEventTarget(PerspectiveEventManager manager){
		val elementIds = new HashSet<String>()
		if(manager.events !== null && !manager.events.isEmpty){
			manager.events.forEach[ 			
				if(!elementIds.add(it.target.elementId)){
					error(String.format("The target event part %s can only be used once!", it.target.elementId), it, PerspectiveDslPackage.Literals.PERSPECTIVE_EVENT__TARGET)
				}
			]
		}
	}

	@Check(CheckType.NORMAL)
	def void checkForDuplicatedPerspectiveEventSenders(PerspectiveEvent event){
		var elementIds = new HashSet<String>()
		var counter = 0
		for (source : event.allowedsources){
			var elementId = source.elementId
			if (!elementIds.add(elementId)) {
				error(String.format("The source event part %s can only be used once!", elementId), event, PerspectiveDslPackage.Literals.PERSPECTIVE_EVENT__ALLOWEDSOURCES)
				return;
			}
			counter++;
		}
	}
	
	@Check(CheckType.NORMAL)
	def void checkForPerspectiveEventSenderCount(PerspectiveEvent event){
		if(event.target !== null && event.allowedsources === null || event.allowedsources.isEmpty){
			error("You must define at least one source event part!", event, PerspectiveDslPackage.Literals.PERSPECTIVE_EVENT__ALLOWEDSOURCES)
		}
	}
	
}
