/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.perspective;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Perspective Title</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.PerspectiveTitle#getHtmlName <em>Html Name</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectiveTitle()
 * @model
 * @generated
 */
public interface PerspectiveTitle extends PerspectiveView {
	/**
	 * Returns the value of the '<em><b>Html Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Html Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Html Name</em>' attribute.
	 * @see #setHtmlName(String)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectiveTitle_HtmlName()
	 * @model unique="false"
	 * @generated
	 */
	String getHtmlName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.PerspectiveTitle#getHtmlName <em>Html Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Html Name</em>' attribute.
	 * @see #getHtmlName()
	 * @generated
	 */
	void setHtmlName(String value);

} // PerspectiveTitle
