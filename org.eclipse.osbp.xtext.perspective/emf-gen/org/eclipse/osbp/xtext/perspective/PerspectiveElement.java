/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.perspective;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Perspective Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.PerspectiveElement#getElementId <em>Element Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.PerspectiveElement#getAccessibilityPhrase <em>Accessibility Phrase</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.PerspectiveElement#getContainerData <em>Container Data</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.PerspectiveElement#getElements <em>Elements</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectiveElement()
 * @model
 * @generated
 */
public interface PerspectiveElement extends PerspectiveLazyResolver {
	/**
	 * Returns the value of the '<em><b>Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Id</em>' attribute.
	 * @see #setElementId(String)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectiveElement_ElementId()
	 * @model unique="false"
	 * @generated
	 */
	String getElementId();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.PerspectiveElement#getElementId <em>Element Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Id</em>' attribute.
	 * @see #getElementId()
	 * @generated
	 */
	void setElementId(String value);

	/**
	 * Returns the value of the '<em><b>Accessibility Phrase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accessibility Phrase</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accessibility Phrase</em>' attribute.
	 * @see #setAccessibilityPhrase(String)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectiveElement_AccessibilityPhrase()
	 * @model unique="false"
	 * @generated
	 */
	String getAccessibilityPhrase();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.PerspectiveElement#getAccessibilityPhrase <em>Accessibility Phrase</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accessibility Phrase</em>' attribute.
	 * @see #getAccessibilityPhrase()
	 * @generated
	 */
	void setAccessibilityPhrase(String value);

	/**
	 * Returns the value of the '<em><b>Container Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container Data</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container Data</em>' attribute.
	 * @see #setContainerData(String)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectiveElement_ContainerData()
	 * @model unique="false"
	 * @generated
	 */
	String getContainerData();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.PerspectiveElement#getContainerData <em>Container Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container Data</em>' attribute.
	 * @see #getContainerData()
	 * @generated
	 */
	void setContainerData(String value);

	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.perspective.PerspectiveElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectiveElement_Elements()
	 * @model containment="true"
	 * @generated
	 */
	EList<PerspectiveElement> getElements();

} // PerspectiveElement
