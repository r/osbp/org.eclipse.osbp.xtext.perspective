/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.perspective;

import org.eclipse.emf.common.util.EList;

import org.eclipse.osbp.xtext.action.ActionToolbar;

import org.eclipse.osbp.xtext.blip.Blip;
import org.eclipse.osbp.xtext.blip.BlipUserTask;

import org.eclipse.xtext.common.types.JvmTypeReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Perspective</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.Perspective#isDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.Perspective#getDescriptionValue <em>Description Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.Perspective#getIconURI <em>Icon URI</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.Perspective#getAccessibilityPhrase <em>Accessibility Phrase</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.Perspective#getProcess <em>Process</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.Perspective#getUserTask <em>User Task</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.Perspective#getToolbar <em>Toolbar</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.Perspective#getToolbarTypeJvm <em>Toolbar Type Jvm</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.Perspective#getElements <em>Elements</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.Perspective#getEventmanager <em>Eventmanager</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspective()
 * @model
 * @generated
 */
public interface Perspective extends PerspectiveBase {
	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(boolean)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspective_Description()
	 * @model unique="false"
	 * @generated
	 */
	boolean isDescription();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.Perspective#isDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #isDescription()
	 * @generated
	 */
	void setDescription(boolean value);

	/**
	 * Returns the value of the '<em><b>Description Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description Value</em>' attribute.
	 * @see #setDescriptionValue(String)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspective_DescriptionValue()
	 * @model unique="false"
	 * @generated
	 */
	String getDescriptionValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.Perspective#getDescriptionValue <em>Description Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description Value</em>' attribute.
	 * @see #getDescriptionValue()
	 * @generated
	 */
	void setDescriptionValue(String value);

	/**
	 * Returns the value of the '<em><b>Icon URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Icon URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Icon URI</em>' attribute.
	 * @see #setIconURI(String)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspective_IconURI()
	 * @model unique="false"
	 * @generated
	 */
	String getIconURI();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.Perspective#getIconURI <em>Icon URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Icon URI</em>' attribute.
	 * @see #getIconURI()
	 * @generated
	 */
	void setIconURI(String value);

	/**
	 * Returns the value of the '<em><b>Accessibility Phrase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accessibility Phrase</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accessibility Phrase</em>' attribute.
	 * @see #setAccessibilityPhrase(String)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspective_AccessibilityPhrase()
	 * @model unique="false"
	 * @generated
	 */
	String getAccessibilityPhrase();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.Perspective#getAccessibilityPhrase <em>Accessibility Phrase</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accessibility Phrase</em>' attribute.
	 * @see #getAccessibilityPhrase()
	 * @generated
	 */
	void setAccessibilityPhrase(String value);

	/**
	 * Returns the value of the '<em><b>Process</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process</em>' reference.
	 * @see #setProcess(Blip)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspective_Process()
	 * @model
	 * @generated
	 */
	Blip getProcess();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.Perspective#getProcess <em>Process</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process</em>' reference.
	 * @see #getProcess()
	 * @generated
	 */
	void setProcess(Blip value);

	/**
	 * Returns the value of the '<em><b>User Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User Task</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Task</em>' reference.
	 * @see #setUserTask(BlipUserTask)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspective_UserTask()
	 * @model
	 * @generated
	 */
	BlipUserTask getUserTask();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.Perspective#getUserTask <em>User Task</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User Task</em>' reference.
	 * @see #getUserTask()
	 * @generated
	 */
	void setUserTask(BlipUserTask value);

	/**
	 * Returns the value of the '<em><b>Toolbar</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Toolbar</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Toolbar</em>' reference.
	 * @see #setToolbar(ActionToolbar)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspective_Toolbar()
	 * @model
	 * @generated
	 */
	ActionToolbar getToolbar();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.Perspective#getToolbar <em>Toolbar</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Toolbar</em>' reference.
	 * @see #getToolbar()
	 * @generated
	 */
	void setToolbar(ActionToolbar value);

	/**
	 * Returns the value of the '<em><b>Toolbar Type Jvm</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Toolbar Type Jvm</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Toolbar Type Jvm</em>' containment reference.
	 * @see #setToolbarTypeJvm(JvmTypeReference)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspective_ToolbarTypeJvm()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	JvmTypeReference getToolbarTypeJvm();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.Perspective#getToolbarTypeJvm <em>Toolbar Type Jvm</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Toolbar Type Jvm</em>' containment reference.
	 * @see #getToolbarTypeJvm()
	 * @generated
	 */
	void setToolbarTypeJvm(JvmTypeReference value);

	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.perspective.PerspectiveElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspective_Elements()
	 * @model containment="true"
	 * @generated
	 */
	EList<PerspectiveElement> getElements();

	/**
	 * Returns the value of the '<em><b>Eventmanager</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Eventmanager</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Eventmanager</em>' containment reference.
	 * @see #setEventmanager(PerspectiveEventManager)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspective_Eventmanager()
	 * @model containment="true"
	 * @generated
	 */
	PerspectiveEventManager getEventmanager();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.Perspective#getEventmanager <em>Eventmanager</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Eventmanager</em>' containment reference.
	 * @see #getEventmanager()
	 * @generated
	 */
	void setEventmanager(PerspectiveEventManager value);

} // Perspective
