/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.perspective;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Perspective Part</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.PerspectivePart#getIconURI <em>Icon URI</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.PerspectivePart#getView <em>View</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.PerspectivePart#isIsClosable <em>Is Closable</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectivePart()
 * @model
 * @generated
 */
public interface PerspectivePart extends PerspectiveElement {
	/**
	 * Returns the value of the '<em><b>Icon URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Icon URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Icon URI</em>' attribute.
	 * @see #setIconURI(String)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectivePart_IconURI()
	 * @model unique="false"
	 * @generated
	 */
	String getIconURI();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.PerspectivePart#getIconURI <em>Icon URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Icon URI</em>' attribute.
	 * @see #getIconURI()
	 * @generated
	 */
	void setIconURI(String value);

	/**
	 * Returns the value of the '<em><b>View</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>View</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>View</em>' containment reference.
	 * @see #setView(PerspectiveView)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectivePart_View()
	 * @model containment="true"
	 * @generated
	 */
	PerspectiveView getView();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.PerspectivePart#getView <em>View</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>View</em>' containment reference.
	 * @see #getView()
	 * @generated
	 */
	void setView(PerspectiveView value);

	/**
	 * Returns the value of the '<em><b>Is Closable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Closable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Closable</em>' attribute.
	 * @see #setIsClosable(boolean)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectivePart_IsClosable()
	 * @model unique="false"
	 * @generated
	 */
	boolean isIsClosable();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.PerspectivePart#isIsClosable <em>Is Closable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Closable</em>' attribute.
	 * @see #isIsClosable()
	 * @generated
	 */
	void setIsClosable(boolean value);

} // PerspectivePart
