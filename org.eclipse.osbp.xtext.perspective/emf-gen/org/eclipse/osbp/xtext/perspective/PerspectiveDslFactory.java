/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.perspective;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage
 * @generated
 */
public interface PerspectiveDslFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PerspectiveDslFactory eINSTANCE = org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Perspective Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Model</em>'.
	 * @generated
	 */
	PerspectiveModel createPerspectiveModel();

	/**
	 * Returns a new object of class '<em>Perspective Lazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Lazy Resolver</em>'.
	 * @generated
	 */
	PerspectiveLazyResolver createPerspectiveLazyResolver();

	/**
	 * Returns a new object of class '<em>Perspective Package</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Package</em>'.
	 * @generated
	 */
	PerspectivePackage createPerspectivePackage();

	/**
	 * Returns a new object of class '<em>Perspective Base</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Base</em>'.
	 * @generated
	 */
	PerspectiveBase createPerspectiveBase();

	/**
	 * Returns a new object of class '<em>Perspective</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective</em>'.
	 * @generated
	 */
	Perspective createPerspective();

	/**
	 * Returns a new object of class '<em>Perspective Event Manager</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Event Manager</em>'.
	 * @generated
	 */
	PerspectiveEventManager createPerspectiveEventManager();

	/**
	 * Returns a new object of class '<em>Perspective Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Event</em>'.
	 * @generated
	 */
	PerspectiveEvent createPerspectiveEvent();

	/**
	 * Returns a new object of class '<em>Perspective Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Element</em>'.
	 * @generated
	 */
	PerspectiveElement createPerspectiveElement();

	/**
	 * Returns a new object of class '<em>Perspective Sash Container</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Sash Container</em>'.
	 * @generated
	 */
	PerspectiveSashContainer createPerspectiveSashContainer();

	/**
	 * Returns a new object of class '<em>Perspective Part</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Part</em>'.
	 * @generated
	 */
	PerspectivePart createPerspectivePart();

	/**
	 * Returns a new object of class '<em>Perspective Part Stack</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Part Stack</em>'.
	 * @generated
	 */
	PerspectivePartStack createPerspectivePartStack();

	/**
	 * Returns a new object of class '<em>Perspective Selection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Selection</em>'.
	 * @generated
	 */
	PerspectiveSelection createPerspectiveSelection();

	/**
	 * Returns a new object of class '<em>Perspective Table</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Table</em>'.
	 * @generated
	 */
	PerspectiveTable createPerspectiveTable();

	/**
	 * Returns a new object of class '<em>Perspective Grid</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Grid</em>'.
	 * @generated
	 */
	PerspectiveGrid createPerspectiveGrid();

	/**
	 * Returns a new object of class '<em>Perspective Chart</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Chart</em>'.
	 * @generated
	 */
	PerspectiveChart createPerspectiveChart();

	/**
	 * Returns a new object of class '<em>Perspective Report</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Report</em>'.
	 * @generated
	 */
	PerspectiveReport createPerspectiveReport();

	/**
	 * Returns a new object of class '<em>Perspective Organization</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Organization</em>'.
	 * @generated
	 */
	PerspectiveOrganization createPerspectiveOrganization();

	/**
	 * Returns a new object of class '<em>Perspective Topology</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Topology</em>'.
	 * @generated
	 */
	PerspectiveTopology createPerspectiveTopology();

	/**
	 * Returns a new object of class '<em>Perspective Dialog</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Dialog</em>'.
	 * @generated
	 */
	PerspectiveDialog createPerspectiveDialog();

	/**
	 * Returns a new object of class '<em>Perspective BPMN</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective BPMN</em>'.
	 * @generated
	 */
	PerspectiveBPMN createPerspectiveBPMN();

	/**
	 * Returns a new object of class '<em>Perspective Kanban</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Kanban</em>'.
	 * @generated
	 */
	PerspectiveKanban createPerspectiveKanban();

	/**
	 * Returns a new object of class '<em>Perspective Welcome</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Welcome</em>'.
	 * @generated
	 */
	PerspectiveWelcome createPerspectiveWelcome();

	/**
	 * Returns a new object of class '<em>Perspective Saiku</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Saiku</em>'.
	 * @generated
	 */
	PerspectiveSaiku createPerspectiveSaiku();

	/**
	 * Returns a new object of class '<em>Perspective Search</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Search</em>'.
	 * @generated
	 */
	PerspectiveSearch createPerspectiveSearch();

	/**
	 * Returns a new object of class '<em>Perspective Data Interchange</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Data Interchange</em>'.
	 * @generated
	 */
	PerspectiveDataInterchange createPerspectiveDataInterchange();

	/**
	 * Returns a new object of class '<em>Perspective Title</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Title</em>'.
	 * @generated
	 */
	PerspectiveTitle createPerspectiveTitle();

	/**
	 * Returns a new object of class '<em>Perspective Dashboard</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Dashboard</em>'.
	 * @generated
	 */
	PerspectiveDashboard createPerspectiveDashboard();

	/**
	 * Returns a new object of class '<em>Perspective Browser</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perspective Browser</em>'.
	 * @generated
	 */
	PerspectiveBrowser createPerspectiveBrowser();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	PerspectiveDslPackage getPerspectiveDslPackage();

} //PerspectiveDslFactory
