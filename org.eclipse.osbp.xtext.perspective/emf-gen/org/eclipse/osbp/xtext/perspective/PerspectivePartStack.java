/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.perspective;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Perspective Part Stack</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.PerspectivePartStack#getSelectedElement <em>Selected Element</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.PerspectivePartStack#getSynchronize <em>Synchronize</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectivePartStack()
 * @model
 * @generated
 */
public interface PerspectivePartStack extends PerspectiveElement {
	/**
	 * Returns the value of the '<em><b>Selected Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selected Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selected Element</em>' reference.
	 * @see #setSelectedElement(PerspectivePart)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectivePartStack_SelectedElement()
	 * @model
	 * @generated
	 */
	PerspectivePart getSelectedElement();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.PerspectivePartStack#getSelectedElement <em>Selected Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selected Element</em>' reference.
	 * @see #getSelectedElement()
	 * @generated
	 */
	void setSelectedElement(PerspectivePart value);

	/**
	 * Returns the value of the '<em><b>Synchronize</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Synchronize</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Synchronize</em>' reference.
	 * @see #setSynchronize(PerspectivePartStack)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectivePartStack_Synchronize()
	 * @model
	 * @generated
	 */
	PerspectivePartStack getSynchronize();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.PerspectivePartStack#getSynchronize <em>Synchronize</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Synchronize</em>' reference.
	 * @see #getSynchronize()
	 * @generated
	 */
	void setSynchronize(PerspectivePartStack value);

} // PerspectivePartStack
