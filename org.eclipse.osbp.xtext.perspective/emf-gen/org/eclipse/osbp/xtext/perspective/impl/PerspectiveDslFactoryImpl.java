/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.perspective.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.osbp.xtext.perspective.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PerspectiveDslFactoryImpl extends EFactoryImpl implements PerspectiveDslFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static PerspectiveDslFactory init() {
		try {
			PerspectiveDslFactory thePerspectiveDslFactory = (PerspectiveDslFactory)EPackage.Registry.INSTANCE.getEFactory(PerspectiveDslPackage.eNS_URI);
			if (thePerspectiveDslFactory != null) {
				return thePerspectiveDslFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new PerspectiveDslFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveDslFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case PerspectiveDslPackage.PERSPECTIVE_MODEL: return createPerspectiveModel();
			case PerspectiveDslPackage.PERSPECTIVE_LAZY_RESOLVER: return createPerspectiveLazyResolver();
			case PerspectiveDslPackage.PERSPECTIVE_PACKAGE: return createPerspectivePackage();
			case PerspectiveDslPackage.PERSPECTIVE_BASE: return createPerspectiveBase();
			case PerspectiveDslPackage.PERSPECTIVE: return createPerspective();
			case PerspectiveDslPackage.PERSPECTIVE_EVENT_MANAGER: return createPerspectiveEventManager();
			case PerspectiveDslPackage.PERSPECTIVE_EVENT: return createPerspectiveEvent();
			case PerspectiveDslPackage.PERSPECTIVE_ELEMENT: return createPerspectiveElement();
			case PerspectiveDslPackage.PERSPECTIVE_SASH_CONTAINER: return createPerspectiveSashContainer();
			case PerspectiveDslPackage.PERSPECTIVE_PART: return createPerspectivePart();
			case PerspectiveDslPackage.PERSPECTIVE_PART_STACK: return createPerspectivePartStack();
			case PerspectiveDslPackage.PERSPECTIVE_SELECTION: return createPerspectiveSelection();
			case PerspectiveDslPackage.PERSPECTIVE_TABLE: return createPerspectiveTable();
			case PerspectiveDslPackage.PERSPECTIVE_GRID: return createPerspectiveGrid();
			case PerspectiveDslPackage.PERSPECTIVE_CHART: return createPerspectiveChart();
			case PerspectiveDslPackage.PERSPECTIVE_REPORT: return createPerspectiveReport();
			case PerspectiveDslPackage.PERSPECTIVE_ORGANIZATION: return createPerspectiveOrganization();
			case PerspectiveDslPackage.PERSPECTIVE_TOPOLOGY: return createPerspectiveTopology();
			case PerspectiveDslPackage.PERSPECTIVE_DIALOG: return createPerspectiveDialog();
			case PerspectiveDslPackage.PERSPECTIVE_BPMN: return createPerspectiveBPMN();
			case PerspectiveDslPackage.PERSPECTIVE_KANBAN: return createPerspectiveKanban();
			case PerspectiveDslPackage.PERSPECTIVE_WELCOME: return createPerspectiveWelcome();
			case PerspectiveDslPackage.PERSPECTIVE_SAIKU: return createPerspectiveSaiku();
			case PerspectiveDslPackage.PERSPECTIVE_SEARCH: return createPerspectiveSearch();
			case PerspectiveDslPackage.PERSPECTIVE_DATA_INTERCHANGE: return createPerspectiveDataInterchange();
			case PerspectiveDslPackage.PERSPECTIVE_TITLE: return createPerspectiveTitle();
			case PerspectiveDslPackage.PERSPECTIVE_DASHBOARD: return createPerspectiveDashboard();
			case PerspectiveDslPackage.PERSPECTIVE_BROWSER: return createPerspectiveBrowser();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case PerspectiveDslPackage.SASH_ORIENTATION:
				return createSashOrientationFromString(eDataType, initialValue);
			case PerspectiveDslPackage.INTERNAL_EOBJECT:
				return createInternalEObjectFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case PerspectiveDslPackage.SASH_ORIENTATION:
				return convertSashOrientationToString(eDataType, instanceValue);
			case PerspectiveDslPackage.INTERNAL_EOBJECT:
				return convertInternalEObjectToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveModel createPerspectiveModel() {
		PerspectiveModelImpl perspectiveModel = new PerspectiveModelImpl();
		return perspectiveModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveLazyResolver createPerspectiveLazyResolver() {
		PerspectiveLazyResolverImpl perspectiveLazyResolver = new PerspectiveLazyResolverImpl();
		return perspectiveLazyResolver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectivePackage createPerspectivePackage() {
		PerspectivePackageImpl perspectivePackage = new PerspectivePackageImpl();
		return perspectivePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveBase createPerspectiveBase() {
		PerspectiveBaseImpl perspectiveBase = new PerspectiveBaseImpl();
		return perspectiveBase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Perspective createPerspective() {
		PerspectiveImpl perspective = new PerspectiveImpl();
		return perspective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveEventManager createPerspectiveEventManager() {
		PerspectiveEventManagerImpl perspectiveEventManager = new PerspectiveEventManagerImpl();
		return perspectiveEventManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveEvent createPerspectiveEvent() {
		PerspectiveEventImpl perspectiveEvent = new PerspectiveEventImpl();
		return perspectiveEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveElement createPerspectiveElement() {
		PerspectiveElementImpl perspectiveElement = new PerspectiveElementImpl();
		return perspectiveElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveSashContainer createPerspectiveSashContainer() {
		PerspectiveSashContainerImpl perspectiveSashContainer = new PerspectiveSashContainerImpl();
		return perspectiveSashContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectivePart createPerspectivePart() {
		PerspectivePartImpl perspectivePart = new PerspectivePartImpl();
		return perspectivePart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectivePartStack createPerspectivePartStack() {
		PerspectivePartStackImpl perspectivePartStack = new PerspectivePartStackImpl();
		return perspectivePartStack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveSelection createPerspectiveSelection() {
		PerspectiveSelectionImpl perspectiveSelection = new PerspectiveSelectionImpl();
		return perspectiveSelection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveTable createPerspectiveTable() {
		PerspectiveTableImpl perspectiveTable = new PerspectiveTableImpl();
		return perspectiveTable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveGrid createPerspectiveGrid() {
		PerspectiveGridImpl perspectiveGrid = new PerspectiveGridImpl();
		return perspectiveGrid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveChart createPerspectiveChart() {
		PerspectiveChartImpl perspectiveChart = new PerspectiveChartImpl();
		return perspectiveChart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveReport createPerspectiveReport() {
		PerspectiveReportImpl perspectiveReport = new PerspectiveReportImpl();
		return perspectiveReport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveOrganization createPerspectiveOrganization() {
		PerspectiveOrganizationImpl perspectiveOrganization = new PerspectiveOrganizationImpl();
		return perspectiveOrganization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveTopology createPerspectiveTopology() {
		PerspectiveTopologyImpl perspectiveTopology = new PerspectiveTopologyImpl();
		return perspectiveTopology;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveDialog createPerspectiveDialog() {
		PerspectiveDialogImpl perspectiveDialog = new PerspectiveDialogImpl();
		return perspectiveDialog;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveBPMN createPerspectiveBPMN() {
		PerspectiveBPMNImpl perspectiveBPMN = new PerspectiveBPMNImpl();
		return perspectiveBPMN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveKanban createPerspectiveKanban() {
		PerspectiveKanbanImpl perspectiveKanban = new PerspectiveKanbanImpl();
		return perspectiveKanban;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveWelcome createPerspectiveWelcome() {
		PerspectiveWelcomeImpl perspectiveWelcome = new PerspectiveWelcomeImpl();
		return perspectiveWelcome;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveSaiku createPerspectiveSaiku() {
		PerspectiveSaikuImpl perspectiveSaiku = new PerspectiveSaikuImpl();
		return perspectiveSaiku;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveSearch createPerspectiveSearch() {
		PerspectiveSearchImpl perspectiveSearch = new PerspectiveSearchImpl();
		return perspectiveSearch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveDataInterchange createPerspectiveDataInterchange() {
		PerspectiveDataInterchangeImpl perspectiveDataInterchange = new PerspectiveDataInterchangeImpl();
		return perspectiveDataInterchange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveTitle createPerspectiveTitle() {
		PerspectiveTitleImpl perspectiveTitle = new PerspectiveTitleImpl();
		return perspectiveTitle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveDashboard createPerspectiveDashboard() {
		PerspectiveDashboardImpl perspectiveDashboard = new PerspectiveDashboardImpl();
		return perspectiveDashboard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveBrowser createPerspectiveBrowser() {
		PerspectiveBrowserImpl perspectiveBrowser = new PerspectiveBrowserImpl();
		return perspectiveBrowser;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SashOrientation createSashOrientationFromString(EDataType eDataType, String initialValue) {
		SashOrientation result = SashOrientation.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSashOrientationToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalEObject createInternalEObjectFromString(EDataType eDataType, String initialValue) {
		return (InternalEObject)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertInternalEObjectToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveDslPackage getPerspectiveDslPackage() {
		return (PerspectiveDslPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static PerspectiveDslPackage getPackage() {
		return PerspectiveDslPackage.eINSTANCE;
	}

} //PerspectiveDslFactoryImpl
