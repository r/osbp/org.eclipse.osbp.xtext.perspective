/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.perspective.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

import org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage;

import org.eclipse.osbp.xtext.action.ActionDSLPackage;

import org.eclipse.osbp.xtext.blip.BlipDSLPackage;

import org.eclipse.osbp.xtext.chart.ChartDSLPackage;

import org.eclipse.osbp.xtext.datainterchange.DataDSLPackage;

import org.eclipse.osbp.xtext.dialogdsl.DialogDSLPackage;

import org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage;

import org.eclipse.osbp.xtext.perspective.Perspective;
import org.eclipse.osbp.xtext.perspective.PerspectiveBPMN;
import org.eclipse.osbp.xtext.perspective.PerspectiveBase;
import org.eclipse.osbp.xtext.perspective.PerspectiveBrowser;
import org.eclipse.osbp.xtext.perspective.PerspectiveChart;
import org.eclipse.osbp.xtext.perspective.PerspectiveDashboard;
import org.eclipse.osbp.xtext.perspective.PerspectiveDataInterchange;
import org.eclipse.osbp.xtext.perspective.PerspectiveDialog;
import org.eclipse.osbp.xtext.perspective.PerspectiveDslFactory;
import org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage;
import org.eclipse.osbp.xtext.perspective.PerspectiveElement;
import org.eclipse.osbp.xtext.perspective.PerspectiveEvent;
import org.eclipse.osbp.xtext.perspective.PerspectiveEventManager;
import org.eclipse.osbp.xtext.perspective.PerspectiveGrid;
import org.eclipse.osbp.xtext.perspective.PerspectiveKanban;
import org.eclipse.osbp.xtext.perspective.PerspectiveLazyResolver;
import org.eclipse.osbp.xtext.perspective.PerspectiveModel;
import org.eclipse.osbp.xtext.perspective.PerspectiveOrganization;
import org.eclipse.osbp.xtext.perspective.PerspectivePackage;
import org.eclipse.osbp.xtext.perspective.PerspectivePart;
import org.eclipse.osbp.xtext.perspective.PerspectivePartStack;
import org.eclipse.osbp.xtext.perspective.PerspectiveReport;
import org.eclipse.osbp.xtext.perspective.PerspectiveSaiku;
import org.eclipse.osbp.xtext.perspective.PerspectiveSashContainer;
import org.eclipse.osbp.xtext.perspective.PerspectiveSearch;
import org.eclipse.osbp.xtext.perspective.PerspectiveSelection;
import org.eclipse.osbp.xtext.perspective.PerspectiveTable;
import org.eclipse.osbp.xtext.perspective.PerspectiveTitle;
import org.eclipse.osbp.xtext.perspective.PerspectiveTopology;
import org.eclipse.osbp.xtext.perspective.PerspectiveView;
import org.eclipse.osbp.xtext.perspective.PerspectiveWelcome;
import org.eclipse.osbp.xtext.perspective.SashOrientation;

import org.eclipse.osbp.xtext.reportdsl.ReportDSLPackage;

import org.eclipse.osbp.xtext.table.TableDSLPackage;

import org.eclipse.osbp.xtext.topologydsl.TopologyDSLPackage;

import org.eclipse.xtext.common.types.TypesPackage;

import org.eclipse.xtext.xtype.XtypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PerspectiveDslPackageImpl extends EPackageImpl implements PerspectiveDslPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveLazyResolverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectivePackageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveBaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveEventManagerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveSashContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectivePartEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectivePartStackEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveSelectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveTableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveGridEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveChartEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveReportEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveOrganizationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveTopologyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveDialogEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveBPMNEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveKanbanEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveWelcomeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveSaikuEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveSearchEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveDataInterchangeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveTitleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveDashboardEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perspectiveBrowserEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum sashOrientationEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType internalEObjectEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private PerspectiveDslPackageImpl() {
		super(eNS_URI, PerspectiveDslFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link PerspectiveDslPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static PerspectiveDslPackage init() {
		if (isInited) return (PerspectiveDslPackage)EPackage.Registry.INSTANCE.getEPackage(PerspectiveDslPackage.eNS_URI);

		// Obtain or create and register package
		PerspectiveDslPackageImpl thePerspectiveDslPackage = (PerspectiveDslPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof PerspectiveDslPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new PerspectiveDslPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		TableDSLPackage.eINSTANCE.eClass();
		ChartDSLPackage.eINSTANCE.eClass();
		ReportDSLPackage.eINSTANCE.eClass();
		OrganizationDSLPackage.eINSTANCE.eClass();
		TopologyDSLPackage.eINSTANCE.eClass();
		DialogDSLPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		thePerspectiveDslPackage.createPackageContents();

		// Initialize created meta-data
		thePerspectiveDslPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		thePerspectiveDslPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(PerspectiveDslPackage.eNS_URI, thePerspectiveDslPackage);
		return thePerspectiveDslPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveModel() {
		return perspectiveModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveModel_ImportSection() {
		return (EReference)perspectiveModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveModel_Packages() {
		return (EReference)perspectiveModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveLazyResolver() {
		return perspectiveLazyResolverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPerspectiveLazyResolver__EResolveProxy__InternalEObject() {
		return perspectiveLazyResolverEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectivePackage() {
		return perspectivePackageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectivePackage_Perspectives() {
		return (EReference)perspectivePackageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveBase() {
		return perspectiveBaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerspectiveBase_Name() {
		return (EAttribute)perspectiveBaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspective() {
		return perspectiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerspective_Description() {
		return (EAttribute)perspectiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerspective_DescriptionValue() {
		return (EAttribute)perspectiveEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerspective_IconURI() {
		return (EAttribute)perspectiveEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerspective_AccessibilityPhrase() {
		return (EAttribute)perspectiveEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspective_Process() {
		return (EReference)perspectiveEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspective_UserTask() {
		return (EReference)perspectiveEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspective_Toolbar() {
		return (EReference)perspectiveEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspective_ToolbarTypeJvm() {
		return (EReference)perspectiveEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspective_Elements() {
		return (EReference)perspectiveEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspective_Eventmanager() {
		return (EReference)perspectiveEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveEventManager() {
		return perspectiveEventManagerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveEventManager_Events() {
		return (EReference)perspectiveEventManagerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveEvent() {
		return perspectiveEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveEvent_Target() {
		return (EReference)perspectiveEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveEvent_Allowedsources() {
		return (EReference)perspectiveEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveElement() {
		return perspectiveElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerspectiveElement_ElementId() {
		return (EAttribute)perspectiveElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerspectiveElement_AccessibilityPhrase() {
		return (EAttribute)perspectiveElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerspectiveElement_ContainerData() {
		return (EAttribute)perspectiveElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveElement_Elements() {
		return (EReference)perspectiveElementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveSashContainer() {
		return perspectiveSashContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerspectiveSashContainer_Orientation() {
		return (EAttribute)perspectiveSashContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveSashContainer_SelectedElement() {
		return (EReference)perspectiveSashContainerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectivePart() {
		return perspectivePartEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerspectivePart_IconURI() {
		return (EAttribute)perspectivePartEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectivePart_View() {
		return (EReference)perspectivePartEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerspectivePart_IsClosable() {
		return (EAttribute)perspectivePartEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectivePartStack() {
		return perspectivePartStackEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectivePartStack_SelectedElement() {
		return (EReference)perspectivePartStackEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectivePartStack_Synchronize() {
		return (EReference)perspectivePartStackEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveView() {
		return perspectiveViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveSelection() {
		return perspectiveSelectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveSelection_Ref() {
		return (EReference)perspectiveSelectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveTable() {
		return perspectiveTableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveTable_Ref() {
		return (EReference)perspectiveTableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveGrid() {
		return perspectiveGridEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveGrid_Ref() {
		return (EReference)perspectiveGridEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveChart() {
		return perspectiveChartEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveChart_Ref() {
		return (EReference)perspectiveChartEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveReport() {
		return perspectiveReportEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveReport_Ref() {
		return (EReference)perspectiveReportEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveOrganization() {
		return perspectiveOrganizationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveOrganization_Ref() {
		return (EReference)perspectiveOrganizationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveTopology() {
		return perspectiveTopologyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveTopology_Ref() {
		return (EReference)perspectiveTopologyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveDialog() {
		return perspectiveDialogEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveDialog_Ref() {
		return (EReference)perspectiveDialogEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveBPMN() {
		return perspectiveBPMNEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveKanban() {
		return perspectiveKanbanEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveKanban_DtoRef() {
		return (EReference)perspectiveKanbanEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveKanban_CardRef() {
		return (EReference)perspectiveKanbanEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveKanban_DialogRef() {
		return (EReference)perspectiveKanbanEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveWelcome() {
		return perspectiveWelcomeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveSaiku() {
		return perspectiveSaikuEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveSearch() {
		return perspectiveSearchEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveSearch_DtoRef() {
		return (EReference)perspectiveSearchEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerspectiveSearch_Depth() {
		return (EAttribute)perspectiveSearchEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerspectiveSearch_FilterCols() {
		return (EAttribute)perspectiveSearchEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveDataInterchange() {
		return perspectiveDataInterchangeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerspectiveDataInterchange_Ref() {
		return (EReference)perspectiveDataInterchangeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveTitle() {
		return perspectiveTitleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerspectiveTitle_HtmlName() {
		return (EAttribute)perspectiveTitleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveDashboard() {
		return perspectiveDashboardEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerspectiveDashboard_Name() {
		return (EAttribute)perspectiveDashboardEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerspectiveBrowser() {
		return perspectiveBrowserEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerspectiveBrowser_Url() {
		return (EAttribute)perspectiveBrowserEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerspectiveBrowser_CubeView() {
		return (EAttribute)perspectiveBrowserEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSashOrientation() {
		return sashOrientationEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getInternalEObject() {
		return internalEObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveDslFactory getPerspectiveDslFactory() {
		return (PerspectiveDslFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		perspectiveModelEClass = createEClass(PERSPECTIVE_MODEL);
		createEReference(perspectiveModelEClass, PERSPECTIVE_MODEL__IMPORT_SECTION);
		createEReference(perspectiveModelEClass, PERSPECTIVE_MODEL__PACKAGES);

		perspectiveLazyResolverEClass = createEClass(PERSPECTIVE_LAZY_RESOLVER);
		createEOperation(perspectiveLazyResolverEClass, PERSPECTIVE_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT);

		perspectivePackageEClass = createEClass(PERSPECTIVE_PACKAGE);
		createEReference(perspectivePackageEClass, PERSPECTIVE_PACKAGE__PERSPECTIVES);

		perspectiveBaseEClass = createEClass(PERSPECTIVE_BASE);
		createEAttribute(perspectiveBaseEClass, PERSPECTIVE_BASE__NAME);

		perspectiveEClass = createEClass(PERSPECTIVE);
		createEAttribute(perspectiveEClass, PERSPECTIVE__DESCRIPTION);
		createEAttribute(perspectiveEClass, PERSPECTIVE__DESCRIPTION_VALUE);
		createEAttribute(perspectiveEClass, PERSPECTIVE__ICON_URI);
		createEAttribute(perspectiveEClass, PERSPECTIVE__ACCESSIBILITY_PHRASE);
		createEReference(perspectiveEClass, PERSPECTIVE__PROCESS);
		createEReference(perspectiveEClass, PERSPECTIVE__USER_TASK);
		createEReference(perspectiveEClass, PERSPECTIVE__TOOLBAR);
		createEReference(perspectiveEClass, PERSPECTIVE__TOOLBAR_TYPE_JVM);
		createEReference(perspectiveEClass, PERSPECTIVE__ELEMENTS);
		createEReference(perspectiveEClass, PERSPECTIVE__EVENTMANAGER);

		perspectiveEventManagerEClass = createEClass(PERSPECTIVE_EVENT_MANAGER);
		createEReference(perspectiveEventManagerEClass, PERSPECTIVE_EVENT_MANAGER__EVENTS);

		perspectiveEventEClass = createEClass(PERSPECTIVE_EVENT);
		createEReference(perspectiveEventEClass, PERSPECTIVE_EVENT__TARGET);
		createEReference(perspectiveEventEClass, PERSPECTIVE_EVENT__ALLOWEDSOURCES);

		perspectiveElementEClass = createEClass(PERSPECTIVE_ELEMENT);
		createEAttribute(perspectiveElementEClass, PERSPECTIVE_ELEMENT__ELEMENT_ID);
		createEAttribute(perspectiveElementEClass, PERSPECTIVE_ELEMENT__ACCESSIBILITY_PHRASE);
		createEAttribute(perspectiveElementEClass, PERSPECTIVE_ELEMENT__CONTAINER_DATA);
		createEReference(perspectiveElementEClass, PERSPECTIVE_ELEMENT__ELEMENTS);

		perspectiveSashContainerEClass = createEClass(PERSPECTIVE_SASH_CONTAINER);
		createEAttribute(perspectiveSashContainerEClass, PERSPECTIVE_SASH_CONTAINER__ORIENTATION);
		createEReference(perspectiveSashContainerEClass, PERSPECTIVE_SASH_CONTAINER__SELECTED_ELEMENT);

		perspectivePartEClass = createEClass(PERSPECTIVE_PART);
		createEAttribute(perspectivePartEClass, PERSPECTIVE_PART__ICON_URI);
		createEReference(perspectivePartEClass, PERSPECTIVE_PART__VIEW);
		createEAttribute(perspectivePartEClass, PERSPECTIVE_PART__IS_CLOSABLE);

		perspectivePartStackEClass = createEClass(PERSPECTIVE_PART_STACK);
		createEReference(perspectivePartStackEClass, PERSPECTIVE_PART_STACK__SELECTED_ELEMENT);
		createEReference(perspectivePartStackEClass, PERSPECTIVE_PART_STACK__SYNCHRONIZE);

		perspectiveViewEClass = createEClass(PERSPECTIVE_VIEW);

		perspectiveSelectionEClass = createEClass(PERSPECTIVE_SELECTION);
		createEReference(perspectiveSelectionEClass, PERSPECTIVE_SELECTION__REF);

		perspectiveTableEClass = createEClass(PERSPECTIVE_TABLE);
		createEReference(perspectiveTableEClass, PERSPECTIVE_TABLE__REF);

		perspectiveGridEClass = createEClass(PERSPECTIVE_GRID);
		createEReference(perspectiveGridEClass, PERSPECTIVE_GRID__REF);

		perspectiveChartEClass = createEClass(PERSPECTIVE_CHART);
		createEReference(perspectiveChartEClass, PERSPECTIVE_CHART__REF);

		perspectiveReportEClass = createEClass(PERSPECTIVE_REPORT);
		createEReference(perspectiveReportEClass, PERSPECTIVE_REPORT__REF);

		perspectiveOrganizationEClass = createEClass(PERSPECTIVE_ORGANIZATION);
		createEReference(perspectiveOrganizationEClass, PERSPECTIVE_ORGANIZATION__REF);

		perspectiveTopologyEClass = createEClass(PERSPECTIVE_TOPOLOGY);
		createEReference(perspectiveTopologyEClass, PERSPECTIVE_TOPOLOGY__REF);

		perspectiveDialogEClass = createEClass(PERSPECTIVE_DIALOG);
		createEReference(perspectiveDialogEClass, PERSPECTIVE_DIALOG__REF);

		perspectiveBPMNEClass = createEClass(PERSPECTIVE_BPMN);

		perspectiveKanbanEClass = createEClass(PERSPECTIVE_KANBAN);
		createEReference(perspectiveKanbanEClass, PERSPECTIVE_KANBAN__DTO_REF);
		createEReference(perspectiveKanbanEClass, PERSPECTIVE_KANBAN__CARD_REF);
		createEReference(perspectiveKanbanEClass, PERSPECTIVE_KANBAN__DIALOG_REF);

		perspectiveWelcomeEClass = createEClass(PERSPECTIVE_WELCOME);

		perspectiveSaikuEClass = createEClass(PERSPECTIVE_SAIKU);

		perspectiveSearchEClass = createEClass(PERSPECTIVE_SEARCH);
		createEReference(perspectiveSearchEClass, PERSPECTIVE_SEARCH__DTO_REF);
		createEAttribute(perspectiveSearchEClass, PERSPECTIVE_SEARCH__DEPTH);
		createEAttribute(perspectiveSearchEClass, PERSPECTIVE_SEARCH__FILTER_COLS);

		perspectiveDataInterchangeEClass = createEClass(PERSPECTIVE_DATA_INTERCHANGE);
		createEReference(perspectiveDataInterchangeEClass, PERSPECTIVE_DATA_INTERCHANGE__REF);

		perspectiveTitleEClass = createEClass(PERSPECTIVE_TITLE);
		createEAttribute(perspectiveTitleEClass, PERSPECTIVE_TITLE__HTML_NAME);

		perspectiveDashboardEClass = createEClass(PERSPECTIVE_DASHBOARD);
		createEAttribute(perspectiveDashboardEClass, PERSPECTIVE_DASHBOARD__NAME);

		perspectiveBrowserEClass = createEClass(PERSPECTIVE_BROWSER);
		createEAttribute(perspectiveBrowserEClass, PERSPECTIVE_BROWSER__URL);
		createEAttribute(perspectiveBrowserEClass, PERSPECTIVE_BROWSER__CUBE_VIEW);

		// Create enums
		sashOrientationEEnum = createEEnum(SASH_ORIENTATION);

		// Create data types
		internalEObjectEDataType = createEDataType(INTERNAL_EOBJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XtypePackage theXtypePackage = (XtypePackage)EPackage.Registry.INSTANCE.getEPackage(XtypePackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		OSBPTypesPackage theOSBPTypesPackage = (OSBPTypesPackage)EPackage.Registry.INSTANCE.getEPackage(OSBPTypesPackage.eNS_URI);
		BlipDSLPackage theBlipDSLPackage = (BlipDSLPackage)EPackage.Registry.INSTANCE.getEPackage(BlipDSLPackage.eNS_URI);
		ActionDSLPackage theActionDSLPackage = (ActionDSLPackage)EPackage.Registry.INSTANCE.getEPackage(ActionDSLPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		TableDSLPackage theTableDSLPackage = (TableDSLPackage)EPackage.Registry.INSTANCE.getEPackage(TableDSLPackage.eNS_URI);
		ChartDSLPackage theChartDSLPackage = (ChartDSLPackage)EPackage.Registry.INSTANCE.getEPackage(ChartDSLPackage.eNS_URI);
		ReportDSLPackage theReportDSLPackage = (ReportDSLPackage)EPackage.Registry.INSTANCE.getEPackage(ReportDSLPackage.eNS_URI);
		OrganizationDSLPackage theOrganizationDSLPackage = (OrganizationDSLPackage)EPackage.Registry.INSTANCE.getEPackage(OrganizationDSLPackage.eNS_URI);
		TopologyDSLPackage theTopologyDSLPackage = (TopologyDSLPackage)EPackage.Registry.INSTANCE.getEPackage(TopologyDSLPackage.eNS_URI);
		DialogDSLPackage theDialogDSLPackage = (DialogDSLPackage)EPackage.Registry.INSTANCE.getEPackage(DialogDSLPackage.eNS_URI);
		OSBPDtoPackage theOSBPDtoPackage = (OSBPDtoPackage)EPackage.Registry.INSTANCE.getEPackage(OSBPDtoPackage.eNS_URI);
		DataDSLPackage theDataDSLPackage = (DataDSLPackage)EPackage.Registry.INSTANCE.getEPackage(DataDSLPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		perspectivePackageEClass.getESuperTypes().add(theOSBPTypesPackage.getLPackage());
		perspectiveBaseEClass.getESuperTypes().add(this.getPerspectiveLazyResolver());
		perspectiveEClass.getESuperTypes().add(this.getPerspectiveBase());
		perspectiveEventManagerEClass.getESuperTypes().add(this.getPerspectiveBase());
		perspectiveElementEClass.getESuperTypes().add(this.getPerspectiveLazyResolver());
		perspectiveSashContainerEClass.getESuperTypes().add(this.getPerspectiveElement());
		perspectivePartEClass.getESuperTypes().add(this.getPerspectiveElement());
		perspectivePartStackEClass.getESuperTypes().add(this.getPerspectiveElement());
		perspectiveViewEClass.getESuperTypes().add(this.getPerspectiveLazyResolver());
		perspectiveSelectionEClass.getESuperTypes().add(this.getPerspectiveView());
		perspectiveTableEClass.getESuperTypes().add(this.getPerspectiveView());
		perspectiveGridEClass.getESuperTypes().add(this.getPerspectiveView());
		perspectiveChartEClass.getESuperTypes().add(this.getPerspectiveView());
		perspectiveReportEClass.getESuperTypes().add(this.getPerspectiveView());
		perspectiveOrganizationEClass.getESuperTypes().add(this.getPerspectiveView());
		perspectiveTopologyEClass.getESuperTypes().add(this.getPerspectiveView());
		perspectiveDialogEClass.getESuperTypes().add(this.getPerspectiveView());
		perspectiveBPMNEClass.getESuperTypes().add(this.getPerspectiveView());
		perspectiveKanbanEClass.getESuperTypes().add(this.getPerspectiveView());
		perspectiveWelcomeEClass.getESuperTypes().add(this.getPerspectiveView());
		perspectiveSaikuEClass.getESuperTypes().add(this.getPerspectiveView());
		perspectiveSearchEClass.getESuperTypes().add(this.getPerspectiveView());
		perspectiveDataInterchangeEClass.getESuperTypes().add(this.getPerspectiveView());
		perspectiveTitleEClass.getESuperTypes().add(this.getPerspectiveView());
		perspectiveDashboardEClass.getESuperTypes().add(this.getPerspectiveView());
		perspectiveBrowserEClass.getESuperTypes().add(this.getPerspectiveView());

		// Initialize classes, features, and operations; add parameters
		initEClass(perspectiveModelEClass, PerspectiveModel.class, "PerspectiveModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerspectiveModel_ImportSection(), theXtypePackage.getXImportSection(), null, "importSection", null, 0, 1, PerspectiveModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerspectiveModel_Packages(), this.getPerspectivePackage(), null, "packages", null, 0, -1, PerspectiveModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveLazyResolverEClass, PerspectiveLazyResolver.class, "PerspectiveLazyResolver", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getPerspectiveLazyResolver__EResolveProxy__InternalEObject(), theEcorePackage.getEObject(), "eResolveProxy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInternalEObject(), "proxy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(perspectivePackageEClass, PerspectivePackage.class, "PerspectivePackage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerspectivePackage_Perspectives(), this.getPerspective(), null, "perspectives", null, 0, -1, PerspectivePackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveBaseEClass, PerspectiveBase.class, "PerspectiveBase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPerspectiveBase_Name(), theEcorePackage.getEString(), "name", null, 0, 1, PerspectiveBase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveEClass, Perspective.class, "Perspective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPerspective_Description(), theEcorePackage.getEBoolean(), "description", null, 0, 1, Perspective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPerspective_DescriptionValue(), theEcorePackage.getEString(), "descriptionValue", null, 0, 1, Perspective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPerspective_IconURI(), theEcorePackage.getEString(), "iconURI", null, 0, 1, Perspective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPerspective_AccessibilityPhrase(), theEcorePackage.getEString(), "accessibilityPhrase", null, 0, 1, Perspective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerspective_Process(), theBlipDSLPackage.getBlip(), null, "process", null, 0, 1, Perspective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerspective_UserTask(), theBlipDSLPackage.getBlipUserTask(), null, "userTask", null, 0, 1, Perspective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerspective_Toolbar(), theActionDSLPackage.getActionToolbar(), null, "toolbar", null, 0, 1, Perspective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerspective_ToolbarTypeJvm(), theTypesPackage.getJvmTypeReference(), null, "toolbarTypeJvm", null, 0, 1, Perspective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerspective_Elements(), this.getPerspectiveElement(), null, "elements", null, 0, -1, Perspective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerspective_Eventmanager(), this.getPerspectiveEventManager(), null, "eventmanager", null, 0, 1, Perspective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveEventManagerEClass, PerspectiveEventManager.class, "PerspectiveEventManager", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerspectiveEventManager_Events(), this.getPerspectiveEvent(), null, "events", null, 0, -1, PerspectiveEventManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveEventEClass, PerspectiveEvent.class, "PerspectiveEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerspectiveEvent_Target(), this.getPerspectivePart(), null, "target", null, 0, 1, PerspectiveEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerspectiveEvent_Allowedsources(), this.getPerspectivePart(), null, "allowedsources", null, 0, -1, PerspectiveEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveElementEClass, PerspectiveElement.class, "PerspectiveElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPerspectiveElement_ElementId(), theEcorePackage.getEString(), "elementId", null, 0, 1, PerspectiveElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPerspectiveElement_AccessibilityPhrase(), theEcorePackage.getEString(), "accessibilityPhrase", null, 0, 1, PerspectiveElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPerspectiveElement_ContainerData(), theEcorePackage.getEString(), "containerData", null, 0, 1, PerspectiveElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerspectiveElement_Elements(), this.getPerspectiveElement(), null, "elements", null, 0, -1, PerspectiveElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveSashContainerEClass, PerspectiveSashContainer.class, "PerspectiveSashContainer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPerspectiveSashContainer_Orientation(), this.getSashOrientation(), "orientation", null, 0, 1, PerspectiveSashContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerspectiveSashContainer_SelectedElement(), this.getPerspectiveElement(), null, "selectedElement", null, 0, 1, PerspectiveSashContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectivePartEClass, PerspectivePart.class, "PerspectivePart", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPerspectivePart_IconURI(), theEcorePackage.getEString(), "iconURI", null, 0, 1, PerspectivePart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerspectivePart_View(), this.getPerspectiveView(), null, "view", null, 0, 1, PerspectivePart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPerspectivePart_IsClosable(), theEcorePackage.getEBoolean(), "isClosable", null, 0, 1, PerspectivePart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectivePartStackEClass, PerspectivePartStack.class, "PerspectivePartStack", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerspectivePartStack_SelectedElement(), this.getPerspectivePart(), null, "selectedElement", null, 0, 1, PerspectivePartStack.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerspectivePartStack_Synchronize(), this.getPerspectivePartStack(), null, "synchronize", null, 0, 1, PerspectivePartStack.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveViewEClass, PerspectiveView.class, "PerspectiveView", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(perspectiveSelectionEClass, PerspectiveSelection.class, "PerspectiveSelection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerspectiveSelection_Ref(), theTableDSLPackage.getTable(), null, "ref", null, 0, 1, PerspectiveSelection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveTableEClass, PerspectiveTable.class, "PerspectiveTable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerspectiveTable_Ref(), theTableDSLPackage.getTable(), null, "ref", null, 0, 1, PerspectiveTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveGridEClass, PerspectiveGrid.class, "PerspectiveGrid", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerspectiveGrid_Ref(), theTableDSLPackage.getTable(), null, "ref", null, 0, 1, PerspectiveGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveChartEClass, PerspectiveChart.class, "PerspectiveChart", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerspectiveChart_Ref(), theChartDSLPackage.getChart(), null, "ref", null, 0, 1, PerspectiveChart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveReportEClass, PerspectiveReport.class, "PerspectiveReport", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerspectiveReport_Ref(), theReportDSLPackage.getReport(), null, "ref", null, 0, 1, PerspectiveReport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveOrganizationEClass, PerspectiveOrganization.class, "PerspectiveOrganization", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerspectiveOrganization_Ref(), theOrganizationDSLPackage.getOrganization(), null, "ref", null, 0, 1, PerspectiveOrganization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveTopologyEClass, PerspectiveTopology.class, "PerspectiveTopology", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerspectiveTopology_Ref(), theTopologyDSLPackage.getTopology(), null, "ref", null, 0, 1, PerspectiveTopology.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveDialogEClass, PerspectiveDialog.class, "PerspectiveDialog", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerspectiveDialog_Ref(), theDialogDSLPackage.getDialog(), null, "ref", null, 0, 1, PerspectiveDialog.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveBPMNEClass, PerspectiveBPMN.class, "PerspectiveBPMN", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(perspectiveKanbanEClass, PerspectiveKanban.class, "PerspectiveKanban", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerspectiveKanban_DtoRef(), theOSBPDtoPackage.getLDto(), null, "dtoRef", null, 0, 1, PerspectiveKanban.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerspectiveKanban_CardRef(), theDialogDSLPackage.getDialog(), null, "cardRef", null, 0, 1, PerspectiveKanban.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerspectiveKanban_DialogRef(), theDialogDSLPackage.getDialog(), null, "dialogRef", null, 0, 1, PerspectiveKanban.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveWelcomeEClass, PerspectiveWelcome.class, "PerspectiveWelcome", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(perspectiveSaikuEClass, PerspectiveSaiku.class, "PerspectiveSaiku", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(perspectiveSearchEClass, PerspectiveSearch.class, "PerspectiveSearch", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerspectiveSearch_DtoRef(), theOSBPDtoPackage.getLDto(), null, "dtoRef", null, 0, 1, PerspectiveSearch.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPerspectiveSearch_Depth(), theEcorePackage.getEInt(), "depth", null, 0, 1, PerspectiveSearch.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPerspectiveSearch_FilterCols(), theEcorePackage.getEInt(), "filterCols", null, 0, 1, PerspectiveSearch.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveDataInterchangeEClass, PerspectiveDataInterchange.class, "PerspectiveDataInterchange", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerspectiveDataInterchange_Ref(), theDataDSLPackage.getDataInterchangeGroup(), null, "ref", null, 0, 1, PerspectiveDataInterchange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveTitleEClass, PerspectiveTitle.class, "PerspectiveTitle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPerspectiveTitle_HtmlName(), theEcorePackage.getEString(), "htmlName", null, 0, 1, PerspectiveTitle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveDashboardEClass, PerspectiveDashboard.class, "PerspectiveDashboard", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPerspectiveDashboard_Name(), theEcorePackage.getEString(), "name", null, 0, 1, PerspectiveDashboard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perspectiveBrowserEClass, PerspectiveBrowser.class, "PerspectiveBrowser", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPerspectiveBrowser_Url(), theEcorePackage.getEString(), "url", null, 0, 1, PerspectiveBrowser.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPerspectiveBrowser_CubeView(), theEcorePackage.getEBoolean(), "cubeView", null, 0, 1, PerspectiveBrowser.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(sashOrientationEEnum, SashOrientation.class, "SashOrientation");
		addEEnumLiteral(sashOrientationEEnum, SashOrientation.HORIZONTAL);
		addEEnumLiteral(sashOrientationEEnum, SashOrientation.VERTICAL);

		// Initialize data types
		initEDataType(internalEObjectEDataType, InternalEObject.class, "InternalEObject", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "rootPackage", "perspectivedsl"
		   });
	}

} //PerspectiveDslPackageImpl
