/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.perspective.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage;
import org.eclipse.osbp.xtext.perspective.PerspectiveSaiku;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Perspective Saiku</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PerspectiveSaikuImpl extends PerspectiveLazyResolverImpl implements PerspectiveSaiku {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PerspectiveSaikuImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PerspectiveDslPackage.Literals.PERSPECTIVE_SAIKU;
	}

} //PerspectiveSaikuImpl
