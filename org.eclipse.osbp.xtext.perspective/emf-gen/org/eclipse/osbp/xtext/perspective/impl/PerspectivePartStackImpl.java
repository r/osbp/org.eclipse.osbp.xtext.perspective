/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.perspective.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage;
import org.eclipse.osbp.xtext.perspective.PerspectivePart;
import org.eclipse.osbp.xtext.perspective.PerspectivePartStack;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Perspective Part Stack</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.impl.PerspectivePartStackImpl#getSelectedElement <em>Selected Element</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.impl.PerspectivePartStackImpl#getSynchronize <em>Synchronize</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PerspectivePartStackImpl extends PerspectiveElementImpl implements PerspectivePartStack {
	/**
	 * The cached value of the '{@link #getSelectedElement() <em>Selected Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectedElement()
	 * @generated
	 * @ordered
	 */
	protected PerspectivePart selectedElement;

	/**
	 * The cached value of the '{@link #getSynchronize() <em>Synchronize</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSynchronize()
	 * @generated
	 * @ordered
	 */
	protected PerspectivePartStack synchronize;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PerspectivePartStackImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PerspectiveDslPackage.Literals.PERSPECTIVE_PART_STACK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectivePart getSelectedElement() {
		if (selectedElement != null && selectedElement.eIsProxy()) {
			InternalEObject oldSelectedElement = (InternalEObject)selectedElement;
			selectedElement = (PerspectivePart)eResolveProxy(oldSelectedElement);
			if (selectedElement != oldSelectedElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PerspectiveDslPackage.PERSPECTIVE_PART_STACK__SELECTED_ELEMENT, oldSelectedElement, selectedElement));
			}
		}
		return selectedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectivePart basicGetSelectedElement() {
		return selectedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectedElement(PerspectivePart newSelectedElement) {
		PerspectivePart oldSelectedElement = selectedElement;
		selectedElement = newSelectedElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PerspectiveDslPackage.PERSPECTIVE_PART_STACK__SELECTED_ELEMENT, oldSelectedElement, selectedElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectivePartStack getSynchronize() {
		if (synchronize != null && synchronize.eIsProxy()) {
			InternalEObject oldSynchronize = (InternalEObject)synchronize;
			synchronize = (PerspectivePartStack)eResolveProxy(oldSynchronize);
			if (synchronize != oldSynchronize) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PerspectiveDslPackage.PERSPECTIVE_PART_STACK__SYNCHRONIZE, oldSynchronize, synchronize));
			}
		}
		return synchronize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectivePartStack basicGetSynchronize() {
		return synchronize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSynchronize(PerspectivePartStack newSynchronize) {
		PerspectivePartStack oldSynchronize = synchronize;
		synchronize = newSynchronize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PerspectiveDslPackage.PERSPECTIVE_PART_STACK__SYNCHRONIZE, oldSynchronize, synchronize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PerspectiveDslPackage.PERSPECTIVE_PART_STACK__SELECTED_ELEMENT:
				if (resolve) return getSelectedElement();
				return basicGetSelectedElement();
			case PerspectiveDslPackage.PERSPECTIVE_PART_STACK__SYNCHRONIZE:
				if (resolve) return getSynchronize();
				return basicGetSynchronize();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PerspectiveDslPackage.PERSPECTIVE_PART_STACK__SELECTED_ELEMENT:
				setSelectedElement((PerspectivePart)newValue);
				return;
			case PerspectiveDslPackage.PERSPECTIVE_PART_STACK__SYNCHRONIZE:
				setSynchronize((PerspectivePartStack)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PerspectiveDslPackage.PERSPECTIVE_PART_STACK__SELECTED_ELEMENT:
				setSelectedElement((PerspectivePart)null);
				return;
			case PerspectiveDslPackage.PERSPECTIVE_PART_STACK__SYNCHRONIZE:
				setSynchronize((PerspectivePartStack)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PerspectiveDslPackage.PERSPECTIVE_PART_STACK__SELECTED_ELEMENT:
				return selectedElement != null;
			case PerspectiveDslPackage.PERSPECTIVE_PART_STACK__SYNCHRONIZE:
				return synchronize != null;
		}
		return super.eIsSet(featureID);
	}

} //PerspectivePartStackImpl
