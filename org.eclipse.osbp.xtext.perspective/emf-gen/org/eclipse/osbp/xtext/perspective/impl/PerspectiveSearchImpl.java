/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.perspective.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.dsl.semantic.dto.LDto;

import org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage;
import org.eclipse.osbp.xtext.perspective.PerspectiveSearch;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Perspective Search</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveSearchImpl#getDtoRef <em>Dto Ref</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveSearchImpl#getDepth <em>Depth</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveSearchImpl#getFilterCols <em>Filter Cols</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PerspectiveSearchImpl extends PerspectiveLazyResolverImpl implements PerspectiveSearch {
	/**
	 * The cached value of the '{@link #getDtoRef() <em>Dto Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDtoRef()
	 * @generated
	 * @ordered
	 */
	protected LDto dtoRef;

	/**
	 * The default value of the '{@link #getDepth() <em>Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDepth()
	 * @generated
	 * @ordered
	 */
	protected static final int DEPTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDepth() <em>Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDepth()
	 * @generated
	 * @ordered
	 */
	protected int depth = DEPTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getFilterCols() <em>Filter Cols</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterCols()
	 * @generated
	 * @ordered
	 */
	protected static final int FILTER_COLS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFilterCols() <em>Filter Cols</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterCols()
	 * @generated
	 * @ordered
	 */
	protected int filterCols = FILTER_COLS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PerspectiveSearchImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PerspectiveDslPackage.Literals.PERSPECTIVE_SEARCH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDto getDtoRef() {
		if (dtoRef != null && dtoRef.eIsProxy()) {
			InternalEObject oldDtoRef = (InternalEObject)dtoRef;
			dtoRef = (LDto)eResolveProxy(oldDtoRef);
			if (dtoRef != oldDtoRef) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PerspectiveDslPackage.PERSPECTIVE_SEARCH__DTO_REF, oldDtoRef, dtoRef));
			}
		}
		return dtoRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDto basicGetDtoRef() {
		return dtoRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDtoRef(LDto newDtoRef) {
		LDto oldDtoRef = dtoRef;
		dtoRef = newDtoRef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PerspectiveDslPackage.PERSPECTIVE_SEARCH__DTO_REF, oldDtoRef, dtoRef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDepth() {
		return depth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDepth(int newDepth) {
		int oldDepth = depth;
		depth = newDepth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PerspectiveDslPackage.PERSPECTIVE_SEARCH__DEPTH, oldDepth, depth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getFilterCols() {
		return filterCols;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFilterCols(int newFilterCols) {
		int oldFilterCols = filterCols;
		filterCols = newFilterCols;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PerspectiveDslPackage.PERSPECTIVE_SEARCH__FILTER_COLS, oldFilterCols, filterCols));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PerspectiveDslPackage.PERSPECTIVE_SEARCH__DTO_REF:
				if (resolve) return getDtoRef();
				return basicGetDtoRef();
			case PerspectiveDslPackage.PERSPECTIVE_SEARCH__DEPTH:
				return getDepth();
			case PerspectiveDslPackage.PERSPECTIVE_SEARCH__FILTER_COLS:
				return getFilterCols();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PerspectiveDslPackage.PERSPECTIVE_SEARCH__DTO_REF:
				setDtoRef((LDto)newValue);
				return;
			case PerspectiveDslPackage.PERSPECTIVE_SEARCH__DEPTH:
				setDepth((Integer)newValue);
				return;
			case PerspectiveDslPackage.PERSPECTIVE_SEARCH__FILTER_COLS:
				setFilterCols((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PerspectiveDslPackage.PERSPECTIVE_SEARCH__DTO_REF:
				setDtoRef((LDto)null);
				return;
			case PerspectiveDslPackage.PERSPECTIVE_SEARCH__DEPTH:
				setDepth(DEPTH_EDEFAULT);
				return;
			case PerspectiveDslPackage.PERSPECTIVE_SEARCH__FILTER_COLS:
				setFilterCols(FILTER_COLS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PerspectiveDslPackage.PERSPECTIVE_SEARCH__DTO_REF:
				return dtoRef != null;
			case PerspectiveDslPackage.PERSPECTIVE_SEARCH__DEPTH:
				return depth != DEPTH_EDEFAULT;
			case PerspectiveDslPackage.PERSPECTIVE_SEARCH__FILTER_COLS:
				return filterCols != FILTER_COLS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (depth: ");
		result.append(depth);
		result.append(", filterCols: ");
		result.append(filterCols);
		result.append(')');
		return result.toString();
	}

} //PerspectiveSearchImpl
