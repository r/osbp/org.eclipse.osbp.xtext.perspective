/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.perspective;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel fileExtensions='perspective' modelName='PerspectiveDsl' prefix='PerspectiveDsl' updateClasspath='false' loadInitialization='false' literalsInterface='true' copyrightText='Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)\r\n All rights reserved. This program and the accompanying materials \r\n are made available under the terms of the Eclipse Public License 2.0  \r\n which accompanies this distribution, and is available at \r\n https://www.eclipse.org/legal/epl-2.0/ \r\n \r\n SPDX-License-Identifier: EPL-2.0 \r\n\r\n Based on ideas from Xtext, Xtend, Xcore\r\n  \r\n Contributors:  \r\n \t\tJoerg Riegel - Initial implementation \r\n ' basePackage='org.eclipse.osbp.xtext'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore rootPackage='perspectivedsl'"
 * @generated
 */
public interface PerspectiveDslPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "perspective";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://osbp.eclipse.org/xtext/perspective/PerspectiveDsl";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "perspectivedsl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PerspectiveDslPackage eINSTANCE = org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveModelImpl <em>Perspective Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveModelImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveModel()
	 * @generated
	 */
	int PERSPECTIVE_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Import Section</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_MODEL__IMPORT_SECTION = 0;

	/**
	 * The feature id for the '<em><b>Packages</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_MODEL__PACKAGES = 1;

	/**
	 * The number of structural features of the '<em>Perspective Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_MODEL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Perspective Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveLazyResolverImpl <em>Perspective Lazy Resolver</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveLazyResolverImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveLazyResolver()
	 * @generated
	 */
	int PERSPECTIVE_LAZY_RESOLVER = 1;

	/**
	 * The number of structural features of the '<em>Perspective Lazy Resolver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_LAZY_RESOLVER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT = 0;

	/**
	 * The number of operations of the '<em>Perspective Lazy Resolver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_LAZY_RESOLVER_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectivePackageImpl <em>Perspective Package</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectivePackageImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectivePackage()
	 * @generated
	 */
	int PERSPECTIVE_PACKAGE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PACKAGE__NAME = OSBPTypesPackage.LPACKAGE__NAME;

	/**
	 * The feature id for the '<em><b>Perspectives</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PACKAGE__PERSPECTIVES = OSBPTypesPackage.LPACKAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Perspective Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PACKAGE_FEATURE_COUNT = OSBPTypesPackage.LPACKAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PACKAGE___ERESOLVE_PROXY__OBJECT = OSBPTypesPackage.LPACKAGE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PACKAGE_OPERATION_COUNT = OSBPTypesPackage.LPACKAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveBaseImpl <em>Perspective Base</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveBaseImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveBase()
	 * @generated
	 */
	int PERSPECTIVE_BASE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_BASE__NAME = PERSPECTIVE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Perspective Base</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_BASE_FEATURE_COUNT = PERSPECTIVE_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_BASE___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Base</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_BASE_OPERATION_COUNT = PERSPECTIVE_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveImpl <em>Perspective</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspective()
	 * @generated
	 */
	int PERSPECTIVE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE__NAME = PERSPECTIVE_BASE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE__DESCRIPTION = PERSPECTIVE_BASE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE__DESCRIPTION_VALUE = PERSPECTIVE_BASE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Icon URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE__ICON_URI = PERSPECTIVE_BASE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Accessibility Phrase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE__ACCESSIBILITY_PHRASE = PERSPECTIVE_BASE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Process</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE__PROCESS = PERSPECTIVE_BASE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>User Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE__USER_TASK = PERSPECTIVE_BASE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Toolbar</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE__TOOLBAR = PERSPECTIVE_BASE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Toolbar Type Jvm</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE__TOOLBAR_TYPE_JVM = PERSPECTIVE_BASE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE__ELEMENTS = PERSPECTIVE_BASE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Eventmanager</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE__EVENTMANAGER = PERSPECTIVE_BASE_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>Perspective</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_FEATURE_COUNT = PERSPECTIVE_BASE_FEATURE_COUNT + 10;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_OPERATION_COUNT = PERSPECTIVE_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveEventManagerImpl <em>Perspective Event Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveEventManagerImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveEventManager()
	 * @generated
	 */
	int PERSPECTIVE_EVENT_MANAGER = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_EVENT_MANAGER__NAME = PERSPECTIVE_BASE__NAME;

	/**
	 * The feature id for the '<em><b>Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_EVENT_MANAGER__EVENTS = PERSPECTIVE_BASE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Perspective Event Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_EVENT_MANAGER_FEATURE_COUNT = PERSPECTIVE_BASE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_EVENT_MANAGER___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Event Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_EVENT_MANAGER_OPERATION_COUNT = PERSPECTIVE_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveEventImpl <em>Perspective Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveEventImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveEvent()
	 * @generated
	 */
	int PERSPECTIVE_EVENT = 6;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_EVENT__TARGET = 0;

	/**
	 * The feature id for the '<em><b>Allowedsources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_EVENT__ALLOWEDSOURCES = 1;

	/**
	 * The number of structural features of the '<em>Perspective Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_EVENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Perspective Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveElementImpl <em>Perspective Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveElementImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveElement()
	 * @generated
	 */
	int PERSPECTIVE_ELEMENT = 7;

	/**
	 * The feature id for the '<em><b>Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_ELEMENT__ELEMENT_ID = PERSPECTIVE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Accessibility Phrase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_ELEMENT__ACCESSIBILITY_PHRASE = PERSPECTIVE_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Container Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_ELEMENT__CONTAINER_DATA = PERSPECTIVE_LAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_ELEMENT__ELEMENTS = PERSPECTIVE_LAZY_RESOLVER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Perspective Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_ELEMENT_FEATURE_COUNT = PERSPECTIVE_LAZY_RESOLVER_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_ELEMENT___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_ELEMENT_OPERATION_COUNT = PERSPECTIVE_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveSashContainerImpl <em>Perspective Sash Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveSashContainerImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveSashContainer()
	 * @generated
	 */
	int PERSPECTIVE_SASH_CONTAINER = 8;

	/**
	 * The feature id for the '<em><b>Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SASH_CONTAINER__ELEMENT_ID = PERSPECTIVE_ELEMENT__ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Accessibility Phrase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SASH_CONTAINER__ACCESSIBILITY_PHRASE = PERSPECTIVE_ELEMENT__ACCESSIBILITY_PHRASE;

	/**
	 * The feature id for the '<em><b>Container Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SASH_CONTAINER__CONTAINER_DATA = PERSPECTIVE_ELEMENT__CONTAINER_DATA;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SASH_CONTAINER__ELEMENTS = PERSPECTIVE_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Orientation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SASH_CONTAINER__ORIENTATION = PERSPECTIVE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Selected Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SASH_CONTAINER__SELECTED_ELEMENT = PERSPECTIVE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Perspective Sash Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SASH_CONTAINER_FEATURE_COUNT = PERSPECTIVE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SASH_CONTAINER___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_ELEMENT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Sash Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SASH_CONTAINER_OPERATION_COUNT = PERSPECTIVE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectivePartImpl <em>Perspective Part</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectivePartImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectivePart()
	 * @generated
	 */
	int PERSPECTIVE_PART = 9;

	/**
	 * The feature id for the '<em><b>Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART__ELEMENT_ID = PERSPECTIVE_ELEMENT__ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Accessibility Phrase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART__ACCESSIBILITY_PHRASE = PERSPECTIVE_ELEMENT__ACCESSIBILITY_PHRASE;

	/**
	 * The feature id for the '<em><b>Container Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART__CONTAINER_DATA = PERSPECTIVE_ELEMENT__CONTAINER_DATA;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART__ELEMENTS = PERSPECTIVE_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Icon URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART__ICON_URI = PERSPECTIVE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>View</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART__VIEW = PERSPECTIVE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Is Closable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART__IS_CLOSABLE = PERSPECTIVE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Perspective Part</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART_FEATURE_COUNT = PERSPECTIVE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_ELEMENT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Part</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART_OPERATION_COUNT = PERSPECTIVE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectivePartStackImpl <em>Perspective Part Stack</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectivePartStackImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectivePartStack()
	 * @generated
	 */
	int PERSPECTIVE_PART_STACK = 10;

	/**
	 * The feature id for the '<em><b>Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART_STACK__ELEMENT_ID = PERSPECTIVE_ELEMENT__ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Accessibility Phrase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART_STACK__ACCESSIBILITY_PHRASE = PERSPECTIVE_ELEMENT__ACCESSIBILITY_PHRASE;

	/**
	 * The feature id for the '<em><b>Container Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART_STACK__CONTAINER_DATA = PERSPECTIVE_ELEMENT__CONTAINER_DATA;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART_STACK__ELEMENTS = PERSPECTIVE_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Selected Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART_STACK__SELECTED_ELEMENT = PERSPECTIVE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Synchronize</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART_STACK__SYNCHRONIZE = PERSPECTIVE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Perspective Part Stack</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART_STACK_FEATURE_COUNT = PERSPECTIVE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART_STACK___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_ELEMENT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Part Stack</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_PART_STACK_OPERATION_COUNT = PERSPECTIVE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.PerspectiveView <em>Perspective View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveView
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveView()
	 * @generated
	 */
	int PERSPECTIVE_VIEW = 11;

	/**
	 * The number of structural features of the '<em>Perspective View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_VIEW_FEATURE_COUNT = PERSPECTIVE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_VIEW___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_VIEW_OPERATION_COUNT = PERSPECTIVE_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveSelectionImpl <em>Perspective Selection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveSelectionImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveSelection()
	 * @generated
	 */
	int PERSPECTIVE_SELECTION = 12;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SELECTION__REF = PERSPECTIVE_VIEW_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Perspective Selection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SELECTION_FEATURE_COUNT = PERSPECTIVE_VIEW_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SELECTION___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_VIEW___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Selection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SELECTION_OPERATION_COUNT = PERSPECTIVE_VIEW_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveTableImpl <em>Perspective Table</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveTableImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveTable()
	 * @generated
	 */
	int PERSPECTIVE_TABLE = 13;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_TABLE__REF = PERSPECTIVE_VIEW_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Perspective Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_TABLE_FEATURE_COUNT = PERSPECTIVE_VIEW_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_TABLE___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_VIEW___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_TABLE_OPERATION_COUNT = PERSPECTIVE_VIEW_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveGridImpl <em>Perspective Grid</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveGridImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveGrid()
	 * @generated
	 */
	int PERSPECTIVE_GRID = 14;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_GRID__REF = PERSPECTIVE_VIEW_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Perspective Grid</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_GRID_FEATURE_COUNT = PERSPECTIVE_VIEW_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_GRID___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_VIEW___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Grid</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_GRID_OPERATION_COUNT = PERSPECTIVE_VIEW_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveChartImpl <em>Perspective Chart</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveChartImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveChart()
	 * @generated
	 */
	int PERSPECTIVE_CHART = 15;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_CHART__REF = PERSPECTIVE_VIEW_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Perspective Chart</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_CHART_FEATURE_COUNT = PERSPECTIVE_VIEW_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_CHART___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_VIEW___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Chart</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_CHART_OPERATION_COUNT = PERSPECTIVE_VIEW_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveReportImpl <em>Perspective Report</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveReportImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveReport()
	 * @generated
	 */
	int PERSPECTIVE_REPORT = 16;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_REPORT__REF = PERSPECTIVE_VIEW_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Perspective Report</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_REPORT_FEATURE_COUNT = PERSPECTIVE_VIEW_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_REPORT___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_VIEW___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Report</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_REPORT_OPERATION_COUNT = PERSPECTIVE_VIEW_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveOrganizationImpl <em>Perspective Organization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveOrganizationImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveOrganization()
	 * @generated
	 */
	int PERSPECTIVE_ORGANIZATION = 17;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_ORGANIZATION__REF = PERSPECTIVE_VIEW_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Perspective Organization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_ORGANIZATION_FEATURE_COUNT = PERSPECTIVE_VIEW_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_ORGANIZATION___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_VIEW___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Organization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_ORGANIZATION_OPERATION_COUNT = PERSPECTIVE_VIEW_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveTopologyImpl <em>Perspective Topology</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveTopologyImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveTopology()
	 * @generated
	 */
	int PERSPECTIVE_TOPOLOGY = 18;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_TOPOLOGY__REF = PERSPECTIVE_VIEW_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Perspective Topology</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_TOPOLOGY_FEATURE_COUNT = PERSPECTIVE_VIEW_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_TOPOLOGY___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_VIEW___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Topology</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_TOPOLOGY_OPERATION_COUNT = PERSPECTIVE_VIEW_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveDialogImpl <em>Perspective Dialog</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDialogImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveDialog()
	 * @generated
	 */
	int PERSPECTIVE_DIALOG = 19;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_DIALOG__REF = PERSPECTIVE_VIEW_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Perspective Dialog</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_DIALOG_FEATURE_COUNT = PERSPECTIVE_VIEW_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_DIALOG___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_VIEW___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Dialog</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_DIALOG_OPERATION_COUNT = PERSPECTIVE_VIEW_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveBPMNImpl <em>Perspective BPMN</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveBPMNImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveBPMN()
	 * @generated
	 */
	int PERSPECTIVE_BPMN = 20;

	/**
	 * The number of structural features of the '<em>Perspective BPMN</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_BPMN_FEATURE_COUNT = PERSPECTIVE_VIEW_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_BPMN___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_VIEW___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective BPMN</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_BPMN_OPERATION_COUNT = PERSPECTIVE_VIEW_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveKanbanImpl <em>Perspective Kanban</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveKanbanImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveKanban()
	 * @generated
	 */
	int PERSPECTIVE_KANBAN = 21;

	/**
	 * The feature id for the '<em><b>Dto Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_KANBAN__DTO_REF = PERSPECTIVE_VIEW_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Card Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_KANBAN__CARD_REF = PERSPECTIVE_VIEW_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Dialog Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_KANBAN__DIALOG_REF = PERSPECTIVE_VIEW_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Perspective Kanban</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_KANBAN_FEATURE_COUNT = PERSPECTIVE_VIEW_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_KANBAN___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_VIEW___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Kanban</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_KANBAN_OPERATION_COUNT = PERSPECTIVE_VIEW_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveWelcomeImpl <em>Perspective Welcome</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveWelcomeImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveWelcome()
	 * @generated
	 */
	int PERSPECTIVE_WELCOME = 22;

	/**
	 * The number of structural features of the '<em>Perspective Welcome</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_WELCOME_FEATURE_COUNT = PERSPECTIVE_VIEW_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_WELCOME___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_VIEW___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Welcome</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_WELCOME_OPERATION_COUNT = PERSPECTIVE_VIEW_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveSaikuImpl <em>Perspective Saiku</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveSaikuImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveSaiku()
	 * @generated
	 */
	int PERSPECTIVE_SAIKU = 23;

	/**
	 * The number of structural features of the '<em>Perspective Saiku</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SAIKU_FEATURE_COUNT = PERSPECTIVE_VIEW_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SAIKU___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_VIEW___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Saiku</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SAIKU_OPERATION_COUNT = PERSPECTIVE_VIEW_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveSearchImpl <em>Perspective Search</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveSearchImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveSearch()
	 * @generated
	 */
	int PERSPECTIVE_SEARCH = 24;

	/**
	 * The feature id for the '<em><b>Dto Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SEARCH__DTO_REF = PERSPECTIVE_VIEW_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SEARCH__DEPTH = PERSPECTIVE_VIEW_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Filter Cols</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SEARCH__FILTER_COLS = PERSPECTIVE_VIEW_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Perspective Search</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SEARCH_FEATURE_COUNT = PERSPECTIVE_VIEW_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SEARCH___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_VIEW___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Search</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_SEARCH_OPERATION_COUNT = PERSPECTIVE_VIEW_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveDataInterchangeImpl <em>Perspective Data Interchange</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDataInterchangeImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveDataInterchange()
	 * @generated
	 */
	int PERSPECTIVE_DATA_INTERCHANGE = 25;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_DATA_INTERCHANGE__REF = PERSPECTIVE_VIEW_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Perspective Data Interchange</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_DATA_INTERCHANGE_FEATURE_COUNT = PERSPECTIVE_VIEW_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_DATA_INTERCHANGE___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_VIEW___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Data Interchange</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_DATA_INTERCHANGE_OPERATION_COUNT = PERSPECTIVE_VIEW_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveTitleImpl <em>Perspective Title</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveTitleImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveTitle()
	 * @generated
	 */
	int PERSPECTIVE_TITLE = 26;

	/**
	 * The feature id for the '<em><b>Html Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_TITLE__HTML_NAME = PERSPECTIVE_VIEW_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Perspective Title</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_TITLE_FEATURE_COUNT = PERSPECTIVE_VIEW_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_TITLE___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_VIEW___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Title</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_TITLE_OPERATION_COUNT = PERSPECTIVE_VIEW_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveDashboardImpl <em>Perspective Dashboard</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDashboardImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveDashboard()
	 * @generated
	 */
	int PERSPECTIVE_DASHBOARD = 27;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_DASHBOARD__NAME = PERSPECTIVE_VIEW_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Perspective Dashboard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_DASHBOARD_FEATURE_COUNT = PERSPECTIVE_VIEW_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_DASHBOARD___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_VIEW___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Dashboard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_DASHBOARD_OPERATION_COUNT = PERSPECTIVE_VIEW_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveBrowserImpl <em>Perspective Browser</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveBrowserImpl
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveBrowser()
	 * @generated
	 */
	int PERSPECTIVE_BROWSER = 28;

	/**
	 * The feature id for the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_BROWSER__URL = PERSPECTIVE_VIEW_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cube View</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_BROWSER__CUBE_VIEW = PERSPECTIVE_VIEW_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Perspective Browser</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_BROWSER_FEATURE_COUNT = PERSPECTIVE_VIEW_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_BROWSER___ERESOLVE_PROXY__INTERNALEOBJECT = PERSPECTIVE_VIEW___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Perspective Browser</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSPECTIVE_BROWSER_OPERATION_COUNT = PERSPECTIVE_VIEW_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.perspective.SashOrientation <em>Sash Orientation</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.perspective.SashOrientation
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getSashOrientation()
	 * @generated
	 */
	int SASH_ORIENTATION = 29;

	/**
	 * The meta object id for the '<em>Internal EObject</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.InternalEObject
	 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getInternalEObject()
	 * @generated
	 */
	int INTERNAL_EOBJECT = 30;


	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveModel <em>Perspective Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Model</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveModel
	 * @generated
	 */
	EClass getPerspectiveModel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.perspective.PerspectiveModel#getImportSection <em>Import Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Import Section</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveModel#getImportSection()
	 * @see #getPerspectiveModel()
	 * @generated
	 */
	EReference getPerspectiveModel_ImportSection();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.perspective.PerspectiveModel#getPackages <em>Packages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Packages</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveModel#getPackages()
	 * @see #getPerspectiveModel()
	 * @generated
	 */
	EReference getPerspectiveModel_Packages();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveLazyResolver <em>Perspective Lazy Resolver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Lazy Resolver</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveLazyResolver
	 * @generated
	 */
	EClass getPerspectiveLazyResolver();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.xtext.perspective.PerspectiveLazyResolver#eResolveProxy(org.eclipse.emf.ecore.InternalEObject) <em>EResolve Proxy</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>EResolve Proxy</em>' operation.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveLazyResolver#eResolveProxy(org.eclipse.emf.ecore.InternalEObject)
	 * @generated
	 */
	EOperation getPerspectiveLazyResolver__EResolveProxy__InternalEObject();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectivePackage <em>Perspective Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Package</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectivePackage
	 * @generated
	 */
	EClass getPerspectivePackage();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.perspective.PerspectivePackage#getPerspectives <em>Perspectives</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Perspectives</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectivePackage#getPerspectives()
	 * @see #getPerspectivePackage()
	 * @generated
	 */
	EReference getPerspectivePackage_Perspectives();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveBase <em>Perspective Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Base</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveBase
	 * @generated
	 */
	EClass getPerspectiveBase();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.perspective.PerspectiveBase#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveBase#getName()
	 * @see #getPerspectiveBase()
	 * @generated
	 */
	EAttribute getPerspectiveBase_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.Perspective <em>Perspective</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.Perspective
	 * @generated
	 */
	EClass getPerspective();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.perspective.Perspective#isDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.Perspective#isDescription()
	 * @see #getPerspective()
	 * @generated
	 */
	EAttribute getPerspective_Description();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.perspective.Perspective#getDescriptionValue <em>Description Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description Value</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.Perspective#getDescriptionValue()
	 * @see #getPerspective()
	 * @generated
	 */
	EAttribute getPerspective_DescriptionValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.perspective.Perspective#getIconURI <em>Icon URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Icon URI</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.Perspective#getIconURI()
	 * @see #getPerspective()
	 * @generated
	 */
	EAttribute getPerspective_IconURI();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.perspective.Perspective#getAccessibilityPhrase <em>Accessibility Phrase</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Accessibility Phrase</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.Perspective#getAccessibilityPhrase()
	 * @see #getPerspective()
	 * @generated
	 */
	EAttribute getPerspective_AccessibilityPhrase();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.Perspective#getProcess <em>Process</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Process</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.Perspective#getProcess()
	 * @see #getPerspective()
	 * @generated
	 */
	EReference getPerspective_Process();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.Perspective#getUserTask <em>User Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>User Task</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.Perspective#getUserTask()
	 * @see #getPerspective()
	 * @generated
	 */
	EReference getPerspective_UserTask();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.Perspective#getToolbar <em>Toolbar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Toolbar</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.Perspective#getToolbar()
	 * @see #getPerspective()
	 * @generated
	 */
	EReference getPerspective_Toolbar();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.perspective.Perspective#getToolbarTypeJvm <em>Toolbar Type Jvm</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Toolbar Type Jvm</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.Perspective#getToolbarTypeJvm()
	 * @see #getPerspective()
	 * @generated
	 */
	EReference getPerspective_ToolbarTypeJvm();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.perspective.Perspective#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.Perspective#getElements()
	 * @see #getPerspective()
	 * @generated
	 */
	EReference getPerspective_Elements();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.perspective.Perspective#getEventmanager <em>Eventmanager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Eventmanager</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.Perspective#getEventmanager()
	 * @see #getPerspective()
	 * @generated
	 */
	EReference getPerspective_Eventmanager();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveEventManager <em>Perspective Event Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Event Manager</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveEventManager
	 * @generated
	 */
	EClass getPerspectiveEventManager();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.perspective.PerspectiveEventManager#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Events</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveEventManager#getEvents()
	 * @see #getPerspectiveEventManager()
	 * @generated
	 */
	EReference getPerspectiveEventManager_Events();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveEvent <em>Perspective Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Event</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveEvent
	 * @generated
	 */
	EClass getPerspectiveEvent();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.PerspectiveEvent#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveEvent#getTarget()
	 * @see #getPerspectiveEvent()
	 * @generated
	 */
	EReference getPerspectiveEvent_Target();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.osbp.xtext.perspective.PerspectiveEvent#getAllowedsources <em>Allowedsources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Allowedsources</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveEvent#getAllowedsources()
	 * @see #getPerspectiveEvent()
	 * @generated
	 */
	EReference getPerspectiveEvent_Allowedsources();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveElement <em>Perspective Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Element</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveElement
	 * @generated
	 */
	EClass getPerspectiveElement();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.perspective.PerspectiveElement#getElementId <em>Element Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Element Id</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveElement#getElementId()
	 * @see #getPerspectiveElement()
	 * @generated
	 */
	EAttribute getPerspectiveElement_ElementId();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.perspective.PerspectiveElement#getAccessibilityPhrase <em>Accessibility Phrase</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Accessibility Phrase</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveElement#getAccessibilityPhrase()
	 * @see #getPerspectiveElement()
	 * @generated
	 */
	EAttribute getPerspectiveElement_AccessibilityPhrase();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.perspective.PerspectiveElement#getContainerData <em>Container Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Container Data</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveElement#getContainerData()
	 * @see #getPerspectiveElement()
	 * @generated
	 */
	EAttribute getPerspectiveElement_ContainerData();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.perspective.PerspectiveElement#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveElement#getElements()
	 * @see #getPerspectiveElement()
	 * @generated
	 */
	EReference getPerspectiveElement_Elements();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveSashContainer <em>Perspective Sash Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Sash Container</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveSashContainer
	 * @generated
	 */
	EClass getPerspectiveSashContainer();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.perspective.PerspectiveSashContainer#getOrientation <em>Orientation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Orientation</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveSashContainer#getOrientation()
	 * @see #getPerspectiveSashContainer()
	 * @generated
	 */
	EAttribute getPerspectiveSashContainer_Orientation();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.PerspectiveSashContainer#getSelectedElement <em>Selected Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Selected Element</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveSashContainer#getSelectedElement()
	 * @see #getPerspectiveSashContainer()
	 * @generated
	 */
	EReference getPerspectiveSashContainer_SelectedElement();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectivePart <em>Perspective Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Part</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectivePart
	 * @generated
	 */
	EClass getPerspectivePart();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.perspective.PerspectivePart#getIconURI <em>Icon URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Icon URI</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectivePart#getIconURI()
	 * @see #getPerspectivePart()
	 * @generated
	 */
	EAttribute getPerspectivePart_IconURI();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.perspective.PerspectivePart#getView <em>View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>View</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectivePart#getView()
	 * @see #getPerspectivePart()
	 * @generated
	 */
	EReference getPerspectivePart_View();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.perspective.PerspectivePart#isIsClosable <em>Is Closable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Closable</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectivePart#isIsClosable()
	 * @see #getPerspectivePart()
	 * @generated
	 */
	EAttribute getPerspectivePart_IsClosable();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectivePartStack <em>Perspective Part Stack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Part Stack</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectivePartStack
	 * @generated
	 */
	EClass getPerspectivePartStack();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.PerspectivePartStack#getSelectedElement <em>Selected Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Selected Element</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectivePartStack#getSelectedElement()
	 * @see #getPerspectivePartStack()
	 * @generated
	 */
	EReference getPerspectivePartStack_SelectedElement();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.PerspectivePartStack#getSynchronize <em>Synchronize</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectivePartStack#getSynchronize()
	 * @see #getPerspectivePartStack()
	 * @generated
	 */
	EReference getPerspectivePartStack_Synchronize();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveView <em>Perspective View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective View</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveView
	 * @generated
	 */
	EClass getPerspectiveView();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveSelection <em>Perspective Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Selection</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveSelection
	 * @generated
	 */
	EClass getPerspectiveSelection();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.PerspectiveSelection#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveSelection#getRef()
	 * @see #getPerspectiveSelection()
	 * @generated
	 */
	EReference getPerspectiveSelection_Ref();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveTable <em>Perspective Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Table</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveTable
	 * @generated
	 */
	EClass getPerspectiveTable();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.PerspectiveTable#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveTable#getRef()
	 * @see #getPerspectiveTable()
	 * @generated
	 */
	EReference getPerspectiveTable_Ref();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveGrid <em>Perspective Grid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Grid</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveGrid
	 * @generated
	 */
	EClass getPerspectiveGrid();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.PerspectiveGrid#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveGrid#getRef()
	 * @see #getPerspectiveGrid()
	 * @generated
	 */
	EReference getPerspectiveGrid_Ref();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveChart <em>Perspective Chart</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Chart</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveChart
	 * @generated
	 */
	EClass getPerspectiveChart();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.PerspectiveChart#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveChart#getRef()
	 * @see #getPerspectiveChart()
	 * @generated
	 */
	EReference getPerspectiveChart_Ref();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveReport <em>Perspective Report</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Report</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveReport
	 * @generated
	 */
	EClass getPerspectiveReport();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.PerspectiveReport#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveReport#getRef()
	 * @see #getPerspectiveReport()
	 * @generated
	 */
	EReference getPerspectiveReport_Ref();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveOrganization <em>Perspective Organization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Organization</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveOrganization
	 * @generated
	 */
	EClass getPerspectiveOrganization();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.PerspectiveOrganization#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveOrganization#getRef()
	 * @see #getPerspectiveOrganization()
	 * @generated
	 */
	EReference getPerspectiveOrganization_Ref();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveTopology <em>Perspective Topology</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Topology</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveTopology
	 * @generated
	 */
	EClass getPerspectiveTopology();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.PerspectiveTopology#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveTopology#getRef()
	 * @see #getPerspectiveTopology()
	 * @generated
	 */
	EReference getPerspectiveTopology_Ref();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveDialog <em>Perspective Dialog</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Dialog</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDialog
	 * @generated
	 */
	EClass getPerspectiveDialog();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.PerspectiveDialog#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDialog#getRef()
	 * @see #getPerspectiveDialog()
	 * @generated
	 */
	EReference getPerspectiveDialog_Ref();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveBPMN <em>Perspective BPMN</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective BPMN</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveBPMN
	 * @generated
	 */
	EClass getPerspectiveBPMN();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveKanban <em>Perspective Kanban</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Kanban</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveKanban
	 * @generated
	 */
	EClass getPerspectiveKanban();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.PerspectiveKanban#getDtoRef <em>Dto Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dto Ref</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveKanban#getDtoRef()
	 * @see #getPerspectiveKanban()
	 * @generated
	 */
	EReference getPerspectiveKanban_DtoRef();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.PerspectiveKanban#getCardRef <em>Card Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Card Ref</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveKanban#getCardRef()
	 * @see #getPerspectiveKanban()
	 * @generated
	 */
	EReference getPerspectiveKanban_CardRef();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.PerspectiveKanban#getDialogRef <em>Dialog Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dialog Ref</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveKanban#getDialogRef()
	 * @see #getPerspectiveKanban()
	 * @generated
	 */
	EReference getPerspectiveKanban_DialogRef();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveWelcome <em>Perspective Welcome</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Welcome</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveWelcome
	 * @generated
	 */
	EClass getPerspectiveWelcome();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveSaiku <em>Perspective Saiku</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Saiku</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveSaiku
	 * @generated
	 */
	EClass getPerspectiveSaiku();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveSearch <em>Perspective Search</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Search</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveSearch
	 * @generated
	 */
	EClass getPerspectiveSearch();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.PerspectiveSearch#getDtoRef <em>Dto Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dto Ref</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveSearch#getDtoRef()
	 * @see #getPerspectiveSearch()
	 * @generated
	 */
	EReference getPerspectiveSearch_DtoRef();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.perspective.PerspectiveSearch#getDepth <em>Depth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Depth</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveSearch#getDepth()
	 * @see #getPerspectiveSearch()
	 * @generated
	 */
	EAttribute getPerspectiveSearch_Depth();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.perspective.PerspectiveSearch#getFilterCols <em>Filter Cols</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filter Cols</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveSearch#getFilterCols()
	 * @see #getPerspectiveSearch()
	 * @generated
	 */
	EAttribute getPerspectiveSearch_FilterCols();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveDataInterchange <em>Perspective Data Interchange</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Data Interchange</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDataInterchange
	 * @generated
	 */
	EClass getPerspectiveDataInterchange();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.perspective.PerspectiveDataInterchange#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDataInterchange#getRef()
	 * @see #getPerspectiveDataInterchange()
	 * @generated
	 */
	EReference getPerspectiveDataInterchange_Ref();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveTitle <em>Perspective Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Title</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveTitle
	 * @generated
	 */
	EClass getPerspectiveTitle();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.perspective.PerspectiveTitle#getHtmlName <em>Html Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Html Name</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveTitle#getHtmlName()
	 * @see #getPerspectiveTitle()
	 * @generated
	 */
	EAttribute getPerspectiveTitle_HtmlName();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveDashboard <em>Perspective Dashboard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Dashboard</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDashboard
	 * @generated
	 */
	EClass getPerspectiveDashboard();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.perspective.PerspectiveDashboard#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDashboard#getName()
	 * @see #getPerspectiveDashboard()
	 * @generated
	 */
	EAttribute getPerspectiveDashboard_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveBrowser <em>Perspective Browser</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perspective Browser</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveBrowser
	 * @generated
	 */
	EClass getPerspectiveBrowser();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.perspective.PerspectiveBrowser#getUrl <em>Url</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Url</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveBrowser#getUrl()
	 * @see #getPerspectiveBrowser()
	 * @generated
	 */
	EAttribute getPerspectiveBrowser_Url();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.perspective.PerspectiveBrowser#isCubeView <em>Cube View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cube View</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveBrowser#isCubeView()
	 * @see #getPerspectiveBrowser()
	 * @generated
	 */
	EAttribute getPerspectiveBrowser_CubeView();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.perspective.SashOrientation <em>Sash Orientation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Sash Orientation</em>'.
	 * @see org.eclipse.osbp.xtext.perspective.SashOrientation
	 * @generated
	 */
	EEnum getSashOrientation();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.ecore.InternalEObject <em>Internal EObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Internal EObject</em>'.
	 * @see org.eclipse.emf.ecore.InternalEObject
	 * @model instanceClass="org.eclipse.emf.ecore.InternalEObject"
	 * @generated
	 */
	EDataType getInternalEObject();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PerspectiveDslFactory getPerspectiveDslFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveModelImpl <em>Perspective Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveModelImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveModel()
		 * @generated
		 */
		EClass PERSPECTIVE_MODEL = eINSTANCE.getPerspectiveModel();

		/**
		 * The meta object literal for the '<em><b>Import Section</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_MODEL__IMPORT_SECTION = eINSTANCE.getPerspectiveModel_ImportSection();

		/**
		 * The meta object literal for the '<em><b>Packages</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_MODEL__PACKAGES = eINSTANCE.getPerspectiveModel_Packages();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveLazyResolverImpl <em>Perspective Lazy Resolver</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveLazyResolverImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveLazyResolver()
		 * @generated
		 */
		EClass PERSPECTIVE_LAZY_RESOLVER = eINSTANCE.getPerspectiveLazyResolver();

		/**
		 * The meta object literal for the '<em><b>EResolve Proxy</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PERSPECTIVE_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT = eINSTANCE.getPerspectiveLazyResolver__EResolveProxy__InternalEObject();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectivePackageImpl <em>Perspective Package</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectivePackageImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectivePackage()
		 * @generated
		 */
		EClass PERSPECTIVE_PACKAGE = eINSTANCE.getPerspectivePackage();

		/**
		 * The meta object literal for the '<em><b>Perspectives</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_PACKAGE__PERSPECTIVES = eINSTANCE.getPerspectivePackage_Perspectives();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveBaseImpl <em>Perspective Base</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveBaseImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveBase()
		 * @generated
		 */
		EClass PERSPECTIVE_BASE = eINSTANCE.getPerspectiveBase();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSPECTIVE_BASE__NAME = eINSTANCE.getPerspectiveBase_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveImpl <em>Perspective</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspective()
		 * @generated
		 */
		EClass PERSPECTIVE = eINSTANCE.getPerspective();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSPECTIVE__DESCRIPTION = eINSTANCE.getPerspective_Description();

		/**
		 * The meta object literal for the '<em><b>Description Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSPECTIVE__DESCRIPTION_VALUE = eINSTANCE.getPerspective_DescriptionValue();

		/**
		 * The meta object literal for the '<em><b>Icon URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSPECTIVE__ICON_URI = eINSTANCE.getPerspective_IconURI();

		/**
		 * The meta object literal for the '<em><b>Accessibility Phrase</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSPECTIVE__ACCESSIBILITY_PHRASE = eINSTANCE.getPerspective_AccessibilityPhrase();

		/**
		 * The meta object literal for the '<em><b>Process</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE__PROCESS = eINSTANCE.getPerspective_Process();

		/**
		 * The meta object literal for the '<em><b>User Task</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE__USER_TASK = eINSTANCE.getPerspective_UserTask();

		/**
		 * The meta object literal for the '<em><b>Toolbar</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE__TOOLBAR = eINSTANCE.getPerspective_Toolbar();

		/**
		 * The meta object literal for the '<em><b>Toolbar Type Jvm</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE__TOOLBAR_TYPE_JVM = eINSTANCE.getPerspective_ToolbarTypeJvm();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE__ELEMENTS = eINSTANCE.getPerspective_Elements();

		/**
		 * The meta object literal for the '<em><b>Eventmanager</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE__EVENTMANAGER = eINSTANCE.getPerspective_Eventmanager();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveEventManagerImpl <em>Perspective Event Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveEventManagerImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveEventManager()
		 * @generated
		 */
		EClass PERSPECTIVE_EVENT_MANAGER = eINSTANCE.getPerspectiveEventManager();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_EVENT_MANAGER__EVENTS = eINSTANCE.getPerspectiveEventManager_Events();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveEventImpl <em>Perspective Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveEventImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveEvent()
		 * @generated
		 */
		EClass PERSPECTIVE_EVENT = eINSTANCE.getPerspectiveEvent();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_EVENT__TARGET = eINSTANCE.getPerspectiveEvent_Target();

		/**
		 * The meta object literal for the '<em><b>Allowedsources</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_EVENT__ALLOWEDSOURCES = eINSTANCE.getPerspectiveEvent_Allowedsources();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveElementImpl <em>Perspective Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveElementImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveElement()
		 * @generated
		 */
		EClass PERSPECTIVE_ELEMENT = eINSTANCE.getPerspectiveElement();

		/**
		 * The meta object literal for the '<em><b>Element Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSPECTIVE_ELEMENT__ELEMENT_ID = eINSTANCE.getPerspectiveElement_ElementId();

		/**
		 * The meta object literal for the '<em><b>Accessibility Phrase</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSPECTIVE_ELEMENT__ACCESSIBILITY_PHRASE = eINSTANCE.getPerspectiveElement_AccessibilityPhrase();

		/**
		 * The meta object literal for the '<em><b>Container Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSPECTIVE_ELEMENT__CONTAINER_DATA = eINSTANCE.getPerspectiveElement_ContainerData();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_ELEMENT__ELEMENTS = eINSTANCE.getPerspectiveElement_Elements();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveSashContainerImpl <em>Perspective Sash Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveSashContainerImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveSashContainer()
		 * @generated
		 */
		EClass PERSPECTIVE_SASH_CONTAINER = eINSTANCE.getPerspectiveSashContainer();

		/**
		 * The meta object literal for the '<em><b>Orientation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSPECTIVE_SASH_CONTAINER__ORIENTATION = eINSTANCE.getPerspectiveSashContainer_Orientation();

		/**
		 * The meta object literal for the '<em><b>Selected Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_SASH_CONTAINER__SELECTED_ELEMENT = eINSTANCE.getPerspectiveSashContainer_SelectedElement();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectivePartImpl <em>Perspective Part</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectivePartImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectivePart()
		 * @generated
		 */
		EClass PERSPECTIVE_PART = eINSTANCE.getPerspectivePart();

		/**
		 * The meta object literal for the '<em><b>Icon URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSPECTIVE_PART__ICON_URI = eINSTANCE.getPerspectivePart_IconURI();

		/**
		 * The meta object literal for the '<em><b>View</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_PART__VIEW = eINSTANCE.getPerspectivePart_View();

		/**
		 * The meta object literal for the '<em><b>Is Closable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSPECTIVE_PART__IS_CLOSABLE = eINSTANCE.getPerspectivePart_IsClosable();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectivePartStackImpl <em>Perspective Part Stack</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectivePartStackImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectivePartStack()
		 * @generated
		 */
		EClass PERSPECTIVE_PART_STACK = eINSTANCE.getPerspectivePartStack();

		/**
		 * The meta object literal for the '<em><b>Selected Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_PART_STACK__SELECTED_ELEMENT = eINSTANCE.getPerspectivePartStack_SelectedElement();

		/**
		 * The meta object literal for the '<em><b>Synchronize</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_PART_STACK__SYNCHRONIZE = eINSTANCE.getPerspectivePartStack_Synchronize();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.PerspectiveView <em>Perspective View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.PerspectiveView
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveView()
		 * @generated
		 */
		EClass PERSPECTIVE_VIEW = eINSTANCE.getPerspectiveView();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveSelectionImpl <em>Perspective Selection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveSelectionImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveSelection()
		 * @generated
		 */
		EClass PERSPECTIVE_SELECTION = eINSTANCE.getPerspectiveSelection();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_SELECTION__REF = eINSTANCE.getPerspectiveSelection_Ref();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveTableImpl <em>Perspective Table</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveTableImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveTable()
		 * @generated
		 */
		EClass PERSPECTIVE_TABLE = eINSTANCE.getPerspectiveTable();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_TABLE__REF = eINSTANCE.getPerspectiveTable_Ref();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveGridImpl <em>Perspective Grid</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveGridImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveGrid()
		 * @generated
		 */
		EClass PERSPECTIVE_GRID = eINSTANCE.getPerspectiveGrid();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_GRID__REF = eINSTANCE.getPerspectiveGrid_Ref();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveChartImpl <em>Perspective Chart</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveChartImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveChart()
		 * @generated
		 */
		EClass PERSPECTIVE_CHART = eINSTANCE.getPerspectiveChart();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_CHART__REF = eINSTANCE.getPerspectiveChart_Ref();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveReportImpl <em>Perspective Report</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveReportImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveReport()
		 * @generated
		 */
		EClass PERSPECTIVE_REPORT = eINSTANCE.getPerspectiveReport();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_REPORT__REF = eINSTANCE.getPerspectiveReport_Ref();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveOrganizationImpl <em>Perspective Organization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveOrganizationImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveOrganization()
		 * @generated
		 */
		EClass PERSPECTIVE_ORGANIZATION = eINSTANCE.getPerspectiveOrganization();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_ORGANIZATION__REF = eINSTANCE.getPerspectiveOrganization_Ref();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveTopologyImpl <em>Perspective Topology</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveTopologyImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveTopology()
		 * @generated
		 */
		EClass PERSPECTIVE_TOPOLOGY = eINSTANCE.getPerspectiveTopology();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_TOPOLOGY__REF = eINSTANCE.getPerspectiveTopology_Ref();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveDialogImpl <em>Perspective Dialog</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDialogImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveDialog()
		 * @generated
		 */
		EClass PERSPECTIVE_DIALOG = eINSTANCE.getPerspectiveDialog();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_DIALOG__REF = eINSTANCE.getPerspectiveDialog_Ref();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveBPMNImpl <em>Perspective BPMN</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveBPMNImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveBPMN()
		 * @generated
		 */
		EClass PERSPECTIVE_BPMN = eINSTANCE.getPerspectiveBPMN();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveKanbanImpl <em>Perspective Kanban</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveKanbanImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveKanban()
		 * @generated
		 */
		EClass PERSPECTIVE_KANBAN = eINSTANCE.getPerspectiveKanban();

		/**
		 * The meta object literal for the '<em><b>Dto Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_KANBAN__DTO_REF = eINSTANCE.getPerspectiveKanban_DtoRef();

		/**
		 * The meta object literal for the '<em><b>Card Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_KANBAN__CARD_REF = eINSTANCE.getPerspectiveKanban_CardRef();

		/**
		 * The meta object literal for the '<em><b>Dialog Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_KANBAN__DIALOG_REF = eINSTANCE.getPerspectiveKanban_DialogRef();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveWelcomeImpl <em>Perspective Welcome</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveWelcomeImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveWelcome()
		 * @generated
		 */
		EClass PERSPECTIVE_WELCOME = eINSTANCE.getPerspectiveWelcome();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveSaikuImpl <em>Perspective Saiku</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveSaikuImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveSaiku()
		 * @generated
		 */
		EClass PERSPECTIVE_SAIKU = eINSTANCE.getPerspectiveSaiku();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveSearchImpl <em>Perspective Search</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveSearchImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveSearch()
		 * @generated
		 */
		EClass PERSPECTIVE_SEARCH = eINSTANCE.getPerspectiveSearch();

		/**
		 * The meta object literal for the '<em><b>Dto Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_SEARCH__DTO_REF = eINSTANCE.getPerspectiveSearch_DtoRef();

		/**
		 * The meta object literal for the '<em><b>Depth</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSPECTIVE_SEARCH__DEPTH = eINSTANCE.getPerspectiveSearch_Depth();

		/**
		 * The meta object literal for the '<em><b>Filter Cols</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSPECTIVE_SEARCH__FILTER_COLS = eINSTANCE.getPerspectiveSearch_FilterCols();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveDataInterchangeImpl <em>Perspective Data Interchange</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDataInterchangeImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveDataInterchange()
		 * @generated
		 */
		EClass PERSPECTIVE_DATA_INTERCHANGE = eINSTANCE.getPerspectiveDataInterchange();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSPECTIVE_DATA_INTERCHANGE__REF = eINSTANCE.getPerspectiveDataInterchange_Ref();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveTitleImpl <em>Perspective Title</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveTitleImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveTitle()
		 * @generated
		 */
		EClass PERSPECTIVE_TITLE = eINSTANCE.getPerspectiveTitle();

		/**
		 * The meta object literal for the '<em><b>Html Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSPECTIVE_TITLE__HTML_NAME = eINSTANCE.getPerspectiveTitle_HtmlName();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveDashboardImpl <em>Perspective Dashboard</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDashboardImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveDashboard()
		 * @generated
		 */
		EClass PERSPECTIVE_DASHBOARD = eINSTANCE.getPerspectiveDashboard();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSPECTIVE_DASHBOARD__NAME = eINSTANCE.getPerspectiveDashboard_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.impl.PerspectiveBrowserImpl <em>Perspective Browser</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveBrowserImpl
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getPerspectiveBrowser()
		 * @generated
		 */
		EClass PERSPECTIVE_BROWSER = eINSTANCE.getPerspectiveBrowser();

		/**
		 * The meta object literal for the '<em><b>Url</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSPECTIVE_BROWSER__URL = eINSTANCE.getPerspectiveBrowser_Url();

		/**
		 * The meta object literal for the '<em><b>Cube View</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSPECTIVE_BROWSER__CUBE_VIEW = eINSTANCE.getPerspectiveBrowser_CubeView();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.perspective.SashOrientation <em>Sash Orientation</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.perspective.SashOrientation
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getSashOrientation()
		 * @generated
		 */
		EEnum SASH_ORIENTATION = eINSTANCE.getSashOrientation();

		/**
		 * The meta object literal for the '<em>Internal EObject</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.ecore.InternalEObject
		 * @see org.eclipse.osbp.xtext.perspective.impl.PerspectiveDslPackageImpl#getInternalEObject()
		 * @generated
		 */
		EDataType INTERNAL_EOBJECT = eINSTANCE.getInternalEObject();

	}

} //PerspectiveDslPackage
