/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.perspective;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Perspective Sash Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.PerspectiveSashContainer#getOrientation <em>Orientation</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.PerspectiveSashContainer#getSelectedElement <em>Selected Element</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectiveSashContainer()
 * @model
 * @generated
 */
public interface PerspectiveSashContainer extends PerspectiveElement {
	/**
	 * Returns the value of the '<em><b>Orientation</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.perspective.SashOrientation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Orientation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Orientation</em>' attribute.
	 * @see org.eclipse.osbp.xtext.perspective.SashOrientation
	 * @see #setOrientation(SashOrientation)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectiveSashContainer_Orientation()
	 * @model unique="false"
	 * @generated
	 */
	SashOrientation getOrientation();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.PerspectiveSashContainer#getOrientation <em>Orientation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Orientation</em>' attribute.
	 * @see org.eclipse.osbp.xtext.perspective.SashOrientation
	 * @see #getOrientation()
	 * @generated
	 */
	void setOrientation(SashOrientation value);

	/**
	 * Returns the value of the '<em><b>Selected Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selected Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selected Element</em>' reference.
	 * @see #setSelectedElement(PerspectiveElement)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectiveSashContainer_SelectedElement()
	 * @model
	 * @generated
	 */
	PerspectiveElement getSelectedElement();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.PerspectiveSashContainer#getSelectedElement <em>Selected Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selected Element</em>' reference.
	 * @see #getSelectedElement()
	 * @generated
	 */
	void setSelectedElement(PerspectiveElement value);

} // PerspectiveSashContainer
