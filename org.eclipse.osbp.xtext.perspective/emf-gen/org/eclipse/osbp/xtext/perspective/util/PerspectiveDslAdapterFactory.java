/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.perspective.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver;
import org.eclipse.osbp.dsl.semantic.common.types.LPackage;

import org.eclipse.osbp.xtext.perspective.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage
 * @generated
 */
public class PerspectiveDslAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static PerspectiveDslPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveDslAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = PerspectiveDslPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PerspectiveDslSwitch<Adapter> modelSwitch =
		new PerspectiveDslSwitch<Adapter>() {
			@Override
			public Adapter casePerspectiveModel(PerspectiveModel object) {
				return createPerspectiveModelAdapter();
			}
			@Override
			public Adapter casePerspectiveLazyResolver(PerspectiveLazyResolver object) {
				return createPerspectiveLazyResolverAdapter();
			}
			@Override
			public Adapter casePerspectivePackage(PerspectivePackage object) {
				return createPerspectivePackageAdapter();
			}
			@Override
			public Adapter casePerspectiveBase(PerspectiveBase object) {
				return createPerspectiveBaseAdapter();
			}
			@Override
			public Adapter casePerspective(Perspective object) {
				return createPerspectiveAdapter();
			}
			@Override
			public Adapter casePerspectiveEventManager(PerspectiveEventManager object) {
				return createPerspectiveEventManagerAdapter();
			}
			@Override
			public Adapter casePerspectiveEvent(PerspectiveEvent object) {
				return createPerspectiveEventAdapter();
			}
			@Override
			public Adapter casePerspectiveElement(PerspectiveElement object) {
				return createPerspectiveElementAdapter();
			}
			@Override
			public Adapter casePerspectiveSashContainer(PerspectiveSashContainer object) {
				return createPerspectiveSashContainerAdapter();
			}
			@Override
			public Adapter casePerspectivePart(PerspectivePart object) {
				return createPerspectivePartAdapter();
			}
			@Override
			public Adapter casePerspectivePartStack(PerspectivePartStack object) {
				return createPerspectivePartStackAdapter();
			}
			@Override
			public Adapter casePerspectiveView(PerspectiveView object) {
				return createPerspectiveViewAdapter();
			}
			@Override
			public Adapter casePerspectiveSelection(PerspectiveSelection object) {
				return createPerspectiveSelectionAdapter();
			}
			@Override
			public Adapter casePerspectiveTable(PerspectiveTable object) {
				return createPerspectiveTableAdapter();
			}
			@Override
			public Adapter casePerspectiveGrid(PerspectiveGrid object) {
				return createPerspectiveGridAdapter();
			}
			@Override
			public Adapter casePerspectiveChart(PerspectiveChart object) {
				return createPerspectiveChartAdapter();
			}
			@Override
			public Adapter casePerspectiveReport(PerspectiveReport object) {
				return createPerspectiveReportAdapter();
			}
			@Override
			public Adapter casePerspectiveOrganization(PerspectiveOrganization object) {
				return createPerspectiveOrganizationAdapter();
			}
			@Override
			public Adapter casePerspectiveTopology(PerspectiveTopology object) {
				return createPerspectiveTopologyAdapter();
			}
			@Override
			public Adapter casePerspectiveDialog(PerspectiveDialog object) {
				return createPerspectiveDialogAdapter();
			}
			@Override
			public Adapter casePerspectiveBPMN(PerspectiveBPMN object) {
				return createPerspectiveBPMNAdapter();
			}
			@Override
			public Adapter casePerspectiveKanban(PerspectiveKanban object) {
				return createPerspectiveKanbanAdapter();
			}
			@Override
			public Adapter casePerspectiveWelcome(PerspectiveWelcome object) {
				return createPerspectiveWelcomeAdapter();
			}
			@Override
			public Adapter casePerspectiveSaiku(PerspectiveSaiku object) {
				return createPerspectiveSaikuAdapter();
			}
			@Override
			public Adapter casePerspectiveSearch(PerspectiveSearch object) {
				return createPerspectiveSearchAdapter();
			}
			@Override
			public Adapter casePerspectiveDataInterchange(PerspectiveDataInterchange object) {
				return createPerspectiveDataInterchangeAdapter();
			}
			@Override
			public Adapter casePerspectiveTitle(PerspectiveTitle object) {
				return createPerspectiveTitleAdapter();
			}
			@Override
			public Adapter casePerspectiveDashboard(PerspectiveDashboard object) {
				return createPerspectiveDashboardAdapter();
			}
			@Override
			public Adapter casePerspectiveBrowser(PerspectiveBrowser object) {
				return createPerspectiveBrowserAdapter();
			}
			@Override
			public Adapter caseLLazyResolver(LLazyResolver object) {
				return createLLazyResolverAdapter();
			}
			@Override
			public Adapter caseLPackage(LPackage object) {
				return createLPackageAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveModel <em>Perspective Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveModel
	 * @generated
	 */
	public Adapter createPerspectiveModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveLazyResolver <em>Perspective Lazy Resolver</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveLazyResolver
	 * @generated
	 */
	public Adapter createPerspectiveLazyResolverAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectivePackage <em>Perspective Package</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectivePackage
	 * @generated
	 */
	public Adapter createPerspectivePackageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveBase <em>Perspective Base</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveBase
	 * @generated
	 */
	public Adapter createPerspectiveBaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.Perspective <em>Perspective</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.Perspective
	 * @generated
	 */
	public Adapter createPerspectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveEventManager <em>Perspective Event Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveEventManager
	 * @generated
	 */
	public Adapter createPerspectiveEventManagerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveEvent <em>Perspective Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveEvent
	 * @generated
	 */
	public Adapter createPerspectiveEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveElement <em>Perspective Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveElement
	 * @generated
	 */
	public Adapter createPerspectiveElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveSashContainer <em>Perspective Sash Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveSashContainer
	 * @generated
	 */
	public Adapter createPerspectiveSashContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectivePart <em>Perspective Part</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectivePart
	 * @generated
	 */
	public Adapter createPerspectivePartAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectivePartStack <em>Perspective Part Stack</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectivePartStack
	 * @generated
	 */
	public Adapter createPerspectivePartStackAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveView <em>Perspective View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveView
	 * @generated
	 */
	public Adapter createPerspectiveViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveSelection <em>Perspective Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveSelection
	 * @generated
	 */
	public Adapter createPerspectiveSelectionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveTable <em>Perspective Table</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveTable
	 * @generated
	 */
	public Adapter createPerspectiveTableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveGrid <em>Perspective Grid</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveGrid
	 * @generated
	 */
	public Adapter createPerspectiveGridAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveChart <em>Perspective Chart</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveChart
	 * @generated
	 */
	public Adapter createPerspectiveChartAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveReport <em>Perspective Report</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveReport
	 * @generated
	 */
	public Adapter createPerspectiveReportAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveOrganization <em>Perspective Organization</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveOrganization
	 * @generated
	 */
	public Adapter createPerspectiveOrganizationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveTopology <em>Perspective Topology</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveTopology
	 * @generated
	 */
	public Adapter createPerspectiveTopologyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveDialog <em>Perspective Dialog</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDialog
	 * @generated
	 */
	public Adapter createPerspectiveDialogAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveBPMN <em>Perspective BPMN</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveBPMN
	 * @generated
	 */
	public Adapter createPerspectiveBPMNAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveKanban <em>Perspective Kanban</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveKanban
	 * @generated
	 */
	public Adapter createPerspectiveKanbanAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveWelcome <em>Perspective Welcome</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveWelcome
	 * @generated
	 */
	public Adapter createPerspectiveWelcomeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveSaiku <em>Perspective Saiku</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveSaiku
	 * @generated
	 */
	public Adapter createPerspectiveSaikuAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveSearch <em>Perspective Search</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveSearch
	 * @generated
	 */
	public Adapter createPerspectiveSearchAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveDataInterchange <em>Perspective Data Interchange</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDataInterchange
	 * @generated
	 */
	public Adapter createPerspectiveDataInterchangeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveTitle <em>Perspective Title</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveTitle
	 * @generated
	 */
	public Adapter createPerspectiveTitleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveDashboard <em>Perspective Dashboard</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDashboard
	 * @generated
	 */
	public Adapter createPerspectiveDashboardAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.perspective.PerspectiveBrowser <em>Perspective Browser</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveBrowser
	 * @generated
	 */
	public Adapter createPerspectiveBrowserAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver <em>LLazy Resolver</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver
	 * @generated
	 */
	public Adapter createLLazyResolverAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.dsl.semantic.common.types.LPackage <em>LPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LPackage
	 * @generated
	 */
	public Adapter createLPackageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //PerspectiveDslAdapterFactory
