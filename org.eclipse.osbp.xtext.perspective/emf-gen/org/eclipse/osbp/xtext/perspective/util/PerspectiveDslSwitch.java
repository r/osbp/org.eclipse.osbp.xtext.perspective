/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.perspective.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver;
import org.eclipse.osbp.dsl.semantic.common.types.LPackage;

import org.eclipse.osbp.xtext.perspective.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage
 * @generated
 */
public class PerspectiveDslSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static PerspectiveDslPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerspectiveDslSwitch() {
		if (modelPackage == null) {
			modelPackage = PerspectiveDslPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case PerspectiveDslPackage.PERSPECTIVE_MODEL: {
				PerspectiveModel perspectiveModel = (PerspectiveModel)theEObject;
				T result = casePerspectiveModel(perspectiveModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_LAZY_RESOLVER: {
				PerspectiveLazyResolver perspectiveLazyResolver = (PerspectiveLazyResolver)theEObject;
				T result = casePerspectiveLazyResolver(perspectiveLazyResolver);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_PACKAGE: {
				PerspectivePackage perspectivePackage = (PerspectivePackage)theEObject;
				T result = casePerspectivePackage(perspectivePackage);
				if (result == null) result = caseLPackage(perspectivePackage);
				if (result == null) result = caseLLazyResolver(perspectivePackage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_BASE: {
				PerspectiveBase perspectiveBase = (PerspectiveBase)theEObject;
				T result = casePerspectiveBase(perspectiveBase);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveBase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE: {
				Perspective perspective = (Perspective)theEObject;
				T result = casePerspective(perspective);
				if (result == null) result = casePerspectiveBase(perspective);
				if (result == null) result = casePerspectiveLazyResolver(perspective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_EVENT_MANAGER: {
				PerspectiveEventManager perspectiveEventManager = (PerspectiveEventManager)theEObject;
				T result = casePerspectiveEventManager(perspectiveEventManager);
				if (result == null) result = casePerspectiveBase(perspectiveEventManager);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveEventManager);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_EVENT: {
				PerspectiveEvent perspectiveEvent = (PerspectiveEvent)theEObject;
				T result = casePerspectiveEvent(perspectiveEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_ELEMENT: {
				PerspectiveElement perspectiveElement = (PerspectiveElement)theEObject;
				T result = casePerspectiveElement(perspectiveElement);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_SASH_CONTAINER: {
				PerspectiveSashContainer perspectiveSashContainer = (PerspectiveSashContainer)theEObject;
				T result = casePerspectiveSashContainer(perspectiveSashContainer);
				if (result == null) result = casePerspectiveElement(perspectiveSashContainer);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveSashContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_PART: {
				PerspectivePart perspectivePart = (PerspectivePart)theEObject;
				T result = casePerspectivePart(perspectivePart);
				if (result == null) result = casePerspectiveElement(perspectivePart);
				if (result == null) result = casePerspectiveLazyResolver(perspectivePart);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_PART_STACK: {
				PerspectivePartStack perspectivePartStack = (PerspectivePartStack)theEObject;
				T result = casePerspectivePartStack(perspectivePartStack);
				if (result == null) result = casePerspectiveElement(perspectivePartStack);
				if (result == null) result = casePerspectiveLazyResolver(perspectivePartStack);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_VIEW: {
				PerspectiveView perspectiveView = (PerspectiveView)theEObject;
				T result = casePerspectiveView(perspectiveView);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_SELECTION: {
				PerspectiveSelection perspectiveSelection = (PerspectiveSelection)theEObject;
				T result = casePerspectiveSelection(perspectiveSelection);
				if (result == null) result = casePerspectiveView(perspectiveSelection);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveSelection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_TABLE: {
				PerspectiveTable perspectiveTable = (PerspectiveTable)theEObject;
				T result = casePerspectiveTable(perspectiveTable);
				if (result == null) result = casePerspectiveView(perspectiveTable);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveTable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_GRID: {
				PerspectiveGrid perspectiveGrid = (PerspectiveGrid)theEObject;
				T result = casePerspectiveGrid(perspectiveGrid);
				if (result == null) result = casePerspectiveView(perspectiveGrid);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveGrid);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_CHART: {
				PerspectiveChart perspectiveChart = (PerspectiveChart)theEObject;
				T result = casePerspectiveChart(perspectiveChart);
				if (result == null) result = casePerspectiveView(perspectiveChart);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveChart);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_REPORT: {
				PerspectiveReport perspectiveReport = (PerspectiveReport)theEObject;
				T result = casePerspectiveReport(perspectiveReport);
				if (result == null) result = casePerspectiveView(perspectiveReport);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveReport);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_ORGANIZATION: {
				PerspectiveOrganization perspectiveOrganization = (PerspectiveOrganization)theEObject;
				T result = casePerspectiveOrganization(perspectiveOrganization);
				if (result == null) result = casePerspectiveView(perspectiveOrganization);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveOrganization);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_TOPOLOGY: {
				PerspectiveTopology perspectiveTopology = (PerspectiveTopology)theEObject;
				T result = casePerspectiveTopology(perspectiveTopology);
				if (result == null) result = casePerspectiveView(perspectiveTopology);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveTopology);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_DIALOG: {
				PerspectiveDialog perspectiveDialog = (PerspectiveDialog)theEObject;
				T result = casePerspectiveDialog(perspectiveDialog);
				if (result == null) result = casePerspectiveView(perspectiveDialog);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveDialog);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_BPMN: {
				PerspectiveBPMN perspectiveBPMN = (PerspectiveBPMN)theEObject;
				T result = casePerspectiveBPMN(perspectiveBPMN);
				if (result == null) result = casePerspectiveView(perspectiveBPMN);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveBPMN);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_KANBAN: {
				PerspectiveKanban perspectiveKanban = (PerspectiveKanban)theEObject;
				T result = casePerspectiveKanban(perspectiveKanban);
				if (result == null) result = casePerspectiveView(perspectiveKanban);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveKanban);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_WELCOME: {
				PerspectiveWelcome perspectiveWelcome = (PerspectiveWelcome)theEObject;
				T result = casePerspectiveWelcome(perspectiveWelcome);
				if (result == null) result = casePerspectiveView(perspectiveWelcome);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveWelcome);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_SAIKU: {
				PerspectiveSaiku perspectiveSaiku = (PerspectiveSaiku)theEObject;
				T result = casePerspectiveSaiku(perspectiveSaiku);
				if (result == null) result = casePerspectiveView(perspectiveSaiku);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveSaiku);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_SEARCH: {
				PerspectiveSearch perspectiveSearch = (PerspectiveSearch)theEObject;
				T result = casePerspectiveSearch(perspectiveSearch);
				if (result == null) result = casePerspectiveView(perspectiveSearch);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveSearch);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_DATA_INTERCHANGE: {
				PerspectiveDataInterchange perspectiveDataInterchange = (PerspectiveDataInterchange)theEObject;
				T result = casePerspectiveDataInterchange(perspectiveDataInterchange);
				if (result == null) result = casePerspectiveView(perspectiveDataInterchange);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveDataInterchange);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_TITLE: {
				PerspectiveTitle perspectiveTitle = (PerspectiveTitle)theEObject;
				T result = casePerspectiveTitle(perspectiveTitle);
				if (result == null) result = casePerspectiveView(perspectiveTitle);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveTitle);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_DASHBOARD: {
				PerspectiveDashboard perspectiveDashboard = (PerspectiveDashboard)theEObject;
				T result = casePerspectiveDashboard(perspectiveDashboard);
				if (result == null) result = casePerspectiveView(perspectiveDashboard);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveDashboard);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PerspectiveDslPackage.PERSPECTIVE_BROWSER: {
				PerspectiveBrowser perspectiveBrowser = (PerspectiveBrowser)theEObject;
				T result = casePerspectiveBrowser(perspectiveBrowser);
				if (result == null) result = casePerspectiveView(perspectiveBrowser);
				if (result == null) result = casePerspectiveLazyResolver(perspectiveBrowser);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveModel(PerspectiveModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Lazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Lazy Resolver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveLazyResolver(PerspectiveLazyResolver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Package</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Package</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectivePackage(PerspectivePackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Base</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Base</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveBase(PerspectiveBase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspective(Perspective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Event Manager</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Event Manager</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveEventManager(PerspectiveEventManager object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveEvent(PerspectiveEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveElement(PerspectiveElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Sash Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Sash Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveSashContainer(PerspectiveSashContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Part</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Part</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectivePart(PerspectivePart object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Part Stack</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Part Stack</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectivePartStack(PerspectivePartStack object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveView(PerspectiveView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Selection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Selection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveSelection(PerspectiveSelection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Table</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Table</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveTable(PerspectiveTable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Grid</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Grid</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveGrid(PerspectiveGrid object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Chart</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Chart</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveChart(PerspectiveChart object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Report</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Report</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveReport(PerspectiveReport object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Organization</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Organization</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveOrganization(PerspectiveOrganization object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Topology</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Topology</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveTopology(PerspectiveTopology object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Dialog</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Dialog</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveDialog(PerspectiveDialog object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective BPMN</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective BPMN</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveBPMN(PerspectiveBPMN object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Kanban</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Kanban</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveKanban(PerspectiveKanban object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Welcome</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Welcome</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveWelcome(PerspectiveWelcome object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Saiku</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Saiku</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveSaiku(PerspectiveSaiku object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Search</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Search</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveSearch(PerspectiveSearch object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Data Interchange</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Data Interchange</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveDataInterchange(PerspectiveDataInterchange object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Title</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Title</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveTitle(PerspectiveTitle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Dashboard</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Dashboard</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveDashboard(PerspectiveDashboard object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Perspective Browser</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Perspective Browser</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerspectiveBrowser(PerspectiveBrowser object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LLazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LLazy Resolver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLLazyResolver(LLazyResolver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LPackage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LPackage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLPackage(LPackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //PerspectiveDslSwitch
