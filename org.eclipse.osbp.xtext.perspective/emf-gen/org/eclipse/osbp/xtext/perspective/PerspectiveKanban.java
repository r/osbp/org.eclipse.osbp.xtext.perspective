/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.perspective;

import org.eclipse.osbp.dsl.semantic.dto.LDto;

import org.eclipse.osbp.xtext.dialogdsl.Dialog;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Perspective Kanban</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.PerspectiveKanban#getDtoRef <em>Dto Ref</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.PerspectiveKanban#getCardRef <em>Card Ref</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.PerspectiveKanban#getDialogRef <em>Dialog Ref</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectiveKanban()
 * @model
 * @generated
 */
public interface PerspectiveKanban extends PerspectiveView {
	/**
	 * Returns the value of the '<em><b>Dto Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dto Ref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dto Ref</em>' reference.
	 * @see #setDtoRef(LDto)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectiveKanban_DtoRef()
	 * @model
	 * @generated
	 */
	LDto getDtoRef();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.PerspectiveKanban#getDtoRef <em>Dto Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dto Ref</em>' reference.
	 * @see #getDtoRef()
	 * @generated
	 */
	void setDtoRef(LDto value);

	/**
	 * Returns the value of the '<em><b>Card Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Card Ref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Card Ref</em>' reference.
	 * @see #setCardRef(Dialog)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectiveKanban_CardRef()
	 * @model
	 * @generated
	 */
	Dialog getCardRef();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.PerspectiveKanban#getCardRef <em>Card Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Card Ref</em>' reference.
	 * @see #getCardRef()
	 * @generated
	 */
	void setCardRef(Dialog value);

	/**
	 * Returns the value of the '<em><b>Dialog Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dialog Ref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dialog Ref</em>' reference.
	 * @see #setDialogRef(Dialog)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectiveKanban_DialogRef()
	 * @model
	 * @generated
	 */
	Dialog getDialogRef();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.PerspectiveKanban#getDialogRef <em>Dialog Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dialog Ref</em>' reference.
	 * @see #getDialogRef()
	 * @generated
	 */
	void setDialogRef(Dialog value);

} // PerspectiveKanban
