/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.perspective;

import org.eclipse.osbp.xtext.topologydsl.Topology;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Perspective Topology</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.perspective.PerspectiveTopology#getRef <em>Ref</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectiveTopology()
 * @model
 * @generated
 */
public interface PerspectiveTopology extends PerspectiveView {
	/**
	 * Returns the value of the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref</em>' reference.
	 * @see #setRef(Topology)
	 * @see org.eclipse.osbp.xtext.perspective.PerspectiveDslPackage#getPerspectiveTopology_Ref()
	 * @model
	 * @generated
	 */
	Topology getRef();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.perspective.PerspectiveTopology#getRef <em>Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ref</em>' reference.
	 * @see #getRef()
	 * @generated
	 */
	void setRef(Topology value);

} // PerspectiveTopology
