/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.perspective.derivedstate;

import java.util.Arrays;
import java.util.function.Consumer;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.xtext.perspective.Perspective;
import org.eclipse.osbp.xtext.perspective.PerspectiveElement;
import org.eclipse.osbp.xtext.perspective.PerspectiveModel;
import org.eclipse.osbp.xtext.perspective.PerspectivePackage;
import org.eclipse.osbp.xtext.perspective.PerspectivePart;
import org.eclipse.osbp.xtext.perspective.PerspectivePartStack;
import org.eclipse.osbp.xtext.perspective.PerspectiveSashContainer;
import org.eclipse.xtext.resource.DerivedStateAwareResource;
import org.eclipse.xtext.xbase.jvmmodel.JvmModelAssociator;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("restriction")
public class PerspectiveModelDerivedStateComputerx extends JvmModelAssociator {
  private final static Logger log = LoggerFactory.getLogger(PerspectiveModelDerivedStateComputerx.class);
  
  private DerivedStateAwareResource resource;
  
  @Override
  public void installDerivedState(final DerivedStateAwareResource resource, final boolean preLinkingPhase) {
    PerspectiveModelDerivedStateComputerx.log.debug("PerspectiveModelDerivedStateComputerx called");
    super.installDerivedState(resource, preLinkingPhase);
    this.resource = resource;
    boolean _isEmpty = resource.getContents().isEmpty();
    if (_isEmpty) {
      return;
    }
    if ((!preLinkingPhase)) {
      EObject _get = resource.getContents().get(0);
      final PerspectiveModel eObject = ((PerspectiveModel) _get);
      try {
        final Consumer<EObject> _function = (EObject it) -> {
          this.map(it);
        };
        eObject.eContents().forEach(_function);
      } catch (final Throwable _t) {
        if (_t instanceof Exception) {
          final Exception ex = (Exception)_t;
          PerspectiveModelDerivedStateComputerx.log.error("{}", ex);
        } else {
          throw Exceptions.sneakyThrow(_t);
        }
      }
    }
  }
  
  protected void _map(final PerspectivePackage object) {
    PerspectiveModelDerivedStateComputerx.log.debug("CCPerspectivePackage");
    final Consumer<Perspective> _function = (Perspective it) -> {
      this.map(it);
    };
    object.getPerspectives().forEach(_function);
  }
  
  protected void _map(final Perspective object) {
    PerspectiveModelDerivedStateComputerx.log.debug("CCPerspective");
    final Consumer<PerspectiveElement> _function = (PerspectiveElement it) -> {
      this.map(it);
    };
    object.getElements().forEach(_function);
  }
  
  protected void _map(final PerspectiveSashContainer object) {
    PerspectiveModelDerivedStateComputerx.log.debug("CCPerspectiveSashContainer");
    final Consumer<PerspectiveElement> _function = (PerspectiveElement it) -> {
      this.map(it);
    };
    object.getElements().forEach(_function);
  }
  
  protected void _map(final PerspectivePartStack object) {
    PerspectiveModelDerivedStateComputerx.log.debug("CCPerspectivePartStack");
    final Consumer<PerspectiveElement> _function = (PerspectiveElement it) -> {
      this.map(it);
    };
    object.getElements().forEach(_function);
  }
  
  protected void _map(final PerspectivePart object) {
    PerspectiveModelDerivedStateComputerx.log.debug("CCPerspectivePart");
  }
  
  protected void _map(final Void object) {
    PerspectiveModelDerivedStateComputerx.log.debug("void");
  }
  
  public void map(final EObject object) {
    if (object instanceof Perspective) {
      _map((Perspective)object);
      return;
    } else if (object instanceof PerspectivePackage) {
      _map((PerspectivePackage)object);
      return;
    } else if (object instanceof PerspectivePart) {
      _map((PerspectivePart)object);
      return;
    } else if (object instanceof PerspectivePartStack) {
      _map((PerspectivePartStack)object);
      return;
    } else if (object instanceof PerspectiveSashContainer) {
      _map((PerspectiveSashContainer)object);
      return;
    } else if (object == null) {
      _map((Void)null);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(object).toString());
    }
  }
}
