/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.perspective.jvmmodel;

import com.vaadin.ui.UI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import javax.inject.Inject;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.e4.core.di.extensions.EventUtils;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.osbp.eventbroker.EventBrokerMsg;
import org.eclipse.osbp.runtime.common.i18n.ITranslator;
import org.eclipse.osbp.runtime.web.vaadin.databinding.VaadinObservables;
import org.eclipse.osbp.xtext.basic.generator.BasicDslGeneratorUtils;
import org.eclipse.osbp.xtext.i18n.I18NModelGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.xbase.compiler.GeneratorConfig;
import org.eclipse.xtext.xbase.compiler.ImportManager;
import org.eclipse.xtext.xbase.compiler.output.TreeAppendable;
import org.eclipse.xtext.xbase.lib.Extension;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("all")
public class PerspectiveModelGenerator extends I18NModelGenerator {
  @Inject
  @Extension
  private BasicDslGeneratorUtils _basicDslGeneratorUtils;
  
  @Override
  public TreeAppendable createAppendable(final EObject context, final ImportManager importManager, final GeneratorConfig config) {
    TreeAppendable _xblockexpression = null;
    {
      this.setBuilder(context.eResource());
      importManager.addImportFor(this._typeReferenceBuilder.typeRef(HashMap.class, this._typeReferenceBuilder.typeRef(String.class), this._typeReferenceBuilder.typeRef(String.class)).getType());
      importManager.addImportFor(this._typeReferenceBuilder.typeRef(Map.class, this._typeReferenceBuilder.typeRef(String.class), this._typeReferenceBuilder.typeRef(String.class)).getType());
      importManager.addImportFor(this._typeReferenceBuilder.typeRef(Collection.class, this._typeReferenceBuilder.typeRef(String.class)).getType());
      this._basicDslGeneratorUtils.addImportFor(this, importManager, this._typeReferenceBuilder, ArrayList.class, IEventBroker.class, EventUtils.class, EventBrokerMsg.class, EventHandler.class, Logger.class, LoggerFactory.class, UI.class, VaadinObservables.class, DataBindingContext.class, BeansObservables.class, ResourceBundle.class, Locale.class, UpdateValueStrategy.class, ITranslator.class);
      _xblockexpression = super.createAppendable(context, importManager, config);
    }
    return _xblockexpression;
  }
  
  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess fsa) {
    this.addTranslatables("PerspectiveNA");
    super.doGenerate(resource, fsa);
  }
}
